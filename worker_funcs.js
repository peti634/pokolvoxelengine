function minimumVertex(arr, arrhossz, arrcsop) {
    var vizsge = 0;
    var ret = [];
    var status = new Int8Array(arr.length);
    var keresi;
    var okelemek = new Int32Array(arr.length);
    var okelemdb = 0;
    var vizsgcs;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] !== 0) {
            okelemek[okelemdb] = i;
            okelemdb++;
        }
    }
    this.keresUres = function () {
        var l = status.length;
        for (var i = keresi; i < okelemdb; i++) {
            var index = okelemek[i];
            if (status[index] === vizsge && arr[index] !== 0) {
                keresi = i + 1;
                return index;
            }
        }
        return -1;
    };
    this.blockok = function (i, j) {
        var index = (j * arrhossz) + i;
        if (index < 0 || index >= arr.length)
            return false;
        if (status[index] === -1)
            return false; //egyszer már levizsgáltuk
        if (arr[index] === 0)
            return false; //itt nincs semmi
        if (vizsgcs !== arrcsop[index])
            return false;
        return true;
    };
    this.terEll = function (x1, x2, y1, y2, tipus) {
        for (var i = x1; i <= x2; i++) {
            if (i >= arrhossz || i < 0)
                return false;
            for (var j = y1; j <= y2; j++) {
                if (j < 0 || j >= (arr.length / arrhossz))
                    return false;
                if (!this.blockok(i, j))
                    return false;
            }
        }
        return true;
    };
    while (true) {

        var legcoord = undefined;
        var legter = 0;
        keresi = 0;
        while (true) {
            var keresu = this.keresUres();
            if (keresu === -1)
                break;
            var elem = arr[keresu];
            vizsgcs = arrcsop[keresu];
            var px = keresu % arrhossz;
            var py = (keresu / arrhossz) | 0;
            var pos = new Int32Array(5);
            pos[0] = px;
            pos[1] = py;
            pos[2] = px;
            pos[3] = py;
            pos[4] = vizsgcs;
            //eddig növeljük 4 irányba amíg lehetséges vertexnek
            for (; ; pos[2]++) {
                if (!this.terEll(pos[2], pos[2], pos[1], pos[3], elem)) {
                    pos[2]--;
                    break;
                }
            }

            for (; ; pos[3]++) {
                if (!this.terEll(pos[0], pos[2], pos[3], pos[3], elem)) {
                    pos[3]--;
                    break;
                }
            }
            for (; ; pos[0]--) {
                if (!this.terEll(pos[0], pos[0], pos[1], pos[3], elem)) {
                    pos[0]++;
                    break;
                }
            }
            for (; ; pos[1]--) {
                if (!this.terEll(pos[0], pos[2], pos[1], pos[1], elem)) {
                    pos[1]++;
                    break;
                }
            }
            var ter = 0;
            for (var i = pos[0]; i <= pos[2]; i++) {
                for (var j = pos[1]; j <= pos[3]; j++) {
                    var index = (j * arrhossz) + i;
                    status[index] = (vizsge === 0) ? 1 : 0;
                    ter++;
                }
            }
            if (ter > legter) {
                legter = ter;
                legcoord = pos;
            }

        }
        if (legcoord === undefined)
            break;
        vizsge = (vizsge === 0) ? 1 : 0;
        for (var i = legcoord[0]; i <= legcoord[2]; i++) {
            for (var j = legcoord[1]; j <= legcoord[3]; j++) {
                var index = (j * arrhossz) + i;
                status[index] = -1;
            }
        }
        ret.push({pos: legcoord});
    }
    return ret;
}

function getOldalPos(pos, oldal) {
    var ret = [pos[0], pos[1], pos[2]];
    switch (oldal) {
        case 0:
            ret[0]--;
            break;
        case 1:
            ret[0]++;
            break;
        case 2:
            ret[1]--;
            break;
        case 3:
            ret[1]++;
            break;
        case 4:
            ret[2]--;
            break;
        case 5:
            ret[2]++;
            break;
    }
    return ret;
}
function vec3_dis(a, b) {
    var x = b[0] - a[0],
            y = b[1] - a[1],
            z = b[2] - a[2];
    return Math.sqrt(x * x + y * y + z * z);
}


function dinamic_3d_array(mxz, type) {
    this.mxz = mxz;
    this.my = 0;
    this.ypos = 0;
    this.buffer = new type(0);
    this.getType = function () {
        return type;
    };
    this.y_teszt = function (y, fill) {
        if (y >= (this.my + this.ypos)) {
            for (; y >= (this.my + this.ypos); this.my += 8)
                ;
            var ujdata = new type(this.mxz * this.mxz * this.my);
            ujdata.set(this.buffer);
            if (fill !== 0 && fill !== undefined) {
                for (var i = this.buffer.length; i < ujdata.length; i++) {
                    ujdata[i] = fill;
                }
            }
            this.buffer = ujdata;
        }
        if (y < this.ypos) {
            var regiy = this.ypos;
            for (;  y < this.ypos; this.ypos -= 8, this.my += 8)
                ;
            var ujdata = new type(this.mxz * this.mxz * this.my);
            var masolpos = (regiy - this.ypos ) * this.mxz * this.mxz;
            ujdata.set(this.buffer, masolpos);
            this.buffer = ujdata;
        }
    };
    this.setElem = function (x, y, z, t) {
        if (x < 0 || y < this.ypos || z < 0 || x >= this.mxz || z >= this.mxz || y >= (this.my + this.ypos))
            return;
        this.buffer[ x + (z * this.mxz) + ((y - this.ypos) * this.mxz * this.mxz) ] = t;
    };
    this.addElem = function (x, y, z, t) {
        if (x < 0 || y < this.ypos || z < 0 || x >= this.mxz || z >= this.mxz || y >= (this.my + this.ypos))
            return;
        this.buffer[ x + (z * this.mxz) + ((y - this.ypos) * this.mxz * this.mxz) ] += t;
    };
    this.getElem = function (x, y, z) {
        if (x < 0 || y < this.ypos || z < 0 || x >= this.mxz || z >= this.mxz || y >= (this.my + this.ypos))
            return 0;
        return this.buffer[ x + (z * this.mxz) + ((y - this.ypos) * this.mxz * this.mxz) ];
    };
    this.getElem_undef = function (x, y, z) {
        if (x < 0 || y < this.ypos || z < 0 || x >= this.mxz || z >= this.mxz || y >= (this.my + this.ypos))
            return undefined;
        return this.buffer[ x + (z * this.mxz) + ((y - this.ypos) * this.mxz * this.mxz) ];
    };
    this.getElem_noy = function (x, y, z) {
        if (x < 0 || y < 0 || z < 0 || x >= this.mxz || z >= this.mxz || y >= this.my)
            return 0;
        return this.buffer[ x + (z * this.mxz) + (y * this.mxz * this.mxz) ];
    };
}

function dinamic_3d_array_napfeny_yteszt(d3_array, y, napfeny) {
    if (y >= (d3_array.my + d3_array.ypos)) {
        for (; y >= (d3_array.my + d3_array.ypos); d3_array.my += 8)
            ;
        var type = d3_array.getType();
        var ujdata = new type(d3_array.mxz * d3_array.mxz * d3_array.my);
        ujdata.set(d3_array.buffer);

        for (var i = d3_array.buffer.length; i < ujdata.length; i++) {
            ujdata[i] = napfeny;
        }

        d3_array.buffer = ujdata;
    }
    if (y < d3_array.ypos) {
        var regiy = d3_array.ypos;
        for (; y < d3_array.ypos; d3_array.ypos -= 8, d3_array.my += 8)
            ;
        var type = d3_array.getType();

        var ujdata = new type(d3_array.mxz * d3_array.mxz * d3_array.my);
        var masolpos = (regiy - d3_array.ypos) * d3_array.mxz * d3_array.mxz;


        ujdata.set(d3_array.buffer, masolpos);

        var mxz_2 = d3_array.mxz * d3_array.mxz;
        var osszy = regiy - d3_array.ypos;
        for (var i = 0; i < mxz_2; i++) {

            var e = napfeny;
            if (d3_array.length > 0) {
                e = d3_array.buffer[i];//legalsó napfény érték kinyerése
            }
            for (var j = 0; j < osszy; j++) {

                ujdata[i + (j * mxz_2)] = e;
            }
        }
        d3_array.buffer = ujdata;
    }

}

function d2d_array() {
    this.buffer;
    this.mx;
    this.my;
    /**
     * Törli a buffer, nullázza mx és my
     */
    this.clean = function () {
        this.buffer = undefined;
        this.mx = 0;
        this.my = 0;
    };
    /**
     * Újraméretezi a buffert
     * @param {Number} x
     * @param {Number} y
     */
    this.resize = function (nx, ny) {

        var ujbuff = new Uint32Array(nx * ny);
        if (this.buffer !== undefined) {
            var mmasol = this.mx;
            if (mmasol > nx)
                mmasol = nx;
            for (var i = 0; i < this.my && i < ny; i++) {
                var pmasol = i * this.mx;
                var masol = this.buffer.subarray(pmasol, pmasol + mmasol);
                ujbuff.set(masol, i * nx);
            }
        }
        this.mx = nx;
        this.my = ny;
        this.buffer = ujbuff;
    };
    /*
     * 
     */
    this.set = function (x, y, e) {
        if (x >= 0 && y >= 0 && x < this.mx && y < this.my) {
            this.buffer[x + (y * this.mx)] = e;
            return true;
        }
        return false;
    };
    this.setArr = function (x, y, arr) {
        var despos = (this.mx * y) + x;
        if (arr.length + despos > this.buffer.length || despos < 0) {
            throw "Hiba";
        }
        this.buffer.set(arr, despos);
    };
    /*
     * 
     */
    this.get = function (x, y) {
        if (x >= 0 && y >= 0 && x < this.mx && y < this.my) {
            return this.buffer[x + (y * this.mx)];
        }
        return 0;
    };
    this.get_undef = function (x, y) {
        if (x >= 0 && y >= 0 && x < this.mx && y < this.my) {
            return this.buffer[x + (y * this.mx)];
        }
        return undefined;
    };
    this.clean();
}

function dinamicArray(inc_step, arrtype) {
    this.buffer = new arrtype(inc_step);
    this.pos = 0;
    this.need = function (size) {
        if (this.buffer.length < this.pos + size) {
            var ujlen = this.buffer.length + inc_step + size;
            var uj = new arrtype(ujlen);
            uj.set(this.buffer);
            this.buffer = uj;
        }
    };
    this.clean = function () {
        this.pos = 0;
    };
    this.add_3Item = function (i1, i2, i3) {
        this.need(3);
        this.buffer[this.pos++] = i1;
        this.buffer[this.pos++] = i2;
        this.buffer[this.pos++] = i3;
    };
    this.add_4Item = function (i1, i2, i3, i4) {
        this.need(4);
        this.buffer[this.pos++] = i1;
        this.buffer[this.pos++] = i2;
        this.buffer[this.pos++] = i3;
        this.buffer[this.pos++] = i4;
    };
}

function getarray2d_m(arr, x, y, xm, ym) {
    if (x >= 0 && y >= 0 && xm > x && ym > y)
        return arr[x + (y * xm)];
    return undefined;
}

function setarray2d(arr, x, y, e) {
    if (x >= 0 && y >= 0 && arr.xm > x && arr.ym > y)
        arr[x + (y * arr.xm)] = e;
}

function texInTex_min(x, y) {
    this.SECTORSIZE = 32;
    //felgyorsítható: int32-be tárolja a foglalt biteket
    this.sx = 0;
    this.sy = 0;
    this.foglalva;
    this.sectorFoglal;
    this.resize = function (nx, ny) {
        var uj = new Uint8Array(nx * ny);
        var secm = Math.ceil(nx / this.SECTORSIZE) * Math.ceil(ny / this.SECTORSIZE);
        var ujsec = new Uint32Array(secm);
        var mmasol = this.sx;
        if (mmasol > nx)
            mmasol = nx;
        for (var i = 0; i < this.sy && i < ny; i++) {
            var pmasol = i * this.sx;
            var masol = this.foglalva.subarray(pmasol, pmasol + mmasol);
            uj.set(masol, i * nx);
        }
        //amikor resize, akkor minden sector nullázva van (mindenhol max a legkisebb elfogalt terület)
        //azért mert ha olyan elemet akarunk lefoglalni, ami alapból túl nagy a mérethez
        //és legalább milyenkor frissítünk egyet
        for (var i = 0; i < ujsec.length; i++)
            ujsec[i] = 0xFFFFFFFF;
        this.sx = nx;
        this.sy = ny;
        this.foglalva = uj;
        this.sectorFoglal = ujsec;
    };
    this.addElem = function (mx, my) {
        this.terell = function (x, y, xm, ym, maxx) {
            for (var j = y; j < y + ym; j++) {
                for (var i = x; i < x + xm; i++) {
                    if (this.foglalva [i + (j * this.sx)] === 1) {
                        if (j >= this.sy || i >= this.sx)
                            return maxx;
                        for (; i < maxx && i < this.sx; i++) {
                            if (this.foglalva [i + (j * this.sx)] === 0)
                                return i - 1;
                        }
                        return maxx;
                    }
                }
            }
            if (j >= this.sy || i >= this.sx)
                return maxx;
            return -1;
        };
        this.sectorEll = function (sectorindex, force) {
            var min = this.sectorFoglal[sectorindex];
            var min_x = min & 0xFFFF;
            var min_y = (min >> 16) & 0xFFFF;
            if (force) {
                if (mx >= min_x && my >= min_y)
                    return undefined;
            } else {
                if (mx >= min_x || my >= min_y)
                    return undefined;
            }

            var sectorblocks = Math.ceil(this.sx / this.SECTORSIZE);
            var sposx = (sectorindex % sectorblocks) * this.SECTORSIZE;
            var sposy = ((sectorindex / sectorblocks) | 0) * this.SECTORSIZE;
            for (var iy = 0; iy < this.SECTORSIZE && !ok; iy++) {
                for (var ix = 0; ix < this.SECTORSIZE && !ok; ix++) {
                    var ret = this.terell(ix + sposx, iy + sposy, mx, my, sposx + this.SECTORSIZE);
                    if (ret === -1) {
                        return [ix + sposx, iy + sposy];
                    } else {
                        ix = ret - sposx;
                    }
                }
            }
            if (mx < min_x && my < min_y) {
                this.sectorFoglal[sectorindex] = (my << 16) + mx;
            }
            return undefined;
        };
        var px = 0;
        var py = 0;
        var ok = false;
        for (var i = 0; i < this.sectorFoglal.length; i++) {
            var ret = this.sectorEll(i, false);
            if (ret !== undefined) {
                px = ret[0];
                py = ret[1];
                ok = true;
                break;
            }
        }


        if (ok) {
            for (var ix = px; ix < px + mx; ix++) {
                for (var iy = py; iy < py + my; iy++) {
                    this.foglalva[ix + (iy * this.sx)] = 1;
                }
            }
            return [px, py];
        }
        return undefined;
    };
    this.delElem = function (x, y, mx, my) {
        var sectorblocks = Math.ceil(this.sx / this.SECTORSIZE);
        for (var ix = x; ix < x + mx; ix += this.SECTORSIZE) {
            for (var iy = y; iy < y + my; iy += this.SECTORSIZE) {
                var sectorindex = ((ix / this.SECTORSIZE) | 0) + (((iy / this.SECTORSIZE) | 0) * sectorblocks);
                this.sectorFoglal[sectorindex] = 0xFFFFFFFF;
            }
        }
        for (var ix = x; ix < x + mx; ix++) {
            for (var iy = y; iy < y + my; iy++) {
                this.foglalva[ix + (iy * this.sx)] = 0;
            }
        }
    };
    this.clean = function () {
        this.foglalva = new Uint8Array(this.sx * this.sy);
        for (var i = 0; i < this.sectorFoglal.length; i++) {
            this.sectorFoglal[i] = 0xFFFFFFFF;
        }
    };
    this.resize(x, y);
}

function textureData(hibakeret) {

    this.hibak = hibakeret | 0; //0-1-2

    this.texsize_x;
    this.texsize_y;
    this.pixelarr;
    this.fenyarr;
    this.texintex;
    this.clean = function () {
        this.pixelarr = new d2d_array();
        this.fenyarr = new d2d_array();
        this.texsize_x = 32;
        this.texsize_y = 32;
        this.texintex = new texInTex_min(this.texsize_x, this.texsize_y);
        this.resize(this.texsize_x, this.texsize_y);
    };
    this.getPixelData = function () {
        return new Uint8Array(this.pixelarr.buffer.buffer, 0, this.pixelarr.buffer.length * 4);
    };
    this.getFenyData = function () {
        return new Uint8Array(this.fenyarr.buffer.buffer, 0, this.fenyarr.buffer.length * 4);
    };
    this.resize = function (nx, ny) {
        this.texintex.resize(nx, ny);
        this.pixelarr.resize(nx, ny);
        this.fenyarr.resize(nx, ny);
        this.texsize_x = this.pixelarr.mx;
        this.texsize_y = this.pixelarr.my;
    };
    this.delTexPos = function (texpos) {
        this.texintex.delElem(texpos[0] - this.hibak, texpos[1] - this.hibak, texpos[2] + (this.hibak * 2), texpos[3] + (this.hibak * 2));
    };
    this.getTexPos = function (xm, ym) {

        var texpos = this.texintex.addElem(xm + (this.hibak * 2), ym + (this.hibak * 2));
        if (texpos === undefined) {//nincs elég hely, resize
            var ujx = this.texsize_x;
            var ujy = this.texsize_y;
            if (ujy < ujx) {
                ujy <<= 1;
            } else
                ujx <<= 1;
            this.resize(ujx, ujy);
            return this.getTexPos(xm, ym);
        }
        texpos[0] += this.hibak;
        texpos[1] += this.hibak;
        return [texpos[0], texpos[1]];
    };
    this.setData = function (xm, ym, texpos, arr) {
        for (var i = 0; i < ym; i++) {
            var mpos = i * xm;
            var masol = arr.subarray(mpos, mpos + (xm));
            var elsopix = masol[0];
            var utolsopix = masol[xm - 1];
            this.pixelarr.setArr(texpos[0], i + texpos[1], masol);
            for (var j = 1; j <= this.hibak; j++) {
                this.pixelarr.set(texpos[0] - j, i + texpos[1], elsopix);
                this.pixelarr.set(texpos[0] + xm + (j - 1), i + texpos[1], utolsopix);
            }

            if (i === 0) {
                for (var k = 1; k <= this.hibak; k++) {
                    this.pixelarr.setArr(texpos[0], texpos[1] - k, masol);
                    for (var j = 1; j <= this.hibak; j++) {

                        this.pixelarr.set(texpos[0] - j, i + texpos[1] - k, elsopix);
                        this.pixelarr.set(texpos[0] + xm + (j - 1), i + texpos[1] - k, utolsopix);
                    }
                }
            }

            if (i === ym - 1) {
                for (var k = 1; k <= this.hibak; k++) {
                    this.pixelarr.setArr(texpos[0], i + texpos[1] + k, masol);
                    for (var j = 1; j <= this.hibak; j++) {
                        this.pixelarr.set(texpos[0] - j, i + texpos[1] + k, elsopix);
                        this.pixelarr.set(texpos[0] + xm + (j - 1), i + texpos[1] + k, utolsopix);
                    }
                }
            }
        }
    };
    this.setFeny = function (xm, ym, texpos, arr) {
        //a valós érték körül még 1 keret érték lesz, így egye kevesebb kell a hiba keretnek
        var vhiba = this.hibak - 1;
        for (var i = 0; i < ym; i++) {
            var mpos = i * xm;
            var masol = arr.subarray(mpos, mpos + (xm));
            var elsopix = masol[0];
            var utolsopix = masol[xm - 1];
            this.fenyarr.setArr(texpos[0], i + texpos[1], masol);
            for (var j = 1; j <= vhiba; j++) {
                this.fenyarr.set(texpos[0] - j, i + texpos[1], elsopix);
                this.fenyarr.set(texpos[0] + xm + (j - 1), i + texpos[1], utolsopix);
            }

            if (i === 0) {
                for (var k = 1; k <= vhiba; k++) {
                    this.fenyarr.setArr(texpos[0], texpos[1] - k, masol);
                    for (var j = 1; j <= vhiba; j++) {

                        this.fenyarr.set(texpos[0] - j, i + texpos[1] - k, elsopix);
                        this.fenyarr.set(texpos[0] + xm + (j - 1), i + texpos[1] - k, utolsopix);
                    }
                }
            }

            if (i === ym - 1) {
                for (var k = 1; k <= vhiba; k++) {
                    this.fenyarr.setArr(texpos[0], i + texpos[1] + k, masol);
                    for (var j = 1; j <= vhiba; j++) {
                        this.fenyarr.set(texpos[0] - j, i + texpos[1] + k, elsopix);
                        this.fenyarr.set(texpos[0] + xm + (j - 1), i + texpos[1] + k, utolsopix);
                    }
                }
            }
        }
    };
    this.clean();
}

function PontFeny(fenypos, color, range, globalfunc, getelemfunc) {
    this.range = range;
    this.needupd = true;
    this.fenyp = fenypos;
    this.fenyp_v = [fenypos[0] - this.range, fenypos[1] - this.range, fenypos[2] - this.range]; //val?s pos

    var r2 = this.range + this.range;
    var r22 = r2 * r2;
    this.felist = new Uint16Array(r2 * r2 * r2);
    this.realcolor_r = new Uint8Array(r2 * r2 * r2);
    this.realcolor_g = new Uint8Array(r2 * r2 * r2);
    this.realcolor_b = new Uint8Array(r2 * r2 * r2);

    this.color = color;
    this.modposlist = new dinamicArray(2000, Int32Array);
    this.torollist = new dinamicArray(2000, Int32Array);
    this.addlist = new dinamicArray(2000, Int32Array);
    this.addlist.add_4Item(this.range, this.range, this.range, this.range);
    this.setfeny = function (ix, iy, iz, f) {
        var index = ix + (iz * r2) + (iy * r2 * r2);
        this.modposlist.need(1);
        this.modposlist.buffer[this.modposlist.pos++] = index;
        this.felist[index] = f;
    };
    this.keresKozelFeny = function (vx, vy, vz) {
        var ret = [0, 0, 0, 0, 0];
        var e;
        e = this.felist[vx - 1 + (vz * r2) + (vy * r2 * r2)];
        if (e > ret[3]) {
            ret = [-1, 0, 0, e];
        }
        e = this.felist[vx + 1 + (vz * r2) + (vy * r2 * r2)];
        if (e > ret[3]) {
            ret[3] = e;
            ret = [+1, 0, 0, e];
        }
        e = this.felist[vx + (vz * r2) + ((vy - 1) * r2 * r2)];
        if (e > ret[3]) {
            ret = [0, -1, 0, e];
        }
        e = this.felist[vx + (vz * r2) + ((vy + 1) * r2 * r2)];
        if (e > ret[3]) {
            ret = [0, +1, 0, e];
        }
        e = this.felist[vx + ((vz - 1) * r2) + (vy * r2 * r2)];
        if (e > ret[3]) {
            ret = [0, 0, -1, e];
        }
        e = this.felist[vx + ((vz + 1) * r2) + (vy * r2 * r2)];
        if (e > ret[3]) {
            ret = [0, 0, +1, e];
        }
        return ret;
    };
    this.UpdateNeedRange = function (x, y, z, torles) {
        if (Math.abs(x - this.fenyp[0]) <= this.range && Math.abs(y - this.fenyp[1]) <= this.range && Math.abs(z - this.fenyp[2]) <= this.range) {
            var vx = x - this.fenyp_v[0];
            var vy = y - this.fenyp_v[1];
            var vz = z - this.fenyp_v[2];
            if (torles) {
                var ret = this.keresKozelFeny(vx, vy, vz);
                var feny = ret[3];
                if (feny > 0) {
                    var ix = vx + ret[0];
                    var iy = vy + ret[1];
                    var iz = vz + ret[2];
                    this.addlist.add_4Item(ix, iy, iz, feny);
                    this.needupd = true;
                    return true;
                }
            } else {
                var index = vx + (vz * this.range * 2) + (vy * this.range * 2 * this.range * 2);
                if (this.felist[index] !== 0) {
                    this.torollist.add_3Item(x - this.fenyp_v[0], y - this.fenyp_v[1], z - this.fenyp_v[2]);
                    this.needupd = true;
                    return true;
                }
            }
        }
        return false;
    };
    this.fenyDataApply = function (torol) {
        if (torol) {
            var fx = -1;
            var fz = 0;
            var fy = 0;
            var osszfor = this.range * 2 * this.range * 2 * this.range * 2;
            for (var ff = 0; ff < osszfor; ff++) {
                fx++;
                if (fx === this.range + this.range) {
                    fx = 0;
                    fz++;
                }
                if (fz === this.range + this.range) {
                    fz = 0;
                    fy++;
                }

                var ertek_r = this.realcolor_r[ff];
                var ertek_g = this.realcolor_g[ff];
                var ertek_b = this.realcolor_b[ff];
                globalfunc(fx + this.fenyp_v[0], fy + this.fenyp_v[1], fz + this.fenyp_v[2], -ertek_r, -ertek_g, -ertek_b);
            }
        } else {
            for (var i = 0; i < this.modposlist.pos; i++) {
                var index = this.modposlist.buffer[i];
                var ertek = this.felist[index];
                var fx = index % r2;
                var fz = ((index / r2) | 0) % r2;
                var fy = (index / r22) | 0;

                var er = this.realcolor_r[index];
                var eg = this.realcolor_g[index];
                var eb = this.realcolor_b[index];


                var ertek_r = ((this.color[0] * ertek) / this.range) | 0;
                var ertek_g = ((this.color[1] * ertek) / this.range) | 0;
                var ertek_b = ((this.color[2] * ertek) / this.range) | 0;

                this.realcolor_r[index] = ertek_r;
                this.realcolor_g[index] = ertek_g;
                this.realcolor_b[index] = ertek_b;

                globalfunc(fx + this.fenyp_v[0], fy + this.fenyp_v[1], fz + this.fenyp_v[2], ertek_r - er, ertek_g - eg, ertek_b - eb);
            }
            this.modposlist.clean();
        }
    };
    this.TorolIrany = function (px, py, pz, pe, arr) {


        var pontlist = new dinamicArray(2000, Int32Array);
        pontlist.add_4Item(px, py, pz, pe);
        var pontlist_i = 0;
        var debug = 0;
        while (debug < 200000) {
            debug++;
            if (pontlist_i >= pontlist.pos)
                break;

            var ix = pontlist.buffer[pontlist_i++];
            var iy = pontlist.buffer[pontlist_i++];
            var iz = pontlist.buffer[pontlist_i++];
            var e = pontlist.buffer[pontlist_i++];

            var index = ix + (iz * r2) + (iy * r2 * r2);
            if (this.felist[index] === 0) {
                continue;
            }
            if (e < this.felist[index]) {
                arr.add_4Item(ix, iy, iz, this.felist[index]);
                continue;
            }

            this.setfeny(ix, iy, iz, 0);
            if (e > 1) {
                pontlist.add_4Item(ix + 1, iy, iz, e - 1);
                pontlist.add_4Item(ix - 1, iy, iz, e - 1);
                pontlist.add_4Item(ix, iy + 1, iz, e - 1);
                pontlist.add_4Item(ix, iy - 1, iz, e - 1);
                pontlist.add_4Item(ix, iy, iz + 1, e - 1);
                pontlist.add_4Item(ix, iy, iz - 1, e - 1);
            }
        }
        if (debug === 200000) {
            throw "PontfenyTorol Maximum literation (200000)";

        }
    };
    this.calc = function () {
        if (this.torollist.pos > 0) {
            var addarr = new dinamicArray(2000, Int32Array);
            for (var i = 0; i < this.torollist.pos; ) {
                var vx = this.torollist.buffer[i++];
                var vy = this.torollist.buffer[i++];
                var vz = this.torollist.buffer[i++];
                var index = vx + (vz * r2) + (vy * r2 * r2);
                var er = this.felist[index];
                this.TorolIrany(vx, vy, vz, er, addarr);
            }
            this.torollist.clean();
            for (var i = 0; i < addarr.pos; ) {
                var x = addarr.buffer[i++];
                var y = addarr.buffer[i++];
                var z = addarr.buffer[i++];
                var e = addarr.buffer[i++];
                var index = x + (z * r2) + (y * r2 * r2);
                if (this.felist[index] !== 0) {
                    this.addlist.add_4Item(x, y, z, e);
                }
            }
        }
        if (this.addlist.pos > 0) {
            this.calcFeny();
            this.addlist.clean();
        }
    };
    this.calcFeny = function () {
        this.addpoint = function (x, y, z, e) {
            if (x < 0 || y < 0 || z < 0 || e <= 1)
                return;
            e -= 1;
            var index = x + (z * r2) + (y * r2 * r2);
            if (this.felist[index] >= e) {
                return;
            }
            if (getelemfunc(x + this.fenyp_v[0], y + this.fenyp_v[1], z + this.fenyp_v[2]) === 0) {
                this.addlist.add_4Item(x, y, z, e);
                this.setfeny(x, y, z, e);
            }
        };
        var realpontpos = 0;
        for (var i = 0; i < this.addlist.pos; ) {
            var x = this.addlist.buffer[i++];
            var y = this.addlist.buffer[i++];
            var z = this.addlist.buffer[i++];
            var e = this.addlist.buffer[i++];
            this.setfeny(x, y, z, e);
        }
        var debug = 0;
        while (debug < 200000) {
            debug++;
            if (realpontpos === this.addlist.pos)
                break;
            var x = this.addlist.buffer[realpontpos++];
            var y = this.addlist.buffer[realpontpos++];
            var z = this.addlist.buffer[realpontpos++];
            var e = this.addlist.buffer[realpontpos++];
            this.addpoint(x - 1, y, z, e);
            this.addpoint(x + 1, y, z, e);
            this.addpoint(x, y - 1, z, e);
            this.addpoint(x, y + 1, z, e);
            this.addpoint(x, y, z - 1, e);
            this.addpoint(x, y, z + 1, e);
        }
        if (debug === 200000) {
            throw "Pontfeny Maximum literation (200000)";

        }
    };
}
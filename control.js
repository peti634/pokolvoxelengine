PokolVoxelEngine.prototype.inputControl = function (canv, mousecb, mousewheelcb, keycallback, billdebug) {
    return new PokolVoxelEngine_inputControl(this, canv, mousecb, mousewheelcb, keycallback, billdebug);
};

PokolVoxelEngine_inputControl = function (core, mousecb, mousewheelcb, keycallback, billdebug) {
    var canv = core.canvas;
    var degToRad = core.math.degToRad;
    this.start = function () {
        mozpos = [0, 0, 0];
    };
    this.end = function () {
        this.pos[0] += mozpos[0];
        this.pos[1] += mozpos[1];
        this.pos[2] += mozpos[2];
    };
    this.MoveCam = function (fx, fy, speed) {
        mozpos[2] -= Math.cos(degToRad(fy)) * speed * Math.cos(degToRad(fx));
        mozpos[0] += Math.sin(degToRad(fy)) * speed * Math.cos(degToRad(fx));
        mozpos[1] -= Math.sin(degToRad(fx)) * speed;
    };
    this.MouseInput = function (e) {
        if (mousecallback) {
            var mousepos;
            if (e.offsetX && e.offsetY) {
                mousepos = {x: e.offsetX, y: e.offsetY};
            } else {
                var canvaspos = getElementAbsolutePos(canv);
                var hscroll = (document.all ? document.scrollLeft : window.pageXOffset);
                var vscroll = (document.all ? document.scrollTop : window.pageYOffset);
                mousepos = {x: e.clientX + hscroll - canvaspos.left, y: e.clientY + vscroll - canvaspos.top};
                if (isFullscreen()) {
                    mousepos.x *= canv.width / screen.width;
                    mousepos.y *= canv.height / screen.height;
                }

            }
            var mmovement = {x: 0, y: 0};
            if (pointerlock_old === GetPointerLock()) {//első lock-ot eldobjuk, chrome valamiért hirtelen nagy értéket ad 2 monitornál
                mmovement = GetMouseMovement(e);
            }
            pointerlock_old = GetPointerLock();
            mousecallback(e, mmovement, mousepos);

        }

    };
    this.reqFullScreen = function (canv) {
        if (canv.requestFullscreen) {
            canv.requestFullscreen();
        } else if (canv.webkitRequestFullscreen) {
            canv.webkitRequestFullscreen();
        } else if (canv.mozRequestFullScreen) {
            canv.mozRequestFullScreen();
        } else if (canv.msRequestFullscreen) {
            canv.msRequestFullscreen();
        }
    };
    this.MouseLock = function () {
        canv.requestPointerLock = canv.requestPointerLock || canv.mozRequestPointerLock || canv.webkitRequestPointerLock;
        canv.requestPointerLock();

    };
    this.InputEvent = function (e) {
        var up = false;
        var dbclick = false;
        var keypush = -1;
        var disablekey = false;
        if (e.type === "keyup" || e.type === "keydown" || e.type === "dblclick") {
            keypush = (('which' in e) ? e.which : e.keyCode) | 0;
            if (e.type === "keyup")
                up = true;
            if (e.type === "dblclick")
                dbclick = true;
        } else if (e.type === "mouseup" || e.type === "mousedown") {
            keypush = e.button;
            if (e.type === "mouseup")
                up = true;
            inputcanv.focus();

        } else {
            return;
        }

        if (KeyCap[keypush] === 1) {
            GombLe[keypush] = (up) ? 0 : 1;


        }
        if (keycallback)
            disablekey = keycallback(keypush, up, dbclick);
        if (billDebug) {
            console.info("bill: " + keypush);
        }
        this.value = '';
        if (disablekey) {
            e.preventDefault();
            return false;
        }
    };
    this.LoadInputKey = function (keyobj) {
        KeyCap = new Uint8Array(256);
        for (var i in keyobj) {
            KeyCap[keyobj[i]] = 1;

        }
    };
    this.key = function (keyindex) {
        return GombLe[keyindex] === 1;
    };
    this.MouseWheel = function (e) {
        if (mousewheelcallback) {
            var wheel = (e.wheelDelta) ? (e.wheelDelta) : (-e.detail);
            mousewheelcallback(wheel);
        }

    };
    this.SetPos = function (x, y, z) {
        this.pos[0] = x;
        this.pos[1] = y;
        this.pos[2] = z;
    };
    this.SetNezszo = function (x, y) {
        this.nezszog[0] = x;
        this.nezszog[1] = y;
    };

    var mozpos = [0, 0, 0];
    this.pos = [0, 0, 0];
    this.nezszog = [0, 0];
    this.unprojAngel = [0, 0, 0];
    var billDebug = billdebug;
    var KeyCap;
    var pointerlock_old = false;
    var GombLe = new Uint8Array(256);

    var keycallback = keycallback;
    var mousecallback = mousecb;
    var mousewheelcallback = mousewheelcb;

    this.canv = canv;

    var inputcanv = document.createElement("INPUT");
    inputcanv.id = "inputcanvas";

    inputcanv.addEventListener("keydown", this.InputEvent);
    inputcanv.addEventListener("keyup", this.InputEvent);
    inputcanv.addEventListener("blur", function () {
        for (var i = 4; i < 256; i++)
            GombLe[i] = 0;
    });
    canv.appendChild(inputcanv);
    canv.addEventListener("mousedown", this.InputEvent);
    canv.addEventListener("mouseup", this.InputEvent);
    canv.addEventListener("dblclick", this.InputEvent);
    canv.addEventListener("mousemove", this.MouseInput);
    canv.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });

    if ("onmousewheel" in canv) {
        canv.onmousewheel = this.MouseWheel;
    } else {
        canv.addEventListener('DOMMouseScroll', this.MouseWheel, false);
    }

};


function GetMouseMovement(e) {
    if (e.movementX !== undefined && e.movementY !== undefined) {//'webkitMovementY' is deprecated. Please use 'movementX/Y' instead. in chrome
        return {x: e.movementX, y: e.movementY};
    }

    return {x: e.mozMovementX || e.webkitMovementX || 0, y: e.mozMovementY || e.webkitMovementY || 0};
}

function GetPointerLock() {
    return (!!document.pointerLockElement || !!document.mozPointerLockElement || !!document.webkitPointerLockElement) === true;
}
function getElementAbsolutePos(element) {
    var top = 0, left = 0;
    do {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while (element);
    return{top: top, left: left};
}
function isFullscreen() {
    return document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
}


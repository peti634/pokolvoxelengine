/*global PokolVoxelEngine_jspath*/
/*global mat4*/
/*global vec2*/
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/voxelterrain.js'></script>");

function PokolVoxelEngine_VoxelVilag(core, settings) {
    var gl = core.ogl;
    var shaderObj;
    var terrainAtlas = core.getTerrainAtlas();
    var relativPos = [0, 0, 0];
    this.vlist = [];
    this.vlistrendez = [];
    this.m_xz = settings.voxel_terrainLengthXZ;
    this.blocksize = settings.voxel_blokSsize;
    this.campos = [0, 0, 0];
    this.indices = [];
    this.init = function () {
        loadShader();
        for (var i = 0; i < this.vlist.length; i++) {
            this.vlist[i].del();
        }
        this.vlist = [];
        this.vlistrendez = [];
        this.indices = [];
    };
    this.setRelativePos = function (rpos) {
        relativPos = rpos;
    };
    this.getRelativePos = function () {
        return relativPos;
    };
    this.getTerrain = function (x, z) {
        var index = this.indices[x + "_" + z];
        if (index !== undefined) {
            return this.vlist[index];
        }
        return undefined;

    };
    this.getTerrainPos = function (x, z) {
        var bx = x / this.m_xz | 0;
        var bz = z / this.m_xz | 0;
        var px = x % this.m_xz;
        var pz = z % this.m_xz;

        var ter = this.getTerrain(bx, bz);
        if (ter === undefined)
            return undefined;
        return [ter, px, pz];
    };
    this.getTerrainList = function () {
        var ret = [];
        for (var i = 0; i < this.vlist.length; i++) {
            var elem = this.vlist[i];
            ret.push({x: elem.px, z: elem.pz});

        }
        return ret;
    };
    this.setCampos = function (campos) {
        var relativCampos = [campos[0] + relativPos[0], campos[1] + relativPos[1], campos[2] + relativPos[2]];

        if (this.campos[0] === relativCampos[0] && this.campos[1] === relativCampos[1] && this.campos[2] === relativCampos[2])
            return;
        this.campos = [relativCampos[0], relativCampos[1], relativCampos[2]];
        var terrainm = this.m_xz * this.blocksize;
        for (var i = 0; i < this.vlistrendez.length; i++) {

            var elem = this.vlistrendez[i];
            var posx = (elem.px * terrainm) + (terrainm / 2);
            var posz = (elem.pz * terrainm) + (terrainm / 2);
            elem.tav = vec2.distance([campos[0], campos[2]], [posx, posz]);
            var tavx = Math.abs(posx - campos[0]) - (terrainm / 2);
            var tavy = Math.abs(posz - campos[2]) - (terrainm / 2);
            elem.needdraw = (tavx < settings.render_maxrange) && (tavy < settings.render_maxrange);
        }
        this.vlistrendez.sort(function (a, b) {
            return  a.tav - b.tav;
        });
    };
    this.update = function () {
        for (var i = 0; i < this.vlist.length; i++) {
            this.vlist[i].update();
        }

    };
    this.clear = function () {
        for (var i = 0; i < this.vlist.length; i++) {
            var elem = this.vlist[i];
            elem.clean();
        }
    };
    this.Draw = function (usefeny) {

        this.setCampos(gl.campos);
        if (usefeny) {
            core.shader.shaderUse(shaderObj, "voxelterrain", ["HIGHFENY", "USEFENY"]);
        } else {
            core.shader.shaderUse(shaderObj, "voxelterrain", ["HIGHFENY"]);
        }
        core.texman.update();
        terrainAtlas.activeShader();

        core.shader.shaderUniform("uNapfeny", [core.napfeny]);
        core.shader.shaderUniform("uFenyRange", [0.13, 1.4, 0.2]);


        this.setCampos(gl.campos);
        var terrainm = this.m_xz * this.blocksize;
        gl.mvPush();
        mat4.translate(gl.mvMatrix, gl.mvMatrix, relativPos);
        var drawlist = [];
        
        core.shader.shaderUniform("uRange", [settings.render_maxrange]);
        for (var i = 0; i < this.vlistrendez.length; i++) {
            var elem = this.vlistrendez[i];
            if (elem === undefined)
                continue;
            if (elem.needdraw === false) {
                continue;
            }
            drawlist.push({elem: elem, x: elem.px * terrainm, z: elem.pz * terrainm});

        }
        



        for (var i = 0; i < drawlist.length; i++) {
            var elem = drawlist[i];
            gl.mvPush();



            mat4.translate(gl.mvMatrix, gl.mvMatrix, [elem.x, 0, elem.z]);
            gl.upload_mvMatrix();

            elem.elem.Draw(1, 1);


            gl.mvPop();
        }
        for (var i = 0; i < drawlist.length; i++) {
            var elem = drawlist[i];
            gl.mvPush();



            mat4.translate(gl.mvMatrix, gl.mvMatrix, [elem.x, 0, elem.z]);
            gl.upload_mvMatrix();

            var alpha = 0.8;

            elem.elem.Draw(2, alpha);

            gl.mvPop();
        }

        for (var i = 0; i < drawlist.length; i++) {
            var elem = drawlist[i];
            gl.mvPush();



            mat4.translate(gl.mvMatrix, gl.mvMatrix, [elem.x, 0, elem.z]);
            gl.upload_mvMatrix();

            elem.elem.Draw(-1);

            gl.mvPop();
        }
        gl.mvPop();
        terrainAtlas.unActiveShader();

        gl.disable(gl.BLEND);
        
    };

    this.addElem = function (x, y, z, e, data) {
        //console.info(x + " _ " + y + " _ " + z + " _ " + e);
        var ret = this.getTerrainPos(x, z);
        if (ret)
            ret[0].addElem(ret[1], y, ret[2], e, data);

    };
    this.addFeny = function (pos, color, range) {
        var ret = this.getTerrainPos(pos[0], pos[2]);
        if (ret)
            ret[0].addFeny([ret[1], pos[1], ret[2]], color, range);

    };

    this.delElem = function (x, y, z) {
        var ret = this.getTerrainPos(x, z);
        if (ret)
            ret[0].delElem(ret[1], y, ret[2]);

    };
    this.getElem = function (x, y, z) {
        var ret = this.getTerrainPos(x, z);
        if (ret)
            return ret[0].getElem(ret[1], y, ret[2]);
        return 0;
    };
    this.oszlopUpd = function (x, z, arr) {

        var ret = this.getTerrainPos(x, z);
        if (ret)
            ret[0].oszlopUpd(ret[1], ret[2], arr);
    };
    this.addTerrain = function (x, z) {
        if (this.getTerrain(x, z))
            return;
        this.worker.postMessage({type: "NEWDATA", px: x, pz: z, mxz: this.m_xz, blocksize: this.blocksize});
        var ujelem = new PokolVoxelEngine_TerrainVoxel(core, x, z, this.m_xz, this.blocksize, this);

        var index = this.vlist.push(ujelem) - 1;
        this.indices[x + "_" + z] = index;
        this.vlistrendez.push(ujelem);
        //force
        this.campos = [undefined, undefined, undefined];
        this.setCampos(gl.campos);


    };
    this.rayPick = function () {

        this.setCampos(gl.campos);
        var campos = [gl.campos[0] - relativPos[0], gl.campos[1] - relativPos[1], gl.campos[2] - relativPos[2]];

        var camAngle = gl.unprojAngel;
        var raypick = new core.math.rayPickGeometry(campos, camAngle);

        var terrainm = this.m_xz * this.blocksize;


        var ret = [0, 0, 0, -1];
        for (var i = 0; i < this.vlistrendez.length; i++) {
            var elem = this.vlistrendez[i];
            if (elem === undefined)
                continue;
            var cpos = [elem.px * terrainm, elem.heightmin * this.blocksize, elem.pz * terrainm];

            if (!raypick.box(cpos, terrainm + this.blocksize, (elem.heightmax - elem.heightmin) * this.blocksize, terrainm + this.blocksize))
                continue;
            cpos[0] = campos[0] - cpos[0];
            cpos[1] = campos[1];
            cpos[2] = campos[2] - cpos[2];

            var boxpos = elem.rayPick(cpos, camAngle);

            if (boxpos[3] !== -1) {
                ret[0] = boxpos[0] + (elem.px * this.m_xz);
                ret[1] = boxpos[1];
                ret[2] = boxpos[2] + (elem.pz * this.m_xz);
                ret[3] = boxpos[3];
                return ret;
            }
        }
        return ret;
    };

    this.onmessage = function (data) {
        var elem = this.getTerrain(data.px, data.pz);
        if (elem) {
            elem.onmessage(data);
        } else {
            throw "No voxel terrain in voxelvilag onmessage";
        }

    };
    this.sendworker = function (px, pz, msg) {
        msg.px = px;
        msg.pz = pz;
        this.worker.postMessage(msg);
    };

    function genrandstr()
    {
        return "";
        var ret = "";
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
            ret += chars.charAt(Math.floor(Math.random() * chars.length));

        return ret;
    }

    //worker init
    this.worker = new Worker(PokolVoxelEngine_jspath + "/worker_voxel.js?rnd=" + genrandstr());
    this.worker.onmessage = function (e) {
        this.parent.onmessage(e.data);
    };
    this.worker.parent = this;


    //init
    var initmsg = {};
    initmsg.type = "INIT";
    initmsg.mxz = settings.voxel_terrainLengthXZ;
    initmsg.napfenyrange = settings.voxel_napfenyRange;
    initmsg.pontfeny_maxrange = settings.voxel_pontfenyMaxRange;
    initmsg.oldalfenyarr = settings.voxel_blokkOldalFeny;
    initmsg.blocksize = settings.voxel_blokSsize;
    initmsg.blocktypelist = settings.blokkTypeList;
    initmsg.csoplist = settings.blokkCsoportList;
    initmsg.blocksize = this.blocksize;

    this.worker.postMessage(initmsg);

    var loadShader = function () {

        var fragment =
                "varying highp vec4 vTexcoord; \n" +
                "varying highp vec2 vVertexPos; \n" +
                "varying highp float vOldal; \n" +
                "uniform highp float uAlpha; \n" +
                "uniform sampler2D uTexdata; \n" +
                "uniform sampler2D uFeny; \n" +
                "uniform highp vec2 uSamplerMeret; \n" +
                "uniform highp float uRange; \n" +
                "uniform highp float uId; \n" +
                "uniform highp float uNapfeny; \n" +
                "uniform highp vec3 uFenyRange; \n" +
                core.texman.TexAtlas.getFragmentShader("getTex") + " \n" +
                "void main(void) { \n" +
                "    if (any(greaterThan(abs(vVertexPos), vec2(uRange)))) discard; \n" +
                "    highp vec2 texcoord = vTexcoord.xy - mod(vTexcoord.xy,uSamplerMeret) + (uSamplerMeret / 2.0); \n" +
                "    highp vec4 szin = texture2D(uTexdata, texcoord); \n" +
                "    highp float texX = floor(szin.r *255.0 + 0.5); \n" +
                "    highp float texY = floor(szin.g *255.0 + 0.5); \n" +
                "    highp vec3 texszin = getTex(texX,texY, vTexcoord.zw).rgb; \n" +
                "    highp vec2 arany = mod(vTexcoord.zw,1.0); \n" +
                "    highp vec4 smfeny = vec4(0.0); \n" +
                "    highp vec4 feny = texture2D(uFeny, texcoord); \n" +
                "    feny.a = max(uFenyRange.x,feny.a * uNapfeny); \n" +
                "    #ifdef HIGHFENY \n" +
                "        highp vec4 abfeny = feny * uFenyRange.z; \n" +
                "        highp vec4 fenyx1 = texture2D(uFeny, texcoord + vec2(-uSamplerMeret.x,0.0));  \n" +
                "        highp vec4 fenyx2 = texture2D(uFeny, texcoord + vec2(uSamplerMeret.x,0.0)); \n" +
                "        highp vec4 fenyy1 = texture2D(uFeny, texcoord + vec2(0.0,-uSamplerMeret.y)); \n" +
                "        highp vec4 fenyy2 = texture2D(uFeny, texcoord + vec2(0.0,uSamplerMeret.y)); \n" +
                "        fenyx1 = (fenyx1.a == 0.0) ? abfeny : vec4(fenyx1.rgb,max(uFenyRange.x,fenyx1.a * uNapfeny)); \n" +
                "        fenyx2 = (fenyx2.a == 0.0) ? abfeny : vec4(fenyx2.rgb,max(uFenyRange.x,fenyx2.a * uNapfeny)); \n" +
                "        fenyy1 = (fenyy1.a == 0.0) ? abfeny : vec4(fenyy1.rgb,max(uFenyRange.x,fenyy1.a * uNapfeny)); \n" +
                "        fenyy2 = (fenyy2.a == 0.0) ? abfeny : vec4(fenyy2.rgb,max(uFenyRange.x,fenyy2.a * uNapfeny)); \n" +
                "        highp vec4 fenyx1y1 = texture2D(uFeny, texcoord + vec2(-uSamplerMeret.x,-uSamplerMeret.y)); \n" +
                "        highp vec4 fenyx1y2 = texture2D(uFeny, texcoord + vec2(-uSamplerMeret.x,uSamplerMeret.y)); \n" +
                "        highp vec4 fenyx2y1 = texture2D(uFeny, texcoord + vec2(uSamplerMeret.x,-uSamplerMeret.y)); \n" +
                "        highp vec4 fenyx2y2 = texture2D(uFeny, texcoord + vec2(uSamplerMeret.x,uSamplerMeret.y)); \n" +
                "        fenyx1y1 = (fenyx1y1.a == 0.0) ? abfeny : vec4(fenyx1y1.rgb,max(uFenyRange.x,fenyx1y1.a * uNapfeny)); \n" +
                "        fenyx1y2 = (fenyx1y2.a == 0.0) ? abfeny : vec4(fenyx1y2.rgb,max(uFenyRange.x,fenyx1y2.a * uNapfeny)); \n" +
                "        fenyx2y1 = (fenyx2y1.a == 0.0) ? abfeny : vec4(fenyx2y1.rgb,max(uFenyRange.x,fenyx2y1.a * uNapfeny)); \n" +
                "        fenyx2y2 = (fenyx2y2.a == 0.0) ? abfeny : vec4(fenyx2y2.rgb,max(uFenyRange.x,fenyx2y2.a * uNapfeny)); \n" +
                "        highp vec4 atlagx1y1 = (feny + fenyx1y1 + fenyx1 + fenyy1) /4.0; \n" +
                "        highp vec4 atlagx2y1 = (feny + fenyx2y1 + fenyx2 + fenyy1) /4.0; \n" +
                "        highp vec4 atlagx1y2 = (feny + fenyx1y2 + fenyx1 + fenyy2) /4.0; \n" +
                "        highp vec4 atlagx2y2 = (feny + fenyx2y2 + fenyx2 + fenyy2) /4.0; \n" +
                "        smfeny += (fenyx1+feny)* max(0.0,-arany.x + 0.5) * (1.0 - abs(arany.y -0.5)*2.0); \n" +
                "        smfeny += (fenyx2+feny)* max(0.0,arany.x - 0.5)* (1.0-(abs(arany.y - 0.5)*2.0)); \n" +
                "        smfeny += (fenyy1+feny)* max(0.0,-arany.y + 0.5)* (1.0 - abs(arany.x -0.5)*2.0); \n" +
                "        smfeny += (fenyy2+feny)* max(0.0,arany.y - 0.5)* (1.0-(abs(arany.x - 0.5)*2.0)); \n" +
                "        smfeny += atlagx1y1 * max(0.0,1.0-arany.x *2.0) * max(0.0,1.0-arany.y *2.0); \n" +
                "        smfeny += atlagx2y1 * max(0.0,arany.x *2.0-1.0) * max(0.0,1.0-arany.y *2.0); \n" +
                "        smfeny += atlagx1y2 * max(0.0,1.0-arany.x *2.0) * max(0.0,arany.y *2.0-1.0); \n" +
                "        smfeny += atlagx2y2 * max(0.0,arany.x *2.0-1.0) * max(0.0,arany.y *2.0-1.0); \n" +
                "        smfeny += feny * (1.0 - abs(1.0-arany.x*2.0)) * (1.0 - abs(1.0-arany.y*2.0)); \n" +
                "        /* MEDIUM \n" +
                "        smfeny += atlagx1y1 * (1.0-arany.x) * (1.0-arany.y); \n" +
                "        smfeny += atlagx2y1 * (arany.x) * (1.0-arany.y); \n" +
                "        smfeny += atlagx1y2 * (1.0-arany.x) * (arany.y); \n" +
                "        smfeny += atlagx2y2 * (arany.x) * (arany.y); \n" +
                "         */ \n" +
                "    #else \n" +
                "        smfeny = feny; \n" +
                "    #endif \n" +
                "    highp float vRangeMax = uFenyRange.y-1.0; \n" +
                "    highp float vNapfeny = smfeny.a; \n" +
                "    highp float vOsszNapfeny = (1.0-vNapfeny)*(1.0-vRangeMax)+vRangeMax; \n" +
                "    #ifdef USEFENY \n" +
                "        highp vec3 vfeny = vec3(vNapfeny) + (smfeny.rgb * vOsszNapfeny); \n" +
                "    #else \n" +
                "        highp vec3 vfeny = vec3(1.0); \n" +
                "    #endif \n" +
                "    gl_FragColor = vec4(vec3(texszin* vOldal * vfeny),uAlpha);   \n" +
                "}";


        var vertex =
                "attribute vec4 aVertexPosition; \n" +
                "attribute vec4 aTexcoord; \n" +
                "uniform mat4 uMVMatrix; \n" +
                "uniform mat4 uCMatrix; \n" +
                "uniform mat4 uPMatrix; \n" +
                "varying highp vec4 vTexcoord; \n" +
                "varying highp vec2 vVertexPos; \n" +
                "varying highp float vOldal; \n" +
                "uniform vec3 uCamPos; \n" +
                "void main(void) { \n" +
                "   gl_Position = uPMatrix * (uCMatrix * uMVMatrix) * vec4(aVertexPosition.xyz, 1.0); \n" +
                "   vOldal = aVertexPosition.w; \n" +
                "   vTexcoord = aTexcoord; \n" +
                "   vVertexPos = aVertexPosition.xz + vec2(uMVMatrix[3][0] - uCamPos.x,uMVMatrix[3][2] - uCamPos.z); \n" +
                "}";

        shaderObj = core.shader.loadShader(fragment, vertex, "voxelterrain", ["HIGHFENY", "USEFENY"]);
    };
    this.init();
}
/*global mat4*/
/*global vec3*/
/*global quat, NaN*/

//Jv+ F2 B+ SZ2 Bv+ Sz2 J2+ F2 J+ F2 Jv+ F2 SZ2 J2 Sz2
function PokolVoxelEngine_animator(core, settings)
{
    var gl = core.ogl;
    var _this = this;

    var boneAnimTexture = core.texman.getfreetex(gl.NEAREST, gl.NEAREST);
    var animatorClipList = [];
    this.animatorControllerList = [];

    this.Animator_Mesh = function (name, node) {
        this.name = name;
        this.node = node;
        this.bonelist = [];
        this.controll_info;
        this.geo;
        this.effect;
    };

    this.Animator_Node = function (id, name, parent, m) {
        this.id = id;
        this.name = name;
        this.childList = [];
        this.parent = parent;
        this.matrix = m;
        var localMatrix = undefined;
        var globalMatrix = undefined;

        this.calcGlobalMatrix = function () {
            if (globalMatrix === undefined) {
                var mat;
                if (localMatrix) {
                    mat = mat4.clone(localMatrix);
                } else {
                    mat = mat4.clone(this.matrix);
                }
                var node = this.parent;
                if (node !== undefined) {
                    mat4.multiply(mat, mat, parent.calcGlobalMatrix());
                }
                globalMatrix = mat;
            }
            return globalMatrix;
        };
        this.getMatrix = function () {
            var mat = mat4.clone(this.matrix);
            var node = this.parent;
            if (node !== undefined) {
                mat4.multiply(mat, mat, parent.getMatrix());
            }
            return mat;
        };
        this.setLocalMatrix = function (mat) {
            localMatrix = mat;
            globalMatrix = undefined;
        };
    };


    this.calcInputMatrix_Time = function (channel, time) {
        if (channel === undefined || channel.input_list === undefined) {
            return undefined;
        }
        var len = channel.input_list.length;
        if (len === 0) {
            return undefined;
        } else if (len === 1) {
            return {framestep: 0, mStart: channel.input_list[0].transform, mEnd: channel.input_list[0].transform};
        } else {
            for (var fi = 0; fi < channel.input_list.length - 1; fi++) {
                var inputitemS = channel.input_list[fi];
                var inputitemE = channel.input_list[fi + 1];
                if (inputitemS.time <= time && time <= inputitemE.time) {
                    var diff = inputitemE.time - inputitemS.time;
                    var framestep = (time - inputitemS.time) / diff;
                    var mStart = inputitemS.transform;
                    var mEnd = inputitemE.transform;
                    return {framestep: framestep, mStart: mStart, mEnd: mEnd};

                }
            }
            if (time < channel.input_list[0].time) {
                return {framestep: 0, mStart: channel.input_list[0].transform, mEnd: channel.input_list[0].transform};
            }
            if (time > channel.input_list[len - 1].time) {
                return {framestep: 1, mStart: channel.input_list[len - 1].transform, mEnd: channel.input_list[len - 1].transform};
            }
        }
        return undefined;
    };

    this.calcAnimMatrix = function (group, node_id, m) {
        var animobj = group.animClipList[group.animClipIndex];
        if (animobj === undefined) {
            return false;
        }
        var achannel = animobj.animInput.channel_list[node_id];
        if (achannel === undefined) {
            return false;
        }
        var gtime = animobj.getTime(group.time);
        var channelret = this.calcInputMatrix_Time(achannel, gtime);
        if (channelret === undefined) {
            //alert("err");
            return false;
        }
        core.math.interpolMatrix(channelret.mStart, channelret.mEnd, channelret.framestep, m);
        if (group.oldTranslateList && group.oldTranslateList[node_id] !== undefined) {
            var mStart = group.oldTranslateList[node_id];
            core.math.interpolMatrix(mStart, m, group.changeTime / group.changeDur, m);
        }
        return true;
    };
    function onlyShowBoneVertex(mesh, onlyShowBoneVertexList) {
        //gyorsíthatók a tömbök nagyrészben (array helyett uintarray)
        //többféle döntés hozható a bone törlésénél pl: del_0 || del_1 || del_2 vagyis:
        //ha bármelyik bone a triangle 3 pontjánál nem kerül mentére, akkor törlődik az adott triangle
        var saveBoneIDList = [];
        function isDelBone(dindex) {
            var cobj = mesh.controll_info[dindex];
            for (var j = 0; j < cobj.jarr.length; j++) {
                var bi = cobj.jarr[j];
                if (saveBoneIDList[bi]) {
                    return false;
                }
            }
            return true;
        }
        if (onlyShowBoneVertexList === undefined) {
            return mesh;
        }
        var geo = mesh.geo;
        for (var i = 0; i < mesh.bonelist.length; i++) {
            var save = false;
            var bone = mesh.bonelist[i];
            for (var j = 0; j < onlyShowBoneVertexList.length; j++) {
                if (bone.nodeObj.name === onlyShowBoneVertexList[j].name) {
                    save = true;
                    break;
                }
            }
            saveBoneIDList[i] = save;
        }
        var nanDataPos = new Uint8Array(geo.dataPos.length);
        for (var i = 0; i < geo.dataPosIndex.length; i += 3) {
            var gindex = geo.index[i] * 3;
            var dindex = geo.dataPosIndex[i];
            var del_0 = isDelBone(geo.dataPosIndex[i]);
            var del_1 = isDelBone(geo.dataPosIndex[i + 1]);
            var del_2 = isDelBone(geo.dataPosIndex[i + 2]);
            if (del_0 || del_1 || del_2) {
                nanDataPos[gindex + 0] = 1;
                nanDataPos[gindex + 1] = 1;
                nanDataPos[gindex + 2] = 1;
                nanDataPos[gindex + 3] = 1;
                nanDataPos[gindex + 4] = 1;
                nanDataPos[gindex + 5] = 1;
                nanDataPos[gindex + 6] = 1;
                nanDataPos[gindex + 7] = 1;
                nanDataPos[gindex + 8] = 1;

            }
        }
        var modifyDataIndexArr = [];
        var modifyIndexArr = [];
        var modifyIndex = 0;
        var modifyCounter = 0;
        var newDataPos = [];
        var newDataNorm = [];
        var newdataTexcoord = [];
        var newdataPosIndex = [];
        var newControlInfo = [];
        var newindex = [];


        for (var i = 0; i < geo.dataPos.length; i += 3) {
            if (nanDataPos[i + 0] === 1) {
                modifyIndex++;
                modifyIndexArr[i] = -1;
            } else {
                modifyIndexArr[i] = modifyIndex;
                newDataPos[modifyCounter + 0] = geo.dataPos[i + 0];
                newDataPos[modifyCounter + 1] = geo.dataPos[i + 1];
                newDataPos[modifyCounter + 2] = geo.dataPos[i + 2];

                newDataNorm[modifyCounter + 0] = geo.dataNorm[i + 0];
                newDataNorm[modifyCounter + 1] = geo.dataNorm[i + 1];
                newDataNorm[modifyCounter + 2] = geo.dataNorm[i + 2];

                if (geo.dataTexcoord && geo.dataTexcoord.length > 0) {
                    newdataTexcoord[modifyCounter + 0] = geo.dataTexcoord[i + 0];
                    newdataTexcoord[modifyCounter + 1] = geo.dataTexcoord[i + 1];
                    newdataTexcoord[modifyCounter + 2] = geo.dataTexcoord[i + 2];
                }
                modifyCounter += 3;
                var dindex = geo.dataPosIndex[i / 3];
                if (modifyDataIndexArr[dindex] === undefined) {
                    var ditem = mesh.controll_info[dindex];
                    newControlInfo.push(ditem);
                    var newdataindex = newControlInfo.length - 1;
                    modifyDataIndexArr[dindex] = newdataindex;
                }
            }
        }

        for (var i = 0; i < geo.dataPosIndex.length; i++) {
            if (modifyIndexArr[i * 3] !== -1) {
                newindex.push(geo.index[i] - modifyIndexArr[i * 3]);
                newdataPosIndex.push(modifyDataIndexArr[geo.dataPosIndex[i]]);
            }
        }
        var newmesh = new _this.Animator_Mesh(mesh.name, mesh.node);
        var newgeo = {};

        newgeo.dataPos = newDataPos;
        newgeo.dataNorm = newDataNorm;
        newgeo.index = newindex;
        newgeo.dataPosIndex = newdataPosIndex;
        newgeo.dataTexcoord = newdataTexcoord;

        newmesh.geo = newgeo;
        newmesh.controll_info = newControlInfo;
        newmesh.bonelist = mesh.bonelist;

        return newmesh;
    }

    this.addAnimatorClipList = function (animDataName, animInput, settings) {
        if (animatorClipList[animDataName] !== undefined)
            throw "ERROR, the " + animDataName + " animObjName is exist";
        var animobj = {
            name: animDataName,
            animInput: animInput.clone(),
            timeoffset: (settings.timeoffset === undefined) ? 0 : settings.timeoffset,
            repeat: (settings.repeat === undefined) ? true : settings.repeat,
            speed: (settings.speed === undefined) ? 1 : settings.speed,
            backward: (settings.backward === undefined) ? false : settings.backward, //jelenleg nem használható
            duration: animInput.duration,
            getTime: function (time) {
                if (time >= this.duration && this.repeat === false) {
                    return this.duration;

                } else {
                    return (time + (this.duration * this.timeoffset)) % this.duration;
                }
            },
            addTime: function (time, add) {
                add *= this.speed;
                var time = time + add;
                if (time >= this.duration && this.repeat === false) {
                    time = this.duration;
                } else {
                    time %= this.duration;
                }
                return time;
            }
        };
        animatorClipList[animDataName] = animobj;
    };
    //animatorAnimDataList kezelése (del,get,stb...)
    this.getAnimatorClipDuraction = function (animDataName) {
        var animobj = animatorClipList[animDataName];
        return animobj.duration;
    };


    var animatorController = function (controllerName, modell_item, meshindex, groupNameList, onlyShowBoneVertexList) {
        this.controllerName = controllerName;
        this.mesh;
        this.animGeometry = new core.geometry.geometryObject();
        this.attributeIndex;
        this.attributeWeight;
        this.node_list = [];
        this.grouplist = [];
        this.boneGroupList = [];
        function init(_this_aI) {

            if (meshindex < 0 || modell_item.meshlist.length <= meshindex) {
                throw "Mesh index error";
            }
            _this_aI.mesh = modell_item.meshlist[meshindex];
            _this_aI.node_list = modell_item.node_list;

            //bone sets
            if (_this_aI.mesh.bonelist.length > 0) {
                _this_aI.mesh = onlyShowBoneVertex(_this_aI.mesh, onlyShowBoneVertexList);
                var g = _this_aI.mesh.geo;
                _this_aI.animGeometry.setVertexData(g.dataPos);
                _this_aI.animGeometry.setNormalData(g.dataNorm);
                _this_aI.animGeometry.setTexcoordData((g.dataTexcoord && g.dataTexcoord.length > 0) ? (g.dataTexcoord.concat()) : (undefined));
                _this_aI.animGeometry.setElementData(g.index);


                var datalen = 4;
                var weightsArr = new Float32Array(datalen * g.dataPosIndex.length);
                var indexArr = new Float32Array(datalen * g.dataPosIndex.length);
                var ArrIndex = 0;

                var identityIndex = _this_aI.mesh.bonelist.length;
                var powlen = Math.pow(2, Math.ceil(Math.log(_this_aI.mesh.bonelist.length + 1) / Math.log(2)));
                for (var i = 0; i < g.dataPosIndex.length; i++) {
                    var posindex = g.dataPosIndex[i];
                    var cobj = _this_aI.mesh.controll_info[posindex];
                    var sorted = [];
                    for (var dindex = 0; dindex < cobj.warr.length; dindex++) {
                        sorted.push({index: dindex, value: cobj.warr[dindex]});
                    }

                    sorted.sort(function (a, b) {
                        return (a.value > b.value) ? -1 : 1;
                    });
                    var osszw = 0;
                    for (var dindex = 0; dindex < datalen; dindex++) {
                        if (dindex < cobj.jarr.length) {
                            var sortedIndex = sorted[dindex].index;
                            var bi = cobj.jarr[sortedIndex];
                            var w = cobj.warr[sortedIndex];
                            indexArr[ArrIndex] = (bi / powlen) + (0.5 / powlen);
                            weightsArr[ArrIndex] = w;
                            ArrIndex++;
                            osszw += w;
                        } else {
                            indexArr[ArrIndex] = (identityIndex / powlen) + (0.5 / powlen);
                            weightsArr[ArrIndex] = 0;
                            ArrIndex++;
                        }
                    }
                    for (var dindex = 0; dindex < datalen; dindex++) {
                        var index = ArrIndex - datalen + dindex;
                        weightsArr[index] *= 1 / osszw;
                    }
                }
                _this_aI.attributeIndex = new core.shader.attributeObject("aBoneMatIndex", indexArr);
                _this_aI.attributeWeight = new core.shader.attributeObject("aWeights", weightsArr);
            }
            //Group add
            for (var i = 0; i < groupNameList.length; i++) {
                _this_aI.grouplist[groupNameList[i]] = {
                    nev: groupNameList[i],
                    animClipList: [],
                    time: 0,
                    animClipIndex: 0,
                    syncGroup: undefined,
                    //csak ha új, és régi animácíó közötti interpoláció
                    oldTranslateList: undefined,
                    changeDur: 0,
                    changeTime: 0,
                    clone: function () {
                        var newg = {};
                        for (var i in this) {
                            newg[i] = this[i];
                        }
                        return newg;
                    }
                };
            }
        }

        this.setObject3D = function (obj3d) {
            if (this.mesh.effect && this.mesh.effect.img && this.mesh.effect.img.texobj) {
                obj3d.setTexture(this.mesh.effect.img.texobj);
            }
            obj3d.insertExternalUniform(0, "uUseAnim");
            if (this.mesh.bonelist.length > 0) {
                obj3d.insertExternalAttribute(0, this.attributeIndex);
                obj3d.insertExternalAttribute(1, this.attributeWeight);

                obj3d.insertExternalTextures(0, "uBoneMat", boneAnimTexture);
                obj3d.setExternalUniform(0, [1]);
            } else {
                obj3d.insertExternalAttribute(0, new core.shader.attributeObject("aBoneMatIndex", undefined));
                obj3d.insertExternalAttribute(1, new core.shader.attributeObject("aWeights", undefined));

                obj3d.setExternalUniform(0, [0]);
            }

        };
        this.rebuild = function (obj3d, propList) {
            var grouplistP = propList.groupList;
            if (this.mesh.bonelist.length > 0) {
                var powlen = Math.pow(2, Math.ceil(Math.log(this.mesh.bonelist.length + 1) / Math.log(2)));
                var nodematlist = [];
                for (var i = 0; i < this.mesh.bonelist.length; i++) {
                    nodematlist[i] = mat4.create();
                    var bone = this.mesh.bonelist[i];
                    var node = bone.nodeObj;

                    var calcm = propList.calculatedBoneMatrix[i];
                    mat4.mul(nodematlist[i], calcm, nodematlist[i]);
                    mat4.mul(nodematlist[i], bone.offsetMatrix, nodematlist[i]);
                    mat4.transpose(nodematlist[i], nodematlist[i]);
                }
                nodematlist[this.mesh.bonelist.length] = mat4.create();//lest identity
                var matrixDataList = new Float32Array(powlen * 16);
                var matrixDataListIndex = 0;
                for (var i = 0; i < nodematlist.length; i++) {
                    var nodemat = nodematlist[i];
                    for (var j = 0; j < nodemat.length; j++) {
                        matrixDataList[matrixDataListIndex++] = nodemat[j];
                    }
                }
                core.texman.bindTexture(boneAnimTexture);
                gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 4, powlen, 0, gl.RGBA, gl.FLOAT, matrixDataList);
            } else {
                var m = mat4.create();
                mat4.transpose(m, this.mesh.node.matrix);
                mat4.mul(gl.mvMatrix, gl.mvMatrix, m);
            }
        };
        this.calcAnim = function (obj3d, propList, steptime) {
            var mcache = mat4.create();
            var grouplistP = propList.groupList;
            for (var i in grouplistP) {
                var g = grouplistP[i];
                if (g.animClipList !== undefined && g.animClipList.length > 0) {
                    var anim = g.animClipList[g.animClipIndex];
                    if (anim === undefined) {
                        continue;
                    }
                    if (g.oldTranslateList !== undefined) {
                        g.changeTime += steptime;
                        if (g.changeTime > g.changeDur) {
                            g.oldTranslateList = undefined;
                            g.changeTime = 0;
                        }
                    }
                    if (g.syncGroup) {
                        g.time = grouplistP[g.syncGroup].time;
                    } else {
                        g.time = anim.addTime(g.time, steptime);
                    }
                }
            }

            if (this.mesh.bonelist.length > 0) {
                for (var i = 0; i < this.mesh.bonelist.length; i++) {
                    var bone = this.mesh.bonelist[i];
                    var node = bone.nodeObj;
                    var gname = this.boneGroupList[node.name];
                    var group = grouplistP[gname];
                    if (group === undefined) {
                        node.setLocalMatrix(undefined);
                    } else {
                        var m = mat4.create();
                        if (_this.calcAnimMatrix(group, node.id, m))
                            node.setLocalMatrix(m);
                    }
                    var calcm = mat4.clone(bone.nodeObj.calcGlobalMatrix());
                    propList.calculatedBoneMatrix[i] = calcm;
                }

                for (var i = 0; i < this.mesh.bonelist.length; i++) {
                    var bone = this.mesh.bonelist[i];
                    var node = bone.nodeObj;
                    for (var childId in propList.childrenListBone) {
                        var nodename = propList.childrenListBone[childId];
                        if (nodename === node.name) {
                            var child = propList.childrenList[childId];
                            var m = mat4.create();

                            mat4.mul(m, m, mat4.transpose(mcache, propList.calculatedBoneMatrix[i]));
                            mat4.mul(m, obj3d.getTranslateMatrix(mat4.identity(mcache)), m);

                            child.setParentMatrix(m);
                        }
                    }

                }

            }
        };
        this.getBoneList = function () {
            var ret = [];
            for (var i = 0; i < this.mesh.bonelist.length; i++) {
                ret[i] = this.mesh.bonelist[i].nodeObj.name;
            }
            return ret;
        };
        this.setGroupAnimClipIndex = function (grouplistP, gname, index) {
            var g = grouplistP[gname];
            g.animClipIndex = index;
            g.time = 0;
        };
        this.setGroupAnimClipIndexInterpolation = function (grouplistP, gname, index, userDuration) {
            var g = grouplistP[gname];
            if (g.oldTranslateList === undefined) {
                g.oldTranslateList = [];
            }
            var nowAnimClip = g.animClipList[g.animClipIndex];
            var gtime = (g.time + (nowAnimClip.animInput.duration * nowAnimClip.timeoffset)) % nowAnimClip.animInput.duration;
            for (var i in nowAnimClip.animInput.channel_list) {
                var chn = nowAnimClip.animInput.channel_list[i];
                var channelret = _this.calcInputMatrix_Time(chn, gtime);

                if (channelret === undefined) {
                    //alert("err");
                    continue;
                }

                var m = mat4.create();
                if (_this.calcAnimMatrix(g, i, m))
                    g.oldTranslateList[i] = m;
                else
                    g.oldTranslateList[i] = undefined;
            }
            g.animClipIndex = index;
            g.changeTime = 0;
            g.changeDur = userDuration;
        };
        this.setGroupSyncGroup = function (gname, syncgname) {
            this.grouplist[gname].syncGroup = syncgname;
        };
        this.setGroupAnimClip = function (gname, index, animname) {
            var g = this.grouplist[gname];
            g.animClipList[index] = animatorClipList[animname];
            //nincs group frissítés, mivel az animClipList referencia tömb
        };
        this.setBoneGroup = function (bonename, gname) {
            if (this.node_list[bonename] === undefined) {
                throw "Anim Node not found (" + bonename + ")";
            }
            this.boneGroupList[bonename] = gname;
        };
        this.clonePropList = function () {
            var groupList = [];
            for (var i in this.grouplist) {
                groupList[i] = this.grouplist[i].clone();
            }

            return {
                groupList: groupList,
                calculatedBoneMatrix: [], //mivel minden calc hívásnál felülírjuk a Node local mátrixát, ezért el kell mentenünk a kirajzoláshoz
                childrenList: [], //itt fogjuk tárolni milyen childek tartoznak a modellhez
                childrenListBone: []//itt pedig hogy 1-1 child milyen Node-hoz
            };
        };

        init(this);

    };
    this.addAnimatorController = function (controllerName, modell_item, meshindex, groupNameList, onlyShowBoneVertexList) {
        var animCtl = new animatorController(controllerName, modell_item, meshindex, groupNameList, onlyShowBoneVertexList);
        this.animatorControllerList[controllerName] = animCtl;
        return animCtl;
    };
    this.getAnimatorController = function (controllerName) {
        return this.animatorControllerList[controllerName];
    };
}
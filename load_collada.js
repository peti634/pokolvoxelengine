/*global PokolVoxelEngine*/
/*global mat4*/
/*global vec3*/
PokolVoxelEngine.prototype.load.collada = function (core, settings, data)
{
    function AnimObj() {
        this.duration = 0;
        this.channel_list = [];
        this.timeoffset = 0;
        this.cleanChannals = function (withoutChannalList) {
            for (var i in this.channel_list) {
                var isDelete = true;

                for (var k = 0; k < withoutChannalList.length; k++) {
                    var citem = withoutChannalList[k];
                    if (i === citem) {
                        isDelete = false;
                        break;
                    } else if (this.channel_list[i].target !== undefined) {
                        var target_node = this.channel_list[i].target;
                        if (target_node.id === citem || target_node.name === citem) {
                            isDelete = false;
                            break;
                        }
                    }
                }
                if (isDelete) {
                    delete this.channel_list[i];
                }
            }
        };
        this.clone = function () {
            var ret = {
                duration: this.duration,
                timeoffset: this.timeoffset,
                channel_list: [],
                clone: this.clone,
                cleanChannals: this.cleanChannals
            };
            for (var i in this.channel_list) {
                var item = this.channel_list[i];
                var channel = {
                    input_list: [],
                    target: item.target
                };
                for (var j = 0; j < item.input_list.length; j++) {
                    var inputitem = item.input_list[j];

                    channel.input_list[j] = {
                        time: inputitem.time,
                        transform: mat4.clone(inputitem.transform)
                    };
                }
                ret.channel_list[i] = channel;

            }

            return ret;
        };

    }

    function Collada_Mesh_Bone(name, mat) {
        this.name = name;
        this.offsetMatrix = mat;
        this.nodeObj = undefined;
    }

    function matConvertHtoL(m) {
        // mirror offset matrices of all bones (assimp)
        m[2] = -m[2];
        m[6] = -m[6];
        m[14] = -m[14];
        m[8] = -m[8];
        m[9] = -m[9];
        m[11] = -m[11];
        //m[10] = -m[10];
    }

    function getUpMatrix(up) {
        var m = undefined;
        if (up === "X_UP") {
            m = [
                0, -1, 0, 0,
                1, 0, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            ];
        }
        if (up === "Y_UP") {
            m = [
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            ];
        }
        if (up === "Z_UP") {
            m = [
                1, 0, 0, 0,
                0, 0, 1, 0,
                0, -1, 0, 0,
                0, 0, 0, 1
            ];

        }
        return m;
    }
    function findElementItem(doc, name) {
        if (doc === undefined)
            return undefined;
        for (var i = 0; i < doc.children.length; i++) {
            var e = doc.children[i];
            if (e.tagName === name)
                return e;
        }
        return undefined;

    }
    function readFloatArray(str) {
        var arr = str.split(" ");
        var ret = [];
        for (var i = 0; i < arr.length; i++) {
            var f = parseFloat(arr[i]);
            if (isNaN(f) === false) {
                ret.push(f);
            }
        }
        return ret;
    }
    function readIntArray(str) {
        var ret = [];
        var arr = str.split(" ");
        for (var i = 0; i < arr.length; i++) {
            var f = parseInt(arr[i]);
            if (isNaN(f) === false) {
                ret.push(f);
            }
        }
        return ret;
    }
    function readNameArray(str) {
        var ret = [];
        var arr = str.split(" ");
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].length > 0) {
                ret.push(arr[i]);
            }
        }
        return ret;
    }
    function findSource(source_list, id) {
        for (var i = 0; i < source_list.length; i++) {
            if ("#" + source_list[i].id === id) {
                return source_list[i];
            }
        }
        return undefined;
    }
    function proc_Sources(source_doc) {
        var source_list = [];
        var doc_source = source_doc.getElementsByTagName("source");
        for (var k = 0; k < doc_source.length; k++) {
            var source_item = {};
            var doc_source_item = doc_source[k];
            source_item.id = doc_source_item.attributes.getNamedItem("id").value;
            source_item.data = [];
            var farr = doc_source_item.getElementsByTagName("float_array");
            if (farr.length > 0) {
                source_item.data = readFloatArray(farr[0].innerHTML);
            }
            var narr = doc_source_item.getElementsByTagName("Name_array");
            if (narr.length > 0) {
                source_item.data = readNameArray(narr[0].innerHTML);
            }
            source_list.push(source_item);
        }
        return source_list;
    }
    function arrayToMatrix(array, mirror) {
        var ret = [];
        while (array.length > 0) {
            var m = mat4.clone(array.splice(0, 16));

            if (mirror) {
                matConvertHtoL(m);
            }
            ret.push(m);
        }
        return ret;
    }

    function arrayToVec3(arr, z_up) {
        var vecarr = [];
        for (var i = 0; i < arr.length; i += 3) {
            if (z_up) {
                vecarr.push(vec3.clone([arr[i], arr[i + 1], -arr[i + 2]]));
            } else {
                vecarr.push(vec3.clone([arr[i], arr[i + 1], arr[i + 2]]));

            }
        }
        return vecarr;
    }
    function arrayToVec2(arr) {
        var vecarr = [];
        for (var i = 0; i < arr.length; i += 2) {
            vecarr.push(vec3.clone([arr[i], arr[i + 1]]));
        }
        return vecarr;
    }
    function findID(arr, id) {
        for (var i = 0; i < arr.length; i++) {
            if ("#" + arr[i].id === id)
                return arr[i];
        }
        return undefined;
    }
    function findUrl(id, geolist, bonelist) {
        var boneelement = undefined;
        for (var i = 0; i < bonelist.length; i++) {
            if (id === "#" + bonelist[i].id) {
                id = bonelist[i].source;
                boneelement = bonelist[i];
                break;
            }
        }
        for (var i = 0; i < geolist.length; i++) {
            if (id === "#" + geolist[i].id) {
                var ret = {geo: geolist[i]};
                if (boneelement) {
                    ret.boneelement = boneelement;
                }
                return ret;
            }
        }
        return undefined;
    }
    function load_lib_images(doc) {
        var doc = findElementItem(doc, "library_images");
        if (doc) {
            var images_docarr = doc.getElementsByTagName("image");
            var image_arr = [];
            for (var i = 0; i < images_docarr.length; i++) {
                var img = {};
                var image_doc = images_docarr[i];

                var id = image_doc.attributes.getNamedItem("id").value;
                var name = image_doc.attributes.getNamedItem("name").value;
                var initform_doc = findElementItem(image_doc, "init_from");
                var src = initform_doc.innerHTML;
                img.id = id;
                img.name = name;
                img.src = src;
                img.texobj = undefined;
                image_arr.push(img);
            }
            return image_arr;
        }

    }
    function load_lib_effect(doc) {
        var doc = findElementItem(doc, "library_effects");
        if (doc) {
            var eff_docarr = doc.getElementsByTagName("effect");
            var eff_arr = [];
            for (var i = 0; i < eff_docarr.length; i++) {
                var doc_eff = eff_docarr[i];
                var id = doc_eff.attributes.getNamedItem("id").value;

                var eff = {};
                eff.id = id;
                var doc_profile_COMMON = findElementItem(doc_eff, "profile_COMMON");

                for (var j = 0; j < doc_profile_COMMON.children.length; j++) {
                    var doc_profile_COMMON_item = doc_profile_COMMON.children[j];
                    if (doc_profile_COMMON_item.tagName === "newparam") {
                        var surface_doc = findElementItem(doc_profile_COMMON_item, "surface");
                        if (surface_doc !== undefined) {
                            var surface_src_doc = findElementItem(surface_doc, "init_from");
                            eff.img_src = surface_src_doc.innerHTML;
                        }

                    }
                }
                eff_arr.push(eff);
            }
            return eff_arr;
        }
    }
    function load_lib_material(doc) {
        var doc = findElementItem(doc, "library_materials");
        if (doc) {
            var mat_docarr = doc.getElementsByTagName("material");
            var mat_arr = [];
            for (var i = 0; i < mat_docarr.length; i++) {
                var mat_doc = mat_docarr[i];

                var id = mat_doc.attributes.getNamedItem("id").value;
                var name = mat_doc.attributes.getNamedItem("name").value;
                var instance_effect_doc = findElementItem(mat_doc, "instance_effect");
                var url = instance_effect_doc.attributes.getNamedItem("url").value;

                var mat = {};
                mat.id = id;
                mat.name = name;
                mat.url = url;
                mat_arr.push(mat);
            }
            return mat_arr;
        }
    }

    function load_lib_geo(doc) {
        var geo_docarr = doc.getElementsByTagName("geometry");
        var geo_list = [];
        for (var i = 0; i < geo_docarr.length; i++) {
            var geo = {};
            var docgeo = geo_docarr[i];

            geo.name = docgeo.attributes.getNamedItem("name").value;
            geo.id = docgeo.attributes.getNamedItem("id").value;
            geo_list.push(geo);
            var mesh_arr = docgeo.getElementsByTagName("mesh");//only 1 mesh
            var mesh = mesh_arr[0];

            var source_list = [];
            var doc_source = mesh.getElementsByTagName("source");
            for (var k = 0; k < doc_source.length; k++) {
                var source_item = {};
                var doc_source_item = doc_source[k];
                source_item.id = doc_source_item.attributes.getNamedItem("id").value;
                source_item.data = [];
                var farr = doc_source_item.getElementsByTagName("float_array");
                source_item.data = readFloatArray(farr[0].innerHTML);
                source_list.push(source_item);

            }
            /////////////////vertices//////////////
            var doc_vertices = mesh.getElementsByTagName("vertices");
            doc_vertices = doc_vertices[0];
            var doc_vertices_input = doc_vertices.getElementsByTagName("input");
            doc_vertices_input = doc_vertices_input[0];
            //semantic = POSITION
            var source = doc_vertices_input.attributes.getNamedItem("source").value;
            var source_data = findSource(source_list, source);
            source_data.id = doc_vertices.attributes.getNamedItem("id").value;//reid*/


            /////////////////polylist//////////////
            var sourceVertex;
            var sourceNormal;
            var sourceTexcoord;
            var isColorInput = false;

            var doc_prims;
            var doc_mesh_triangles = findElementItem(mesh, "triangles");
            var doc_mesh_polylist = findElementItem(mesh, "polylist");
            if (doc_mesh_triangles !== undefined) {
                doc_prims = doc_mesh_triangles;
            } else {
                doc_prims = doc_mesh_polylist;
            }


            var doc_input = doc_prims.getElementsByTagName("input");
            for (var k = 0; k < doc_input.length; k++) {
                var doc_input_item = doc_input[k];

                var semantic = doc_input_item.attributes.getNamedItem("semantic").value;
                var source = doc_input_item.attributes.getNamedItem("source").value;
                var source_data = findSource(source_list, source);

                if (semantic === "VERTEX") {
                    sourceVertex = arrayToVec3(source_data.data, true);
                }
                if (semantic === "NORMAL") {
                    sourceNormal = arrayToVec3(source_data.data, true);
                }
                if (semantic === "TEXCOORD") {
                    sourceTexcoord = arrayToVec2(source_data.data, true);
                }
                if (semantic === "COLOR") {
                    isColorInput = true;
                }
            }

            var parray_doc = findElementItem(doc_prims, "p");
            var primitives = readIntArray(parray_doc.innerHTML);

            if (doc_mesh_polylist !== undefined) {


                var counter = 0;
                geo.dataPos = new Float32Array(primitives.length * 3);
                geo.dataNorm = new Float32Array(primitives.length * 3);
                geo.dataTexcoord = [];
                geo.index = [];
                geo.dataPosIndex = [];
                var indexcount = 0;
                while (primitives.length > 0) {
                    for (var pi = 0; pi < 3; pi++) {
                        var index = primitives.shift();

                        geo.dataPosIndex.push(index);



                        var v3 = sourceVertex[index];
                        geo.dataPos[counter + 0] = -v3[0];
                        geo.dataPos[counter + 1] = v3[1];
                        geo.dataPos[counter + 2] = v3[2];

                        var index = primitives.shift();
                        var v3 = sourceNormal[index];

                        geo.dataNorm[counter + 0] = -v3[0];
                        geo.dataNorm[counter + 1] = v3[1];
                        geo.dataNorm[counter + 2] = v3[2];

                        counter += 3;

                        if (sourceTexcoord) {
                            var index = primitives.shift();
                            var v2 = sourceTexcoord[index];
                            geo.dataTexcoord.push(v2[0], -v2[1]);
                        }
                        if (isColorInput) {
                            //drop color index
                            primitives.shift();
                        }
                    }
                    //geo.index.push(indexcount, indexcount + 2, indexcount + 1);
                    geo.index.push(indexcount, indexcount + 1, indexcount + 2);
                    indexcount += 3;
                }
            }
            if (doc_mesh_triangles !== undefined) {


                var counter = 0;
                geo.dataPos = new Float32Array(primitives.length * 3);
                geo.dataNorm = new Float32Array(primitives.length * 3);
                geo.dataTexcoord = [];
                geo.index = [];
                geo.dataPosIndex = new Uint32Array(primitives.length);
                geo.dataPosMax = sourceVertex.length;
                var indexcount = 0;
                var dataposCount = 0;
                while (primitives.length > 0) {
                    for (var pi = 0; pi < 3; pi++) {
                        var index = primitives.shift();

                        geo.dataPosIndex[dataposCount++] = index;

                        var v3 = sourceVertex[index];
                        geo.dataPos[counter + 0] = -v3[0];
                        geo.dataPos[counter + 1] = v3[1];
                        geo.dataPos[counter + 2] = v3[2];

                        var v3 = sourceNormal[index];

                        geo.dataNorm[counter + 0] = -v3[0];
                        geo.dataNorm[counter + 1] = v3[1];
                        geo.dataNorm[counter + 2] = v3[2];

                        counter += 3;

                        if (sourceTexcoord) {

                            var v2 = sourceTexcoord[index];
                            geo.dataTexcoord.push(v2[0], -v2[1]);
                        }
                        if (isColorInput) {
                        }
                    }
                    //geo.index.push(indexcount, indexcount + 2, indexcount + 1);
                    geo.index.push(indexcount, indexcount + 1, indexcount + 2);
                    indexcount += 3;
                }
            }
        }


        return geo_list;
    }
    function load_lib_controller(doc) {
        var doc_libc = doc.getElementsByTagName("library_controllers");
        doc_libc = doc_libc[0];
        if (doc_libc === undefined) {
            return [];
        }
        var libc_docarr = doc_libc.getElementsByTagName("controller");
        var controll_list = [];

        for (var i = 0; i < libc_docarr.length; i++) {

            var doc_control = libc_docarr[i];
            var id = doc_control.attributes.getNamedItem("id").value;

            var doc_control_skin = doc_control.getElementsByTagName("skin");
            doc_control_skin = doc_control_skin[0];
            var skin_source = doc_control_skin.attributes.getNamedItem("source").value;

            //bind shape matrix
            var doc_bindmatrix = findElementItem(doc_control_skin, "bind_shape_matrix");
            var bindShapeMatrix = arrayToMatrix(readFloatArray(doc_bindmatrix.innerHTML), false)[0];

            //sources
            var source_list = proc_Sources(doc_control_skin);

            //vertex_weights
            var doc_vertex_weights = findElementItem(doc_control_skin, "vertex_weights");
            var doc_input = doc_vertex_weights.getElementsByTagName("input");
            var doc_vcount = findElementItem(doc_vertex_weights, "vcount");
            var doc_v = findElementItem(doc_vertex_weights, "v");

            var vcount_arr = readIntArray(doc_vcount.innerHTML);
            var v_arr = readIntArray(doc_v.innerHTML);

            var joint_invbindmatrix;
            var joint_joints;
            var joint_weights;

            for (var k = 0; k < doc_input.length; k++) {
                var doc_input_item = doc_input[k];
                var semantic = doc_input_item.attributes.getNamedItem("semantic").value;
                var source = doc_input_item.attributes.getNamedItem("source").value;
                if (semantic === "JOINT") {
                    joint_joints = findSource(source_list, source).data;
                }

                if (semantic === "WEIGHT") {
                    joint_weights = findSource(source_list, source).data;
                }
            }

            //joints
            var doc_joints = findElementItem(doc_control_skin, "joints");
            var doc_joints_inputs = doc_joints.getElementsByTagName("input");
            for (var k = 0; k < doc_joints_inputs.length; k++) {
                var doc_joints_inputs_item = doc_joints_inputs[k];
                var sematic = doc_joints_inputs_item.attributes.getNamedItem("semantic").value;
                var source = doc_joints_inputs_item.attributes.getNamedItem("source").value;
                if (sematic === "INV_BIND_MATRIX") {
                    var finded_source = findSource(source_list, source);
                    var marray = arrayToMatrix(finded_source.data, false);

                    for (var mi = 0; mi < marray.length; mi++) {
                        var m = marray[mi];
                        mat4.mul(m, bindShapeMatrix, m);
                        matConvertHtoL(m);
                        m[1] *= -1;
                        m[2] *= -1;
                        m[3] *= -1;
                        m[4] *= -1;
                        m[8] *= -1;
                        m[12] *= -1;
                    }
                    joint_invbindmatrix = marray;
                    break;
                }
            }
            var bonelist = [];
            for (var k = 0; k < joint_joints.length; k++) {
                var joint_item = joint_joints[k];
                var bone = new Collada_Mesh_Bone(joint_item, joint_invbindmatrix[k]);
                bonelist.push(bone);
            }
            var controll_info = [];
            var controll_data = [];
            var v_index = 0;
            for (var vii = 0; vii < vcount_arr.length; vii++) {
                var value = vcount_arr[vii];
                var jarr = new Uint32Array(value);
                var warr = new Float32Array(value);

                for (var vk = 0; vk < value; vk++) {
                    var joint = v_arr[v_index++];
                    var weight_index = v_arr[v_index++];

                    jarr[vk] = joint;
                    warr[vk] = joint_weights[weight_index];
                }
                controll_info[vii] = {jarr: jarr, warr: warr};
            }
            controll_list.push({id: id, source: skin_source, bonelist: bonelist, controll_info: controll_info});
        }
        return controll_list;
    }
    function load_node(doc_node, parentNode, node_list, meshlist, node_sid_list) {
        for (var i = 0; i < doc_node.children.length; i++) {
            var doc_node_item = doc_node.children[i];
            if (doc_node_item.tagName === "node") {

                var id = doc_node_item.attributes.getNamedItem("id").value;
                var name = doc_node_item.attributes.getNamedItem("name").value;

                var mat = mat4.create();
                for (var ci = 0; ci < doc_node_item.childNodes.length; ci++) {
                    var citem = doc_node_item.childNodes[ci];

                    switch (citem.tagName) {
                        case "lookat":

                            console.error("Load->Collada->lookat not impelmented!");
                            break;
                        case "matrix":
                            var arr = readFloatArray(citem.innerHTML);
                            mat4.transpose(arr, arr);
                            mat4.multiply(mat, mat, arr);
                            break;
                        case "translate":
                            var arr = readFloatArray(citem.innerHTML);
                            mat4.translate(mat, mat, arr);
                            break;
                        case "rotate":
                            var arr = readFloatArray(citem.innerHTML);
                            var angle = arr[3] * Math.PI / 180;
                            mat4.rotate(mat, mat, angle, arr);
                            break;
                        case "scale":
                            var arr = readFloatArray(citem.innerHTML);
                            mat4.scale(mat, mat, arr);
                            break;
                    }
                }
                mat4.transpose(mat, mat);
                matConvertHtoL(mat);
                mat[1] *= -1;
                mat[2] *= -1;
                mat[3] *= -1;
                mat[4] *= -1;
                mat[8] *= -1;
                mat[12] *= -1;
                var node = new core.animator.Animator_Node(id, name, parentNode, mat);

                var sid = doc_node_item.attributes.getNamedItem("sid");
                if (sid !== null) {
                    node_sid_list[sid.value] = id;
                }

                var doc_instance_controller = findElementItem(doc_node_item, "instance_controller");
                var doc_instance_geometry = findElementItem(doc_node_item, "instance_geometry");
                if (doc_instance_controller || doc_instance_geometry) {
                    var doc_geo_contr = (doc_instance_controller) ? doc_instance_controller : doc_instance_geometry;
                    var url = doc_geo_contr.attributes.getNamedItem("url").value;
                    var mesh = {node: node, url: url, material_target: undefined};
                    var doc_bind_material = findElementItem(doc_geo_contr, "bind_material");
                    var doc_technique_common = findElementItem(doc_bind_material, "technique_common");
                    var doc_instance_material = findElementItem(doc_technique_common, "instance_material");

                    if (doc_instance_material) {
                        mesh.material_target = doc_instance_material.attributes.getNamedItem("target").value;
                    }
                    meshlist.push(mesh);
                }

                parentNode.childList.push(node);
                node_list[node.id] = node;
                node_list[node.name] = node;

                load_node(doc_node_item, node, node_list, meshlist, node_sid_list);
            }
        }
    }

    function load_lib_scene(doc, node, node_list, meshlist, node_sid_list) {
        var doc_lib_vs = doc.getElementsByTagName("library_visual_scenes");
        doc_lib_vs = doc_lib_vs[0];
        var doc_vs = doc_lib_vs.getElementsByTagName("visual_scene");
        doc_vs = doc_vs[0];
        load_node(doc_vs, node, node_list, meshlist, node_sid_list);
    }

    function load_lib_anm(doc) {
        var channelList = [];
        var doc = findElementItem(doc, "library_animations");
        if (doc) {
            var doc_animation = doc.getElementsByTagName("animation");
            for (var i = 0; i < doc_animation.length; i++) {
                var doc_doc_animation_item = doc_animation[i];
                var source_list = proc_Sources(doc_doc_animation_item);

                var doc_sampler = findElementItem(doc_doc_animation_item, "sampler");
                var doc_input = doc_sampler.getElementsByTagName("input");

                var doc_channel = findElementItem(doc_doc_animation_item, "channel");
                var channel_target = doc_channel.attributes.getNamedItem("target").value;

                var slash_list = channel_target.split("/");
                var channelData = {
                    id: doc_doc_animation_item.attributes.getNamedItem("id").value,
                    target: slash_list[0],
                    subtarget: slash_list[1]
                };
                for (var k = 0; k < doc_input.length; k++) {
                    var input_item = doc_input[k];
                    var semantic = input_item.attributes.getNamedItem("semantic").value;
                    var source = input_item.attributes.getNamedItem("source").value;

                    var source_data = findSource(source_list, source);
                    switch (semantic) {
                        case "INPUT":
                            channelData.input = source_data.data;
                            break;
                        case "OUTPUT":
                            channelData.output = source_data.data;
                            break;
                        case "INTERPOLATION":
                            channelData.interpolation = source_data.data;
                            break;
                        case "IN_TANGENT":
                            channelData.in_tangent = source_data.data;
                            break;
                        case "OUT_TANGENT":
                            channelData.out_tangent = source_data.data;
                            break;
                    }
                }

                channelList.push(channelData);

            }
        } else {
            return undefined;
        }
        return channelList;
    }
    function load_lib_anim_clips(doc) {
        var clip_list = [];
        var doc = findElementItem(doc, "library_animation_clips");
        if (doc) {
            var doc_clips = doc.getElementsByTagName("animation_clip");
            for (var i = 0; i < doc_clips.length; i++) {
                var doc_clips_item = doc_clips[i];
                var clip_item = {
                    name: doc_clips_item.attributes.getNamedItem("name").value,
                    anim_inst_list: []
                };
                var doc_anim_inst = doc_clips_item.getElementsByTagName("instance_animation");
                for (var j = 0; j < doc_anim_inst.length; j++) {
                    var doc_anim_inst_item = doc_anim_inst[j];
                    clip_item.anim_inst_list.push(doc_anim_inst_item.attributes.getNamedItem("url").value);
                }

                clip_list.push(clip_item);
            }
        } else {
            return undefined;
        }
        return clip_list;
    }

    function process_anim(clipname) {
        var ret = new AnimObj();
        for (var i in node_list) {
            var node = node_list[i];
            var node_anim = undefined;
            for (var k = 0; k < anm_list.length; k++) {
                var anim = anm_list[k];

                if (anim.target === node.id && anim.clipname === clipname) {
                    node_anim = anim;
                    anm_list.splice(k, 1);
                    break;
                }
            }
            //csak transform elfogadott, blender mindig ebbe számolja a bone mozgásokat
            if (node_anim === undefined || node_anim.subtarget !== "transform") {
                continue;
            }
            var input_list = [];
            var matarr = arrayToMatrix(node_anim.output, true);
            var absoluttime = node_anim.input[0];
            for (var ii = 0; ii < node_anim.input.length; ii++) {
                var input = node_anim.input[ii] - absoluttime;
                var tmatrix = matarr[ii];
                tmatrix[1] *= -1;
                tmatrix[2] *= -1;
                tmatrix[3] *= -1;
                tmatrix[4] *= -1;
                tmatrix[8] *= -1;
                tmatrix[12] *= -1;
                input_list.push({
                    time: input,
                    transform: tmatrix
                });
                ret.duration = Math.max(ret.duration, input);
            }
            var channel = {
                target: node_list[node_anim.target],
                input_list: input_list
            };
            ret.channel_list[node_anim.target] = channel;
        }
        return ret;

    }

    var dpar = new DOMParser();
    var doc = dpar.parseFromString(data, "application/xml");
    doc = findElementItem(doc, "COLLADA");

    var img_list = load_lib_images(doc);
    var eff_list = load_lib_effect(doc);
    var mat_list = load_lib_material(doc);
    var geo_list = load_lib_geo(doc);
    var anm_list = load_lib_anm(doc);
    var anm_clips = load_lib_anim_clips(doc);
    //console.info(anm_list);
    var controll_list = load_lib_controller(doc);


    var upMatrix = mat4.create();
    var doc_asset = findElementItem(doc, "asset");
    if (doc_asset) {
        var doc_upmatrix = findElementItem(doc_asset, "up_axis");
        if (doc_upmatrix) {
            upMatrix = getUpMatrix(doc_upmatrix.innerHTML);
        }
    }
    var rootmatrix = mat4.create();
    var rootNode = new core.animator.Animator_Node("", "", undefined, rootmatrix);
    var node_meshlist = [];
    var node_list = [rootNode];

    //teljesen felesleges az SID, amikor ott van az ID
    //sajnos fel kell írni, mivel a bone-ok erre mutatnak
    //később a bone targetek az SID listában lévő ID-kra lesznek lecserélve, innentől már nem létezik az SID
    var node_sid_list = [];
    load_lib_scene(doc, rootNode, node_list, node_meshlist, node_sid_list);
    var meshlist = [];

    for (var i = 0; i < node_meshlist.length; i++) {
        var nodemesh = node_meshlist[i];
        var mesh = new core.animator.Animator_Mesh(nodemesh.node.name, nodemesh.node);
        var returl = findUrl(nodemesh.url, geo_list, controll_list);
        mesh.geo = returl.geo;
        mesh.bonelist = [];
        if (returl.boneelement) {
            mesh.bonelist = returl.boneelement.bonelist;
            mesh.controll_info = returl.boneelement.controll_info;
            for (var j = 0; j < mesh.bonelist.length; j++) {
                var bone = mesh.bonelist[j];
                //itt cseréljük ki az SID-t ID-ra
                var node_id = node_sid_list[bone.name];
                bone.nodeObj = node_list[node_id];
            }
        }

        if (nodemesh.material_target) {
            var mat = findID(mat_list, nodemesh.material_target);
            var eff = findID(eff_list, mat.url);
            mesh.effect = eff;
            if (mesh.effect.img_src) {
                mesh.effect.img = findID(img_list, "#" + mesh.effect.img_src);
            }
        }

        meshlist.push(mesh);
    }


    var anm_list_proc;
    //process anim
    if (anm_list !== undefined && anm_list.length > 0) {
        anm_list_proc = [];

        if (anm_clips !== undefined && anm_clips.length > 0) {
            //a clip neveket hozzárendeljuk a anim elemekhez
            for (var i = 0; i < anm_clips.length; i++) {
                var clip = anm_clips[i];
                var name = clip.name;
                for (var j = 0; j < clip.anim_inst_list.length; j++) {
                    var clipid = clip.anim_inst_list[j];
                    for (var k = 0; k < anm_list.length; k++) {
                        if (clipid === "#" + anm_list[k].id) {
                            anm_list[k].clipname = name;
                            break;
                        }
                    }
                }
            }

            for (var i = 0; i < anm_clips.length; i++) {
                var name = anm_clips[i].name;
                anm_list_proc[name] = process_anim(name);


            }
        } else {
            anm_list_proc[0] = process_anim(undefined);
        }

    }

    var scene = {
        meshlist: meshlist,
        rootNode: rootNode,
        bonelist: controll_list,
        node_list: node_list,
        img_list: img_list,
        mat_list: mat_list,
        eff_list: eff_list,
        anm_list: anm_list_proc,
        upmatrix: upMatrix
    };
    //console.info(scene);
    return scene;

};



/*global vec3*/
function PokolVoxelEngine_fizika(core) {

    var FIZIKAKAR_Y_ERR = 0.0001;
    function getLinePoint(kezdarr, vegarr, w, labw) {
        var ret = [];
        function point(x, y, z) {
            x |= 0;
            y |= 0;
            z |= 0;
            for (var i = 0; i < ret.length; i++) {
                if (ret[i][0] === x && ret[i][1] === y && ret[i][2] === z)
                    return;
            }
            var v = [x, y, z];
            v[3] = vec3.distance(v, kezdarr);
            ret.push(v);
        }
        function fillPoint(x, y, z, w) {
            point(x, y, z);

            for (var i = -w; i <= w; i++) {
                for (var j = -w - labw; j <= w; j++) {
                    for (var k = -w; k <= w; k++) {
                        point(x + i, y + j, z + k);

                    }
                }
            }
        }


        var kezdx = kezdarr[0];
        var kezdy = kezdarr[1];
        var kezdz = kezdarr[2];
        var vegx = vegarr[0];
        var vegy = vegarr[1];
        var vegz = vegarr[2];
        var t1 = Math.abs(kezdx - vegx);
        var t2 = Math.abs(kezdy - vegy);
        var t3 = Math.abs(kezdz - vegz);
        if (t2 > t1)
            t1 = t2;
        if (t3 > t1)
            t1 = t3;
        if (t1 === 0)
            return [];
        var xn = (vegx - kezdx) / t1;
        var yn = (vegy - kezdy) / t1;
        var zn = (vegz - kezdz) / t1;


        var px = kezdx;
        var py = kezdy;
        var pz = kezdz;


        while (1) {
            fillPoint(px, py, pz, w);
            px += xn;
            py += yn;
            pz += zn;
            if (Math.abs(px - kezdx) > Math.abs(kezdx - vegx) ||
                    Math.abs(py - kezdy) > Math.abs(kezdy - vegy) ||
                    Math.abs(pz - kezdz) > Math.abs(kezdz - vegz))
                break;
        }
        return ret;
    }

    function fizike_osszegyujt_blocks(pos, pos2, blocksize, width) {

        var x = pos[0] / blocksize;
        var y = pos[1] / blocksize;
        var z = pos[2] / blocksize;
        var x2 = pos2[0] / blocksize;
        var y2 = pos2[1] / blocksize;
        var z2 = pos2[2] / blocksize;

        return getLinePoint([x, y, z], [x2, y2, z2], width, 4);
        /*
         var ret = [];
         for (var i = (x - width) | 0; i < (x2 + width) | 0; i++) {
         for (var j = (y - width) | 0; j < (y2 + width) | 0; j++) {
         for (var k = (z - width) | 0; k < (z2 + width) | 0; k++) {
         var v = [i, j, k];
         v[3] = vec3.distance(v, [x, y, z]); // telán: * blocksize? (így lesz valós távolság)
         ret.push(v);
         }
         }
         }
        return ret;*/
    }

    function VertexUtkoz(po, ir, f1, f2, f3, tavobj) {
        var aktretp = vec3.create();
        if (core.math.glRayPick(po, ir, f1, f2, f3, aktretp) === 1) {
            var akttav = vec3.distance(po, aktretp);
            if (vec3.length(ir) < akttav)
                return false;
            if (tavobj[0] > akttav) {
                vec3.copy(tavobj[1], aktretp);
                tavobj[0] = akttav;
                tavobj[3] = -1;
                return true;
            }


        }
        return false;
    }


    function FalUtkoz(po, ir, fp, tavobj) {

        var EmberM = 0.40;
        var s = core.voxelVilag.blocksize;
        var LepcsoM = 0.3;
        var LabMag = 0.15;
        var x1 = fp[0] - EmberM;
        var x2 = fp[0] + EmberM + s;
        var y1 = fp[1] - EmberM;
        var y2 = fp[1] + EmberM + s + LepcsoM + LabMag;
        var z1 = fp[2] - EmberM;
        var z2 = fp[2] + EmberM + s;

        var f0 = [x1, y1, z1];
        var f1 = [x2, y1, z1];
        var f2 = [x1, y1, z2];
        var f3 = [x2, y1, z2];

        var f4 = [x1, y2, z1];
        var f5 = [x2, y2, z1];
        var f6 = [x1, y2, z2];
        var f7 = [x2, y2, z2];
        if (VertexUtkoz(po, ir, f0, f1, f2, tavobj) || VertexUtkoz(po, ir, f3, f1, f2, tavobj)) {
            tavobj[1][1] -= 0.0001;
            tavobj[2] = 1;
        }
        if (VertexUtkoz(po, ir, f4, f5, f6, tavobj) || VertexUtkoz(po, ir, f7, f5, f6, tavobj)) {
            tavobj[1][1] += 0.0001;
            tavobj[2] = 1;
        }

        //x szembe-hátúl
        if (VertexUtkoz(po, ir, f0, f1, f4, tavobj) || VertexUtkoz(po, ir, f5, f1, f4, tavobj)) {
            if (y2 - tavobj[1][1] < LepcsoM) {
                tavobj[3] = y2 + 0.001;
            }
            tavobj[1][2] -= 0.0001;
            tavobj[2] = 2;
        }
        if (VertexUtkoz(po, ir, f2, f3, f6, tavobj) || VertexUtkoz(po, ir, f7, f3, f6, tavobj)) {
            if (y2 - tavobj[1][1] < LepcsoM) {
                tavobj[3] = y2 + 0.001;
            }
            tavobj[1][2] += 0.0001;
            tavobj[2] = 2;
        }

        //z
        if (VertexUtkoz(po, ir, f0, f2, f4, tavobj) || VertexUtkoz(po, ir, f6, f2, f4, tavobj)) {
            if (y2 - tavobj[1][1] < LepcsoM) {
                tavobj[3] = y2 + 0.001;
            }
            tavobj[1][0] -= 0.0001;
            tavobj[2] = 0;
        }
        if (VertexUtkoz(po, ir, f1, f3, f5, tavobj) || VertexUtkoz(po, ir, f7, f3, f5, tavobj)) {
            if (y2 - tavobj[1][1] < LepcsoM) {
                tavobj[3] = y2 + 0.001;
            }
            tavobj[1][0] += 0.0001;
            tavobj[2] = 0;
        }

        return false;
    }


    function FalUtkozVizsg(oldpos, ujpos) {
        var blocksize = core.voxelVilag.blocksize;

        var ret = [ujpos[0], ujpos[1], ujpos[2]];
        var retarr = fizike_osszegyujt_blocks(oldpos, ujpos, blocksize, 3);

        retarr.sort(function (a, b) {
            return  a[3] - b[3];
        });
        var pos = [];
        pos[0] = ujpos[0] - oldpos[0];
        pos[1] = ujpos[1] - oldpos[1];
        pos[2] = ujpos[2] - oldpos[2];


        for (var i = 0; i < retarr.length; i++) {
            var e = retarr[i];
            var elem = core.voxelVilag.getElem(e[0] | 0, e[1] | 0, e[2] | 0);
            if (elem === 0)
                continue;
            e[0] *= blocksize;
            e[1] *= blocksize;
            e[2] *= blocksize;
            var x = e[0];
            var y = e[1];
            var z = e[2];
            var tavobj = [Number.POSITIVE_INFINITY, [0, 0, 0], -1, -1];
            FalUtkoz(oldpos, pos, [x, y, z], tavobj);
            if (tavobj[2] !== -1)
                return tavobj;


        }

        return undefined;
    }

    this.moveCheck = function (oldpos, ujpos, szint) {
        if (szint === undefined)
            szint = 0;
        if (szint === 4)
            return true;


        var vret = FalUtkozVizsg(oldpos, ujpos);

        if (vret && vret[2] !== -1) {
            if (vret[3] !== -1) {
                var SarokPos = [vret[1][0], vret[3], vret[1][2]];
                var SarokIrany = [0, 0, 0];

                if (vret[2] === 0)
                    SarokIrany[0] = (oldpos[0] < ujpos[0]) ? (0.01) : (-0.01);
                else
                    SarokIrany[2] = (oldpos[2] < ujpos[2]) ? (0.01) : (-0.01);
                var vret2 = FalUtkozVizsg(oldpos, ujpos);

                if (vret2 && vret2[2] !== -1) {
                    vec3.copy(ujpos, SarokPos);
                    vec3.add(ujpos, ujpos, SarokIrany);
                    return true;
                }
            }

            ujpos[vret[2]] = vret[1][vret[2]];
            this.moveCheck(oldpos, ujpos, szint + 1);
            return true;//már biztos hogy ütközünk
        }
        return false;
    };
}

function PokolVoxelEngine_texman(core, textureLoadComplete_Callback) {
    var gl = core.ogl;
    var texman_binded = [];//ez már bindelve van

    this.update = function () {
        texman_binded = [];
        var needfill = false;
        for (var i = gl.Max_Texture_Units - 1; i >= 0; i--) {
            gl.activeTexture(gl.TEXTURE0 + i);
            var ret = gl.getParameter(gl.TEXTURE_BINDING_2D);
            var retid = -1;
            if (ret !== null && ret.textureID !== undefined) {
                retid = ret.textureID;
            }
            if (retid === -1) {
                if (needfill) {
                    texman_binded[i] = {id: -1, used: false};
                }
            } else {
                needfill = true;
                texman_binded[i] = texman_binded[i] = {id: retid, used: false};
            }

        }
    };

    this.lockTexture = function (tex, name) {
        var id = tex.textureID;
        var findindex = -1;
        var used = false;
        for (var i = 0; i < texman_binded.length; i++) {
            var elem = texman_binded[i]; //nem lehet undefined elem!!
            if (texman_binded[i].id === id) {
                if (texman_binded[i].used === true) {
                    return;//ez már aktív
                } else {
                    used = true;
                    findindex = i;
                    break;
                }
            } else {
                if (elem.used === false) {
                    findindex = i;
                }
            }
        }
        //nincs 1 ilyen elem se ami unused, és nincs több hely bindelni
        if (findindex === -1 && gl.Max_Texture_Units <= texman_binded.length) {
            throw "TEXMAN> Nincs több üres bind textura";
        }
        if (findindex === -1 || used === false) {//új tömb eleme bevezetése
            if (gl.Max_Texture_Units <= texman_binded.length) {//nem tudunk többet bindelni, bindelt textúra felhasználása

            } else {
                findindex = texman_binded.length;
            }
        }
        if (used === false) {
            gl.activeTexture(gl.TEXTURE0 + findindex);
            gl.bindTexture(gl.TEXTURE_2D, tex);
        }
        core.shader.shaderUniform(name, [findindex]);
        texman_binded[findindex] = {id: id, name: name, used: true};

    };
    this.bindTexture = function (tex) {
        var id = tex.textureID;
        var findindex = -1;
        for (var i = 0; i < texman_binded.length; i++) {
            var elem = texman_binded[i]; //nem lehet undefined elem!!
            if (texman_binded[i].id === id) {
                gl.activeTexture(gl.TEXTURE0 + i);
                return;
            } else {
                if (elem.used === false) {
                    findindex = i;
                }
            }
        }
        //nincs 1 ilyen elem se ami unused, és nincs több hely bindelni
        if (findindex === -1 && gl.Max_Texture_Units <= texman_binded.length) {
            throw "TEXMAN> Nincs több üres bind textura";
        }
        if (findindex === -1) {//új tömb eleme bevezetése
            if (gl.Max_Texture_Units <= texman_binded.length) {//nem tudunk többet bindelni, bindelt textúra felhasználása

            } else {
                findindex = texman_binded.length;
            }
        }

        gl.activeTexture(gl.TEXTURE0 + findindex);
        gl.bindTexture(gl.TEXTURE_2D, tex);
        texman_binded[findindex] = {id: id, name: undefined, used: false};


    };

    this.unlockTexture = function (tex) {

        var id = tex.textureID;
        for (var i = 0; i < texman_binded.length; i++) {
            var elem = texman_binded[i]; //nem lehet undefined elem!!
            if (elem.id === id) {
                if (elem.used === false) {
                    throw "TEXMAN> unlock hiba, ez a textura nincs is használva: " + id;
                }
                elem.used = false;
                //nem breakelünk, lock-nak meg kellene fogni a több bindelést, de inkább mennyen végig
                return;
            }
        }

        throw "TEXMAN> unlock hiba, textura id nem található";

    };

    //minden fps ticknél az összes texturának unusednek kell lenni
    this.checkTexture = function () {
        var s = "";
        var error = false;
        for (var i = 0; i < texman_binded.length; i++) {
            var elem = texman_binded[i]; //nem lehet undefined elem!!
            if (elem.used === true) {
                s += " (" + elem.id + ")";
                error = true;
            }
        }
        if (error) {
            throw "TEXMAN> check hiba, a következő textúrák nem lettek unlockolva: " + s;
        }
    };


    this.getfreetex = function (magfilter, minfilter) {
        var ret = gl.createTexture();
        ret.magfilter = (magfilter) ? magfilter : gl.NEAREST;
        ret.minfilter = (minfilter) ? minfilter : gl.NEAREST_MIPMAP_NEAREST;
        gl.bindTexture(gl.TEXTURE_2D, ret);
        ret.width = 1;
        ret.height = 1;
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, ret.magfilter);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, ret.minfilter);
        if (ret.minfilter === gl.LINEAR_MIPMAP_LINEAR || ret.minfilter === gl.NEAREST_MIPMAP_NEAREST ||
                ret.minfilter === gl.NEAREST_MIPMAP_LINEAR || ret.minfilter === gl.LINEAR_MIPMAP_NEAREST) {
            gl.generateMipmap(gl.TEXTURE_2D);
        }
        gl.bindTexture(gl.TEXTURE_2D, null);
        return ret;
    };
    this.getTexData = function (texture) {
        var framebuffer = gl.createFramebuffer();
        gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
        var data = new Uint8Array(texture.width * texture.height * 4);
        gl.readPixels(0, 0, texture.width, texture.height, gl.RGBA, gl.UNSIGNED_BYTE, data);

        gl.deleteFramebuffer(framebuffer);
        return data;
    };
    this.getdatatex = function (w, h, data, magfilter, minfilter) {
        var ret = gl.createTexture();
        ret.magfilter = (magfilter) ? magfilter : gl.NEAREST;
        ret.minfilter = (minfilter) ? minfilter : gl.NEAREST_MIPMAP_NEAREST;
        ret.width = w;
        ret.height = h;
        gl.bindTexture(gl.TEXTURE_2D, ret);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, w, h, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array(data));
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, ret.magfilter);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, ret.minfilter);
        if (ret.minfilter === gl.LINEAR_MIPMAP_LINEAR || ret.minfilter === gl.NEAREST_MIPMAP_NEAREST ||
                ret.minfilter === gl.NEAREST_MIPMAP_LINEAR || ret.minfilter === gl.LINEAR_MIPMAP_NEAREST) {
            gl.generateMipmap(gl.TEXTURE_2D);
        }
        gl.bindTexture(gl.TEXTURE_2D, null);
        return ret;
    };
    this.drawTex = function (tex, drawobj) {
        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, tex.magfilter);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, tex.minfilter);
        tex.width = drawobj.width;
        tex.height = drawobj.height;
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, drawobj);
        if (tex.minfilter === gl.LINEAR_MIPMAP_LINEAR ||
                tex.minfilter === gl.NEAREST_MIPMAP_NEAREST ||
                tex.minfilter === gl.NEAREST_MIPMAP_LINEAR ||
                tex.minfilter === gl.LINEAR_MIPMAP_NEAREST)
        {
            gl.generateMipmap(gl.TEXTURE_2D);
        }

        gl.bindTexture(gl.TEXTURE_2D, null);
        if (textureLoadComplete_Callback !== undefined)
            textureLoadComplete_Callback();
    };
    this.newtexture = function (url, magfilter, minfilter) {
        var ret = gl.createTexture();
        ret.magfilter = (magfilter) ? magfilter : gl.LINEAR;
        ret.minfilter = (minfilter) ? minfilter : gl.NEAREST_MIPMAP_NEAREST;
        ret.width = 1;
        ret.height = 1;
        gl.bindTexture(gl.TEXTURE_2D, ret);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 0, 0]));
        gl.bindTexture(gl.TEXTURE_2D, null);
        var img = new Image();
        img.onload = function () {
            gl.bindTexture(gl.TEXTURE_2D, img.tex);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, img.tex.magfilter);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, img.tex.minfilter);
            ret.width = img.width;
            ret.height = img.height;
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
            if (img.tex.minfilter === gl.LINEAR_MIPMAP_LINEAR ||
                    img.tex.minfilter === gl.NEAREST_MIPMAP_NEAREST ||
                    img.tex.minfilter === gl.NEAREST_MIPMAP_LINEAR ||
                    img.tex.minfilter === gl.LINEAR_MIPMAP_NEAREST)
            {
                gl.generateMipmap(gl.TEXTURE_2D);
            }
            //gl.bindTexture(gl.TEXTURE_2D, null);

            if (textureLoadComplete_Callback !== undefined)
                textureLoadComplete_Callback();
        };
        img.tex = ret;
        img.src = url;
        ret.img = img;
        return ret;
    };
    function resizeCanvasImage(img, maxWidth, maxHeight, step) {
        //http://stackoverflow.com/questions/18922880/html5-canvas-resize-downscale-image-high-quality
        var imgWidth = img.width, imgHeight = img.height;
        var ratio = 1, ratio1 = 1, ratio2 = 1;
        ratio1 = maxWidth / imgWidth;
        ratio2 = maxHeight / imgHeight;
        // Use the smallest ratio that the image best fit into the maxWidth x maxHeight box.
        if (ratio1 < ratio2) {
            ratio = ratio1;
        } else {
            ratio = ratio2;
        }
        var canvasCopy = document.createElement("canvas");
        var copyContext = canvasCopy.getContext("2d");
        var canvasCopy2 = document.createElement("canvas");
        var copyContext2 = canvasCopy2.getContext("2d");
        canvasCopy.width = imgWidth;
        canvasCopy.height = imgHeight;
        copyContext.drawImage(img, 0, 0);
        // init
        canvasCopy2.width = imgWidth;
        canvasCopy2.height = imgHeight;
        copyContext2.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvasCopy2.width, canvasCopy2.height);

        var rounds = step;
        var roundRatio = ratio * rounds;
        for (var i = 1; i <= rounds; i++) {
            // tmp
            canvasCopy.width = imgWidth * roundRatio / i;
            canvasCopy.height = imgHeight * roundRatio / i;
            copyContext.drawImage(canvasCopy2, 0, 0, canvasCopy2.width, canvasCopy2.height, 0, 0, canvasCopy.width, canvasCopy.height);
            // copy back
            canvasCopy2.width = imgWidth * roundRatio / i;
            canvasCopy2.height = imgHeight * roundRatio / i;
            copyContext2.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvasCopy2.width, canvasCopy2.height);

        } // end for
        return canvasCopy2;
    }

    this.TexAtlas = function (max, texsize, bor, lod_size) {
        this.max = max;
        this.canvas = document.createElement("CANVAS");
        this.canvas.width = max;
        this.canvas.height = max;
        this.ctx = this.canvas.getContext("2d");
        this.ctx.fillStyle = "#FF0000";
        this.ctx.fillRect(0, 0, this.max, this.max);

        this.canvas_lod = document.createElement("CANVAS");//fix no mipmap max lo
        this.lod_size = lod_size;
        this.lod_ratio = lod_size / max;
        this.canvas_lod.width = lod_size;
        this.canvas_lod.height = lod_size;
        this.ctx_lod = this.canvas_lod.getContext("2d");
        this.ctx_lod.fillStyle = "#FF0000";
        this.ctx_lod.fillRect(0, 0, this.lod_size, this.lod_size);

        this.bor = bor;
        this.texsize = texsize;

        this.imglist = [];
        this.bitlen = Math.floor(this.max / (this.bor + this.bor + this.texsize));
        this.nextbit = 0;

        this.text;
        this.text_lod;

        this.clearLevel;

        this.addimg = function (img) {
            this.drwimg(img, this.nextbit);
            this.nextbit++;
            return this.nextbit - 1;
        };
        this.drwimg = function (img, id) {

            var retpos = this.getPos(id);
            var posx = retpos.x;
            var posy = retpos.y;

            var downstep = Math.sqrt(img.width / this.texsize);
            var downstep_lod = Math.sqrt(downstep / this.lod_ratio);
            var dsc_lod = resizeCanvasImage(img, this.texsize * this.lod_ratio, this.texsize * this.lod_ratio, downstep_lod * 2);
            var dsc = resizeCanvasImage(img, this.texsize, this.texsize, downstep * 2);
            var bornum = Math.ceil(this.bor / this.texsize);

            var clipx = posx - this.bor;
            var clipy = posy - bor;
            var clipxm = this.bor + this.bor + this.texsize;
            var clipym = this.bor + this.bor + this.texsize;

            this.ctx.save();
            this.ctx.beginPath();
            this.ctx.rect(clipx, clipy, clipxm, clipym);
            this.ctx.clip();

            this.ctx_lod.save();
            this.ctx_lod.beginPath();
            this.ctx_lod.rect(clipx * this.lod_ratio, clipy * this.lod_ratio, clipxm * this.lod_ratio, clipym * this.lod_ratio);
            this.ctx_lod.clip();

            for (var i = -bornum; i <= bornum; i++) {
                for (var j = -bornum; j <= bornum; j++) {
                    var px = posx - (i * this.texsize);
                    var py = posy - (j * this.texsize);

                    this.ctx.drawImage(dsc, px, py, this.texsize, this.texsize);
                    this.ctx_lod.drawImage(dsc_lod, px * this.lod_ratio, py * this.lod_ratio, this.texsize * this.lod_ratio, this.texsize * this.lod_ratio);
                }
            }
            this.ctx.restore();
            this.ctx_lod.restore();
        };
        this.loadimg = function (url) {
            var img = new Image();
            img.onload = function () {

                this.obj.drwimg(this, this.texid);
                if (this.obj.text !== undefined && this.obj.text !== undefined) {
                    this.obj.updateTexture();
                }
                if (textureLoadComplete_Callback !== undefined)
                    textureLoadComplete_Callback();
            };
            img.obj = this;
            img.texid = this.nextbit;
            img.src = url;
            this.nextbit++;
            return this.nextbit - 1;

        };
        this.getPos = function (id) {
            var px = id % this.bitlen;
            var py = (id / this.bitlen) | 0;
            return {x: (px * (this.texsize + this.bor + this.bor)) + this.bor, y: (py * (this.texsize + this.bor + this.bor)) + this.bor};
        };

        this.getTexSize = function () {
            return this.bor + this.bor + this.texsize;
        };

        this.fillMimap = function () {

            var level = 0;
            var size = this.max;
            var c = document.createElement("CANVAS");
            var lctx = c.getContext("2d");
            while (1) {
                level++;
                size >>= 1;
                if (size === 0)
                    break;
                if (level < this.clearLevel)
                    continue;

                c.width = size;
                c.height = size;
                lctx.clearRect(0, 0, size, size);

                //console.info("level" + size);
                gl.texImage2D(gl.TEXTURE_2D, level, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, c);

            }
        };
        this.createTexture = function (clearLevel) {
            this.clearLevel = clearLevel;
            this.text = gl.createTexture();
            this.text_lod = gl.createTexture();
        };
        this.updateTexture = function () {

            gl.bindTexture(gl.TEXTURE_2D, this.text);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.canvas);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.generateMipmap(gl.TEXTURE_2D);
            this.fillMimap();

            gl.bindTexture(gl.TEXTURE_2D, this.text_lod);

            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.canvas_lod);
        };

        this.activeShader = function () {
            core.texman.lockTexture(this.text, "uSampleri");
            core.texman.lockTexture(this.text_lod, "uSampleri1");


            core.shader.shaderUniform("uTexSize", [this.max / (this.texsize)]);
            core.shader.shaderUniform("uFullSize", [this.max]);
            core.shader.shaderUniform("uTexBor", [this.bor]);
            core.shader.shaderUniform("uTexPosSzor", [this.texsize + this.bor + this.bor]);
        };
        this.unActiveShader = function () {
            core.texman.unlockTexture(this.text);
            core.texman.unlockTexture(this.text_lod);
        };
    };
    this.TexAtlas.getFragmentShader = function (functionname) {
        var fragment =
                "uniform highp float uFullSize; \n" +
                "uniform highp float uTexSize; \n" +
                "uniform highp float uTexBor; \n" +
                "uniform highp float uTexPosSzor; \n" +
                "uniform sampler2D uSampleri; \n" +
                "uniform sampler2D uSampleri1; \n" +
                "highp vec4 " + functionname + "(highp float x, highp float y,highp vec2 vTextureCoord){ \n" +
                "    highp vec2 TexPos = vec2((x * uTexPosSzor) + uTexBor, (y * uTexPosSzor) + uTexBor); \n" +
                "    highp vec4 szin =  texture2D(uSampleri, fract(vTextureCoord  )/uTexSize + (1.0/uFullSize*TexPos)) ; \n" +
                "    if (szin.a < 1.0){ \n" +
                "        szin =  texture2D(uSampleri1, fract(vTextureCoord  )/uTexSize + (1.0/uFullSize*TexPos)); \n" +
                "    } \n" +
                "    return szin; \n" +
                "}";
        return fragment;
    };

}
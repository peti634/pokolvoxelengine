/* global PokolVoxelEngine*/

/*
 * PokolVoxelEngine v0.7 - 2018.02.22.
 * PokolStudio - peti634
 */

PokolVoxelEngine.prototype.animatorMeshClass = function (core) {
    var gl = core.ogl;

    var animatorMeshList = [];
    var animatorMeshGeos = new core.geos.GeosContainer();
    var idCounter = 1;
    var shaderObj;

    this.animatorMeshObject = function (animatorController) {
        animatorMeshList[idCounter] = this;
        var id = idCounter++;
        var propList = animatorController.clonePropList();
        this.obj3d;

        var init = function (_this_aO) {
            _this_aO.obj3d = new core.geos.Geo_Free(core.geos.Geo_Free.Triangle, animatorController.animGeometry, [0, 0, 0], [1, 1, 1]);
            animatorMeshGeos.addList(_this_aO.obj3d);
            _this_aO.obj3d.setDrawAfterCallback(drawAfterCallback, {animatorIns: animatorController, propList: propList});
            animatorController.setObject3D(_this_aO.obj3d);
        };

        var drawAfterCallback = function (obj3d, userobj) {
            userobj.animatorIns.rebuild(obj3d, userobj.propList);
        };
        this.getAnimatorController = function () {
            return animatorController;
        };
        this.getId = function () {
            return id;
        };

        this.calcAnim = function (steptime) {
            animatorController.calcAnim(this.obj3d, propList, steptime);
        };
        //0 to animDuration
        this.getAnimTime = function (gname) {
            return propList.groupList[gname].time;
        };
        this.getAnimDuration = function () {

        };
        this.clean = function () {
            core.geos.globalGeos.delElemRef(this.obj3d);
            animatorController.unregisterAnimatorObject(propList);
        };
        this.setGroupAnimClipIndex = function (gname, index) {
            animatorController.setGroupAnimClipIndex(propList, gname, index);
        };
        this.setGroupAnimClipIndexInterpolation = function (gname, index, userDuration) {
            animatorController.setGroupAnimClipIndexInterpolation(propList, gname, index, userDuration);
        };
        this.getBoneMatrix = function (boneName) {
            for (var i = 0; i < animatorController.mesh.bonelist.length; i++) {
                var bone = animatorController.mesh.bonelist[i];
                var node = bone.nodeObj;
                if (node.name === boneName) {
                    var mat = mat4.clone(propList.calculatedBoneMatrix[i]);
                    return mat4.transpose(mat, mat);
                }
            }
            return mat4.create();
        };
        this.boneGoToOrigo = function (boneName) {
            //go to origo
            if (boneName === undefined)
            {
                this.obj3d.setExtMatrix(undefined);
            } else {
                var sm = this.getBoneMatrix(boneName);
                mat4.invert(sm, sm);
                this.obj3d.setExtMatrix(sm);
            }
        };
        /*
         Szükséges még beállítani hogy milyen Node-hoz addjuk hozzá (setChildrenNode), mivel ez csak magához a modellhez adjuk hozzá
         */
        this.addChildren = function (children, boneName, subMatrixFromExt) {
            var id = children.getGeoId();
            if (propList.childrenList[id] === undefined) {
                propList.childrenList[id] = children;
                propList.childrenListBone[id] = boneName;
                children.setParent(this, subMatrixFromExt);
                //this.sendParentMatrixToChilds();
            }
        };
        this.delChildren = function (children, subMatrixFromExt) {


        };
        this.setChildrenBone = function (childId, boneName) {
            propList.childrenListBone[childId] = boneName;
        };
        this.getChildrenList = function () {
            var ret = [];
            for (var i in propList.childrenList){
                ret.push({id: i, boneName: propList.childrenListBone[i]});
            }
            return ret;
        };
        this.setTex = function (t) {
            this.obj3d.setTexture(t);
        };
        init(this);
        this.calcAnim(0);

    };

    this.draw = function () {
        animatorMeshGeos.draw();
    };
    this.calc = function (steptime) {
        for (var i = 1; i < animatorMeshList.length; i++) {
            animatorMeshList[i].calcAnim(steptime);
        }
    };
    this.getAnimatorMesh = function (id) {
        return animatorMeshList[id];
    };
    this.getAnimatorMeshIdList = function () {
        var ret = [];
        for (var i in animatorMeshList)
            ret.push(i);
        return ret;
    };
    var loadShader = function () {
        var fragment =
                "uniform sampler2D uTex; \n" +
                "uniform highp vec3 uSzin; \n" +
                "uniform int uTexUse; \n" +
                "uniform highp float uAlpha; \n" +
                "varying highp vec3 vFeny; \n" +
                "varying highp vec2 vTexcoord; \n" +
                "void main(void) { \n" +
                "   highp vec4 texcolor = vec4(1.0); \n" +
                "   if(uTexUse == 1){\n" +
                "       texcolor = texture2D(uTex, vTexcoord); \n" +
                "   }\n" +
                "   gl_FragColor = vec4(texcolor.rgb * uSzin * vFeny,uAlpha); \n" +
                "}";
        var createString_BoneMat = function (index) {
            return "mat4(texture2D(uBoneMat, vec2(0.125," + index + "))," +
                    "texture2D(uBoneMat, vec2(0.375," + index + "))," +
                    "texture2D(uBoneMat, vec2(0.625," + index + "))," +
                    "texture2D(uBoneMat, vec2(0.875," + index + ")))";
        };
        var vertex =
                "attribute vec3 aVertexPosition; \n" +
                "attribute vec3 aNormal; \n" +
                "attribute vec2 aTexture; \n" +
                "attribute vec4 aWeights; \n" +
                "attribute vec4 aBoneMatIndex; \n" +
                "uniform sampler2D uBoneMat; \n" +
                "uniform mat4 uMVMatrix; \n" +
                "uniform int uUseAnim; \n" +
                "uniform mat4 uCMatrix; \n" +
                "uniform mat4 uPMatrix; \n" +
                "uniform vec3 uFenySzin[6]; \n" +
                "varying highp vec3 vFeny; \n" +
                "varying highp vec2 vTexcoord; \n" +
                "highp float osszFeny; \n" +
                "vec3 transformedNormal; \n" +
                "void addFeny(highp vec3 normal, highp vec3 szin){ \n" +
                "   highp float ero = max(dot(transformedNormal, normal), 0.0); \n" +
                "   vFeny += szin * ero; \n" +
                "   osszFeny += ero; \n" +
                "} \n" +
                "void main(void) { \n" +
                "   highp vec3 norm = vec3(0.0);\n" +
                "   highp vec4 pos = vec4(0.0);\n" +
                "   if (uUseAnim == 1){\n" +
                "       mat4 bonemat;\n" +
                "       bonemat = " + createString_BoneMat("aBoneMatIndex.x") + ";\n" +
                "       pos += (bonemat * vec4(aVertexPosition,1.0)) * aWeights.x;\n" +
                "       norm += (mat3(bonemat) * aNormal) * aWeights.x;\n" +
                "       if (aWeights.y != 0.0){\n" +
                "           bonemat = " + createString_BoneMat("aBoneMatIndex.y") + ";\n" +
                "           pos += (bonemat * vec4(aVertexPosition,1.0)) * aWeights.y;\n" +
                "           norm += (mat3(bonemat) * aNormal) * aWeights.y;\n" +
                "           if (aWeights.z != 0.0){\n" +
                "               bonemat = " + createString_BoneMat("aBoneMatIndex.z") + ";\n" +
                "               pos += (bonemat * vec4(aVertexPosition,1.0)) * aWeights.z;\n" +
                "               norm += (mat3(bonemat) * aNormal) * aWeights.z;\n" +
                "               bonemat = " + createString_BoneMat("aBoneMatIndex.w") + ";\n" +
                "               pos += (bonemat * vec4(aVertexPosition,1.0)) * aWeights.w;\n" +
                "               norm += (mat3(bonemat) * aNormal) * aWeights.w;\n" +
                "           } \n" +
                "       } \n" +
                "   } else {\n" +
                "       pos = vec4(aVertexPosition,1.0);\n" +
                "       norm = aNormal;\n" +
                "   } \n" +
                "   gl_Position = uPMatrix * (uCMatrix * uMVMatrix) * pos; \n" +
                "   mat3 uMVMatrix3 = mat3(uMVMatrix); osszFeny = 0.0; vFeny = vec3(0.0);\n" +
                "   transformedNormal = uMVMatrix3 * norm; \n" +
                "   addFeny(vec3( 1.0,0.0,0.0),uFenySzin[0]); \n" +
                "   addFeny(vec3(-1.0,0.0,0.0),uFenySzin[1]); \n" +
                "   addFeny(vec3(0.0, 1.0,0.0),uFenySzin[2]); \n" +
                "   addFeny(vec3(0.0,-1.0,0.0),uFenySzin[3]); \n" +
                "   addFeny(vec3(0.0,0.0, 1.0),uFenySzin[4]); \n" +
                "   addFeny(vec3(0.0,0.0,-1.0),uFenySzin[5]); \n" +
                "   vFeny /= osszFeny; \n" +
                "   vTexcoord = aTexture; \n" +
                "}";
        shaderObj = core.shader.loadShader(fragment, vertex, "animator", []);
        animatorMeshGeos.customShader = shaderObj;
    };
    loadShader();
};
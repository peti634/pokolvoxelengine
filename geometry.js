/* global PokolVoxelEngine*/

/*
 * PokolVoxelEngine v0.7 - 2018.02.22.
 * PokolStudio - peti634
 */

PokolVoxelEngine.prototype.geometryClass = function (core) {
    var gl = core.ogl;
    this.geometryObject = function () {
        var VertexBuff = gl.createBuffer();
        var NormalBuff = gl.createBuffer();
        var TextureBuff = gl.createBuffer();
        var ElementBuff = gl.createBuffer();
        var VertexData = [];
        var TextureData = [];
        var ElementData = [];
        var VertexData = [];
        var TextureEnable = false;
        var NormalEnable = false;
        this.select = function () {
            core.shader.shaderAtrribute("aVertexPosition", VertexBuff);
            core.shader.shaderAtrribute("aNormal", NormalBuff);
            core.shader.shaderAtrribute("aTexture", TextureEnable ? TextureBuff : undefined);
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ElementBuff);
        };
        ///////////////////////////////Texcoord////////////////////////////////////////////
        this.setTexcoordData = function (texcoordData) {
            TextureData = texcoordData;
            if (TextureData === undefined) {
                TextureEnable = false;
            } else {
                TextureEnable = true;
                var data = new Float32Array(TextureData);
                gl.bindBuffer(gl.ARRAY_BUFFER, TextureBuff);
                gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
            }
        };
        /////////////////////////////////////////////////////////////////////////////////
        this.getTextureBuff = function () {
            return TextureBuff;
        };

        ///////////////////////////////Vertex////////////////////////////////////////////
        this.setVertexData = function (vertexData) {
            VertexData = vertexData;
            gl.bindBuffer(gl.ARRAY_BUFFER, VertexBuff);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(VertexData), gl.STATIC_DRAW);
        };
        /////////////////////////////////////////////////////////////////////////////////
        this.getVertexBuff = function () {
            return VertexBuff;
        };
        ///////////////////////////////Vertex////////////////////////////////////////////
        this.setNormalData = function (normdata) {
            gl.bindBuffer(gl.ARRAY_BUFFER, NormalBuff);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normdata), gl.STATIC_DRAW);
        };
        /////////////////////////////////////////////////////////////////////////////////
        this.getNormalBuff = function () {
            return NormalBuff;
        };
        ////////////////////////////////////////Element//////////////////////////////////
        this.setElementData = function (data) {
            ElementData = data;
            ElementBuff.length = data.length;
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ElementBuff);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(data), gl.STATIC_DRAW);
        };
        /////////////////////////////////////////////////////////////////////////////////
        this.getElementBuff = function () {
            return ElementBuff;
        };
        this.getElementLength = function(){
            return ElementBuff.length;
        };

    };
};
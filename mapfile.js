/* global PokolVoxelEngine */

document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/worker_terrtovox.js'></script>");
////////////////BUFFERS/////////////////////
PokolVoxelEngine_dataBufferSave = function (BUFFER_INC_STEP) {
    if (BUFFER_INC_STEP === undefined)
        BUFFER_INC_STEP = 100;
    this.Buffer = new Uint8Array(BUFFER_INC_STEP);
    this.index_max = 0;
    this.index_seek = 0;
    this.Buffer_sizetest = function (len) {
        if (len > this.Buffer.length - this.index_seek) {
            var uj = new Uint8Array(this.Buffer.length + len + BUFFER_INC_STEP);
            uj.set(this.Buffer);
            this.Buffer = uj;
        }
    };
    this.Buffer_addChar = function (c) {
        this.Buffer_sizetest(1);
        this.Buffer[this.index_seek++] = c;
        return this.index_seek - 1;
    };
    this.Buffer_addInt = function (i) {
        this.Buffer_sizetest(4);
        this.Buffer.set(new Uint8Array(new Int32Array([i]).buffer, 0, 4), this.index_seek);
        this.index_seek += 4;
        return this.index_seek - 4;
    };
    this.Buffer_addUint = function (u) {
        this.Buffer_sizetest(4);
        this.Buffer.set(new Uint8Array(new Uint32Array([u]).buffer, 0, 4), this.index_seek);
        this.index_seek += 4;
        return this.index_seek - 4;
    };
    this.Buffer_addWord = function (w) {
        this.Buffer_sizetest(2);
        this.Buffer.set(new Uint8Array(new Uint16Array([w]).buffer, 0, 2), this.index_seek);
        this.index_seek += 2;
        return this.index_seek - 2;
    };
    this.Buffer_addString = function (str) {
        var vlen = Math.min(str.length, 0xFFFF);
        this.Buffer_sizetest(2 + (vlen * 2));
        this.Buffer_addWord(vlen);
        for (var i = 0; i < vlen; i++) {
            this.Buffer_addWord(str.charCodeAt(i));
        }
        return this.index_seek - (2 + (vlen * 2));
    };
    this.Seek = function (seek) {
        this.index_max = this.index_seek;
        this.index_seek = seek;
    };
    this.ResetSeek = function () {
        this.index_seek = this.index_max;
    };
    this.Buffer_getBuffer = function () {
        return this.Buffer.subarray(0, this.index_seek);
    };
};
PokolVoxelEngine_dataBufferLoad = function (data) {
    this.data = new Uint8Array(data);
    this.dataindex = 0;
    this.getChar = function () {
        return this.data[this.dataindex++];
    };
    this.getWord = function () {
        return new Int16Array((new Uint8Array([this.data[this.dataindex++], this.data[this.dataindex++]])).buffer, 0, 1) [0];
    };
    this.getInt = function () {
        var uarray = new Uint8Array([this.data[this.dataindex++], this.data[this.dataindex++], this.data[this.dataindex++], this.data[this.dataindex++]]);
        return new Int32Array(uarray.buffer, 0, 1)[0];
    };
    this.getUInt = function () {
        var uarray = new Uint8Array([this.data[this.dataindex++], this.data[this.dataindex++], this.data[this.dataindex++], this.data[this.dataindex++]]);
        return new Uint32Array(uarray.buffer, 0, 1)[0];
    };
    this.getString = function () {
        var len = this.getWord();
        var ret = "";
        for (var i = 0; i < len; i++) {
            ret += String.fromCharCode(this.getWord());
        }
        return ret;
    };
};

function PokolVoxelEngine_MapSave()
{
    this.versio = 10;
    this.buffer;
    this.terr_buffer;
    this.voxelterr_buffer;
    this.zip;
    this.Start = function (saveVoxel_Size, saveTerrain_Size) {
        this.buffer = new PokolVoxelEngine_dataBufferSave();
        this.buffer.Buffer_addChar("P".charCodeAt(0));
        this.buffer.Buffer_addChar("V".charCodeAt(0));
        this.buffer.Buffer_addChar("M".charCodeAt(0));
        this.buffer.Buffer_addChar(this.versio);

        this.buffer.Buffer_addUint(saveTerrain_Size);
        this.buffer.Buffer_addUint(saveVoxel_Size);

        if (saveTerrain_Size !== 0)
            this.terr_buffer = new PokolVoxelEngine_dataBufferSave();
        if (saveVoxel_Size !== 0)
            this.voxelterr_buffer = new PokolVoxelEngine_dataBufferSave();
        this.zip = new JSZip();
        this.zip.file("info.bin", this.buffer.Buffer_getBuffer(), {binary: true});
    };
    this.AddVoxelTerrain = function (xpos, ypos, zpos, ysize, data, objdata) {
        xpos = xpos | 0;
        zpos = zpos | 0;
        this.voxelterr_buffer.Buffer_addUint(xpos);//xpos
        this.voxelterr_buffer.Buffer_addUint(zpos);//zpos
        this.voxelterr_buffer.Buffer_addInt(ypos);//ypos
        this.voxelterr_buffer.Buffer_addUint(ysize);//ysize
        this.voxelterr_buffer.Buffer_addUint(objdata.byteLength);//objdatalen

        var dir = this.zip.folder("vox" + xpos + "_" + zpos);
        var u8data = new Uint8Array(data.buffer, 0, data.byteLength);
        dir.file("blocks.bin", u8data, {binary: true});
        var u8obj = new Uint8Array(objdata.buffer, 0, objdata.byteLength);
        dir.file("obj.bin", u8obj, {binary: true});
    };

    this.AddTerrain = function (xpos, zpos, height_data, tipus_data) {
        xpos = xpos | 0;
        zpos = zpos | 0;
        this.terr_buffer.Buffer_addUint(xpos);//xpos
        this.terr_buffer.Buffer_addUint(zpos);//zpos

        var dir = this.zip.folder("ter" + xpos + "_" + zpos);

        var u8data = new Uint8Array(height_data.buffer, 0, height_data.length * 4);
        dir.file("height.bin", u8data, {binary: true});

        var u8data = new Uint8Array(tipus_data.buffer, 0, tipus_data.length * 2);
        dir.file("tipus.bin", u8data, {binary: true});

    };

    this.Save = function (zipnev)
    {
        if (this.terr_buffer !== undefined)
            this.zip.file("tinfo.bin", this.terr_buffer.Buffer_getBuffer(), {binary: true});
        if (this.voxelterr_buffer !== undefined)
            this.zip.file("vtinfo.bin", this.voxelterr_buffer.Buffer_getBuffer(), {binary: true});
        //STORE 
        var content = this.zip.generate({type: "blob", compression: "DEFLATE", compressionOptions: {level: 6}});

        var letolt = document.createElement("a");
        letolt.href = URL.createObjectURL(content);
        letolt.download = zipnev;
        document.body.appendChild(letolt);
        letolt.click();
        document.body.removeChild(letolt);

    };
}

function PokolVoxelEngine_MapLoad() {
    var _this = this;
    this.load_cb;
    this.load_cbex;

    this.VERZIO = 10;

    this.ERROR_ZIP = 1;
    this.ERROR_NO_INFO = 2;
    this.ERROR_NO_TINFO = 3;
    this.ERROR_NO_BTINFO = 20;

    this.ERROR_HEADSTR = 4;
    this.ERROR_VERZIO = 5;
    this.ERROR_NO_HEIGHTBIN = 6;
    this.ERROR_NO_TIPUSBIN = 7;
    this.ERROR_NO_BLOCKSBIN = 8;
    this.ERROR_BLOCK_SIZE = 9;
    this.ERROR_FILEREAD = 10;
    this._readok = function (contents) {

        var cb_data = {load_cbex: this.load_cbex, error: 0};
        try {


            try
            {
                //zip decomp
                var zip = JSZip();
                zip.load(contents);
            } catch (error) {
                throw this.ERROR_ZIP;
            }
            //read
            cb_data.terr_list = [];
            cb_data.voxel_list = [];
            var info_file = zip.file("info.bin");
            if (info_file === null) {
                throw this.ERROR_NO_INFO;
            }
            var info_buffer = new PokolVoxelEngine_dataBufferLoad(info_file.asUint8Array());

            var headstr = String.fromCharCode(info_buffer.getChar()) + String.fromCharCode(info_buffer.getChar()) + String.fromCharCode(info_buffer.getChar());
            if (headstr !== "PVM") {
                throw this.ERROR_HEADSTR;
            }
            var version = info_buffer.getChar();
            if (version !== this.VERZIO) {
                throw this.ERROR_VERZIO;
            }
            cb_data.terrain_size = info_buffer.getUInt();
            cb_data.voxelterrain_size = info_buffer.getUInt();


            if (cb_data.terrain_size !== 0) {
                var tinfo_file = zip.file("tinfo.bin");
                if (tinfo_file === null) {
                    throw this.ERROR_NO_TINFO;
                }

                var tinfo_buffer = new PokolVoxelEngine_dataBufferLoad(tinfo_file.asUint8Array());
                //terrain
                while (tinfo_buffer.dataindex < tinfo_buffer.data.length) {
                    var x = tinfo_buffer.getUInt();
                    var z = tinfo_buffer.getUInt();
                    var elem = {};
                    elem.x = x;
                    elem.z = z;

                    var hfile = zip.file("ter" + x + "_" + z + "/height.bin");
                    if (hfile === null) {
                        throw this.ERROR_NO_HEIGHTBIN;
                    }
                    var tfile = zip.file("ter" + x + "_" + z + "/tipus.bin");

                    if (tfile === null) {
                        throw this.ERROR_NO_TIPUSBIN;
                    }
                    var height = hfile.asUint8Array();
                    height = new Float32Array(height.buffer, 0, height.length / 4);


                    var tipus = tfile.asUint8Array();
                    tipus = new Uint16Array(tipus.buffer, 0, tipus.length / 2);

                    elem.height = height;
                    elem.tipus = tipus;
                    cb_data.terr_list.push(elem);

                }
            }
            if (cb_data.voxelterrain_size !== 0) {

                var btinfo_file = zip.file("vtinfo.bin");
                if (tinfo_file === null) {
                    throw this.ERROR_NO_BTINFO;
                }

                var btinfo_buffer = new PokolVoxelEngine_dataBufferLoad(btinfo_file.asUint8Array());
                //blocks
                while (btinfo_buffer.dataindex < btinfo_buffer.data.length) {

                    var x = btinfo_buffer.getUInt();
                    var z = btinfo_buffer.getUInt();
                    var ypos = btinfo_buffer.getInt();
                    var ysize = btinfo_buffer.getUInt();//ell
                    var objbytelen = btinfo_buffer.getUInt();//ell
                    var elem = {};
                    elem.x = x;
                    elem.z = z;
                    elem.y = ypos;

                    var bfile = zip.file("vox" + x + "_" + z + "/blocks.bin");
                    var ofile = zip.file("vox" + x + "_" + z + "/obj.bin");

                    if (bfile === null) {
                        throw this.ERROR_NO_BLOCKSBIN;
                    }

                    var blocks = bfile.asUint8Array();
                    blocks = new Uint16Array(blocks.buffer, 0, blocks.length / 2);

                    if ((cb_data.voxelterrain_size * cb_data.voxelterrain_size * ysize) !== blocks.length) {
                        //a valós adat nem egyezik meg az elmentettel
                        throw this.ERROR_BLOCK_SIZE;
                    }

                    var objects = ofile.asUint8Array();

                    if (objbytelen !== objects.length) {
                        //a valós adat nem egyezik meg az elmentettel
                        throw this.ERROR_BLOCK_SIZE;
                    }
                    objects = new Uint32Array(objects.buffer, 0, objects.length / 4);
                    elem.blocks = blocks;
                    elem.objdata = objects;
                    cb_data.voxel_list.push(elem);


                }
            }

            this.load_cb(cb_data);

        } catch (error) {
            cb_data.error = error;
            this.load_cb(cb_data);
        }


    };

    this._onread = function (e) {
        _this._readok(e.target.result);
    };
    this._onerror = function () {
        this.load_cb({error: this.ERROR_FILEREAD});
    };
    this._onload = function (event) {

        var file = event.target.files[0];
        if (!file) {
            return;
        }
        var reader = new FileReader();
        reader.onload = _this._onread;
        reader.onerror = _this._onerror;
        reader.readAsBinaryString(file);
    };
    this.open = function () {
        var input = document.createElement("INPUT");
        document.body.appendChild(input);
        input.type = "file";
        input.style.display = "none";
        input.onchange = this._onload;
        input.focus();
        input.click();

    };
    this.loadData = function (data) {
        this._readok(data);
    };
}


function PokolVoxelEngine_DefaultsaveMap_options() {
    //0 - nem menti a terrain adatokat
    //1 - nyers terrain adatok mentése
    //2 - terrain adatok mentése a voxel terrainba
    this.saveTerrainData = 0;
    this.saveVoxelData = true; //voxel adatok mentése
}

PokolVoxelEngine.prototype.saveMap = function (name, options) {


    var ment = new PokolVoxelEngine_MapSave();
    var terr_size = 0;
    if (options.saveTerrainData === 1)
        terr_size = this.terrainVilag.blockdarab + 1;

    var voxel_size = 0;
    if (options.saveVoxelData)
        voxel_size = this.voxelVilag.m_xz;
    ment.Start(voxel_size, terr_size);

    if (options.saveTerrainData === 1) {
        var list = this.terrainVilag.getTerrainList();
        for (var i = 0; i < list.length; i++) {
            var x = list[i].x;
            var z = list[i].z;
            var terr = this.terrainVilag.getTerrain(x, z);
            if (terr === undefined)
                continue;
            ment.AddTerrain(x, z, terr.VertexHeight, terr.Tipus);
        }
    }



    if (options.saveVoxelData) {
        var list = this.voxelVilag.getTerrainList();
        for (var i = 0; i < list.length; i++) {
            var x = list[i].x;
            var z = list[i].z;
            var terr = this.voxelVilag.getTerrain(x, z);
            if (terr === undefined)
                continue;
            //terrain data kiszedése
            var terdata = new PokolVoxelEngine_dinamic_3d_array(terr.mxz, Uint16Array);
            terdata.copyFrom(terr.blockdata);
            if (options.saveTerrainData === 2 && terr.blockdata_terr === undefined) {
                var terlist = this.terrainVilag.collectTerrainForVoxelTerrain(x, z, this.voxelVilag.m_xz);
                var retdata = PokolVoxelEngine_terrtovox(this.terrainVilag.blockdarab, terlist, x, z, this.voxelVilag.m_xz, this.voxelVilag.blocksize);
                for (var di = 0; di < retdata.length; di += 4) {
                    var dx = retdata[di + 0];
                    var dy = retdata[di + 1];
                    var dz = retdata[di + 2];
                    var dt = retdata[di + 3];
                    terdata.y_teszt(dy);
                    terdata.setElem(dx, dy, dz, dt);
                }
            }
            if (options.saveTerrainData !== 2 && terr.blockdata_terr !== undefined) {
                for (var dx = 0; dx < terr.blockdata_terr.mxz; dx++) {
                    for (var dy = 0; dy < terr.blockdata_terr.my; dy++) {
                        for (var dz = 0; dz < terr.blockdata_terr.mxz; dz++) {
                            var vy = dy + terr.blockdata_terr.ypos;
                            if (terr.blockdata_terr.getElem(dx, vy, dz) === 1) {
                                terdata.setElem(dx, vy, dz, 0);
                            }
                        }
                    }
                }

            }
            terdata.cleanFreeLevel();
            var terdataobj = new this.dinamicArray(1000, Uint32Array);

            for (var oi = 0; oi < terr.geos.geo_list.length; oi++) {
                var elem = terr.geos.geo_list[oi];
                if (elem.infodata32 === undefined || elem.typeid === undefined)
                    continue;
                var ex = ((elem.pos[0] / terr.blocksize) | 0) - (x * terr.mxz);
                var ey = ((elem.pos[1] / terr.blocksize) | 0) + 0x7FFFFFFF;
                var ez = ((elem.pos[2] / terr.blocksize) | 0) - (z * terr.mxz);
                terdataobj.add_4Item(ex, ey, ez, elem.type);

                var retdata = elem.infodata32;
                terdataobj.add_1Item(retdata.length * 4);
                terdataobj.add_data(retdata);
            }

            var ujterdataobj = new Uint32Array(terdataobj.pos);
            ujterdataobj.set(terdataobj.buffer.subarray(0, terdataobj.pos), 0);
            ment.AddVoxelTerrain(x, terdata.ypos, z, terdata.my, terdata.buffer, ujterdataobj);
        }
    }
    ment.Save(name);
};
PokolVoxelEngine.prototype.loadMap_cb_proto = function (core, data) {
    var terr_darab = data.terrain_size;
    var voxel_darab = data.voxelterrain_size;
    if (data.error === 0) {
        var newterr;
        var newvoxel;
        if (terr_darab !== 0) {
            newterr = core.newTerrainVilag();
            data.terrainvilag = newterr;
        }
        if (voxel_darab !== 0) {
            newvoxel = core.newVoxelVilag();
            data.newvoxelvilag = newvoxel;
        }
        var ret = data.load_cbex(data);
        if (ret === true) {
            this.getArray = function (arr, x, y) {
                return arr[x + (y * terr_darab)];
            };
            if (terr_darab !== 0) {
                newterr.setRelativePos(core.terrainVilag.getRelativePos());
                core.terrainVilag = newterr;

                this.addTerr = function (kx, kz, vx, vz) {
                    kx = (kx / newterr.blockdarab) | 0;
                    kz = (kz / newterr.blockdarab) | 0;
                    vx = (vx / newterr.blockdarab) | 0;
                    vz = (vz / newterr.blockdarab) | 0;
                    for (var i = kx; i < vx; i++) {
                        for (var j = kz; j < vz; j++) {

                            newterr.AddTerrain(i, j);
                        }
                    }
                };
                for (var ii = 0; ii < data.terr_list.length; ii++) {
                    var terr = data.terr_list[ii];
                    var x = terr.x * terr_darab;
                    var z = terr.z * terr_darab;
                    this.addTerr(x, z, x + terr_darab, z + terr_darab);
                }


                for (var ii = 0; ii < data.terr_list.length; ii++) {
                    var terr = data.terr_list[ii];
                    var x = terr.x * terr_darab;
                    var z = terr.z * terr_darab;
                    var x2 = terr.x * (terr_darab - 1);
                    var z2 = terr.z * (terr_darab - 1);

                    for (var i = 0; i < terr_darab; i++) {
                        for (var j = 0; j < terr_darab; j++) {
                            var e = this.getArray(terr.height, i, j);
                            newterr.SetHeight(i + x2, j + z2, e);
                            var e = this.getArray(terr.tipus, i, j);

                            newterr.SetTipus(i + x2, j + z2, e);
                        }
                    }
                }
            }
            if (voxel_darab !== -1) {
                newvoxel.setRelativePos(core.voxelVilag.getRelativePos());
                core.voxelVilag = newvoxel;
                this.addVoxelTerr = function (kx, kz, vx, vz) {
                    kx = (kx / newvoxel.m_xz) | 0;
                    kz = (kz / newvoxel.m_xz) | 0;
                    vx = (vx / newvoxel.m_xz) | 0;
                    vz = (vz / newvoxel.m_xz) | 0;
                    for (var i = kx; i < vx; i++) {
                        for (var j = kz; j < vz; j++) {

                            newvoxel.addTerrain(i, j);
                        }
                    }
                };
                for (var ii = 0; ii < data.voxel_list.length; ii++) {
                    var terr = data.voxel_list[ii];
                    var x = terr.x * voxel_darab;
                    var z = terr.z * voxel_darab;
                    this.addVoxelTerr(x, z, x + voxel_darab, z + voxel_darab);
                }
                for (var ii = 0; ii < data.voxel_list.length; ii++) {
                    var terr = data.voxel_list [ii];
                    var x = terr.x * voxel_darab;
                    var y = terr.y;
                    var z = terr.z * voxel_darab;
                    var objlist = [];
                    for (var obdi = 0; obdi < terr.objdata.length; ) {
                        var ox = terr.objdata[obdi++];
                        var oy = terr.objdata[obdi++] - 0x7FFFFFFF;
                        var oz = terr.objdata[obdi++];
                        var otype = terr.objdata[obdi++];
                        var odatalen = (terr.objdata[obdi++] / 4) | 0;
                        var odata = new Uint32Array(odatalen);
                        odata.set(terr.objdata.subarray(obdi, obdi + odatalen), 0);
                        obdi += odatalen;
                        objlist["o" + ox + "_" + oy + "_" + oz] = {type: otype, data: odata};
                    }


                    for (var i = 0; i < terr.blocks.length; i++) {
                        var elem = terr.blocks[i];
                        if (elem !== 0) {
                            var vx = i % voxel_darab;
                            var vy = (i / (voxel_darab * voxel_darab)) | 0;
                            var vz = ((i / voxel_darab) | 0) % voxel_darab;
                            vx += x;
                            vy += y;
                            vz += z;
                            var obj;
                            var objelem = objlist["o" + vx + "_" + vy + "_" + vz];
                            if (objelem !== undefined) {
                                if (objelem.type !== elem) {
                                    throw "Obj type error";
                                }
                                obj = objelem.data;
                            }
                            newvoxel.addElem(vx, vy, vz, elem, obj);
                        }
                    }
                }
                newvoxel.update();

            }

        }
    } else {
        data.load_cbex(data);
    }
};
PokolVoxelEngine.prototype.loadMap = function (callback) {
    var betolt = new PokolVoxelEngine_MapLoad();
    betolt.load_cb = this.loadMap_cb;
    betolt.load_cbex = callback;
    betolt.open();
};
PokolVoxelEngine.prototype.loadMapData = function (data, callback) {
    var betolt = new PokolVoxelEngine_MapLoad();
    betolt.load_cb = this.loadMap_cb;
    betolt.load_cbex = callback;
    betolt.loadData(data);
};
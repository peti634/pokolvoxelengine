/* global PokolVoxelEngine*/
/* global vec3*/
/* global vec4*/
/* global mat4*/

/**
 *
 * @param {quat} out the receiving quaternion
 * @param {quat} m rotation matrix
 * @returns {quat} out
 */
quat.fromMat4 = function (out, m) {
    var m11 = m[ 0 ], m12 = m[ 4 ], m13 = m[ 8 ],
            m21 = m[ 1 ], m22 = m[ 5 ], m23 = m[ 9 ],
            m31 = m[ 2 ], m32 = m[ 6 ], m33 = m[ 10 ],
            trace = m11 + m22 + m33,
            s;
    if (trace > 0) {
        s = 0.5 / Math.sqrt(trace + 1.0);
        out[3] = 0.25 / s;
        out[0] = (m32 - m23) * s;
        out[1] = (m13 - m31) * s;
        out[2] = (m21 - m12) * s;
    } else if (m11 > m22 && m11 > m33) {
        s = 2.0 * Math.sqrt(1.0 + m11 - m22 - m33);
        out[3] = (m32 - m23) / s;
        out[0] = 0.25 * s;
        out[1] = (m12 + m21) / s;
        out[2] = (m13 + m31) / s;
    } else if (m22 > m33) {
        s = 2.0 * Math.sqrt(1.0 + m22 - m11 - m33);
        out[3] = (m13 - m31) / s;
        out[0] = (m12 + m21) / s;
        out[1] = 0.25 * s;
        out[2] = (m23 + m32) / s;
    } else {
        s = 2.0 * Math.sqrt(1.0 + m33 - m11 - m22);
        out[3] = (m21 - m12) / s;
        out[0] = (m13 + m31) / s;
        out[1] = (m23 + m32) / s;
        out[2] = 0.25 * s;
    }
    return out;
};

/**
 *
 * @param {quat} out the receiving vec3 eulerian angles
 * @param {quat} quat
 * @returns {quat} out
 */
quat.toEulerian = function (out, quat) {
    var ysqr = quat[1] * quat[1];
    var t0 = -2 * (ysqr + quat[2] * quat[2]) + 1;
    var t1 = +2 * (quat[0] * quat[1] - quat[3] * quat[2]);
    var t2 = -2 * (quat[0] * quat[2] + quat[3] * quat[1]);
    var t3 = +2 * (quat[1] * quat[2] - quat[3] * quat[0]);
    var t4 = -2 * (quat[0] * quat[0] + ysqr) + 1;

    if (t2 > 1)
        t2 = 1;
    if (t2 < -1)
        t2 = -1;

    out[0] = Math.atan2(t3, t4);
    out[1] = Math.asin(t2);
    out[2] = Math.atan2(t1, t0);
};

PokolVoxelEngine.prototype.math = {
    vec3man: {
        get: function (arr, index) {
            index *= 3;
            return [arr[index], arr[index + 1], arr[index + 2]];
        },
        set: function (arr, index, vec) {
            index *= 3;
            arr[index] = vec[0];
            arr[index + 1] = vec[1];
            arr[index + 2] = vec[2];
        }
    },
    angleToCircle: function (a) {
        a += 180;
        if (a < 0)
            a = 360 + (a % 360);
        return (Math.abs((a) % 360)) - 180;
    },
    radToDeg: function (a) {
        return a * 180 / Math.PI;
    },
    degToRad: function (degrees) {
        return degrees * Math.PI / 180;
    },
    isInt: function (n) {
        return n !== "" && !isNaN(n) && Math.round(n) === n;
    },
    isFloat: function (n) {
        return n !== "" && !isNaN(n) && Math.round(n) !== n;
    },
    IsPowerOfTwo: function (x) {
        return (x & (x - 1)) === 0;
    },
    getAngle: function (a, b) {
        var x = a[0] - b[0];
        var y = a[1] - b[1];
        return Math.atan2(x, y);
    },
    rotateToPoint: function (pos, t, fok) {
        return [pos[0] + (t * Math.sin(fok)), pos[1] + (t * Math.cos(fok))];
    },
    getAngle3DX: function (v0, v1) {
        var v = vec3.sub([0, 0, 0], v1, v0);
        var yhossz = v[0];
        var xhossz = vec3.dist([yhossz, 0, 0], v);
        return [this.getAngle([yhossz, xhossz], [0, 0]), this.getAngle([0, 0], [v[2], v[1]])];
    },
    getAngle3DY: function (v0, v1) {
        var v = vec3.sub([0, 0, 0], v0, v1);
        var yhossz = v[1];
        var xhossz = vec3.dist([0, yhossz, 0], v);
        return [this.getAngle([yhossz, xhossz], [0, 0]), this.getAngle([0, 0], [v[0], v[2]])];
    },
    getAngle3DZ: function (v0, v1) {
        var v = vec3.sub([0, 0, 0], v0, v1);
        var yhossz = v[2];
        var xhossz = vec3.dist([0, 0, yhossz], v);
        return [this.getAngle([yhossz, xhossz], [0, 0]), this.getAngle([0, 0], [v[0], v[1]])];
    },
    glUnproject: function (degToRad, vec, camangle, proj, viewport) {
        var unproj_obj = {dest: [0, 0, 0], m: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], cam: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], v: [0, 0, 0, 0], tv: [0, 0, 0, 0]};
        var dest = unproj_obj.dest; //output
        var m = unproj_obj.m; //view * proj
        var cam = unproj_obj.cam;
        var v = unproj_obj.v; //vector
        var tv = unproj_obj.tv; //transformed vector

        mat4.identity(cam);
        //apply viewport transform
        v[0] = vec[0] * 2.0 / viewport[0] - 1.0;
        v[1] = (viewport[1] - vec[1]) * 2.0 / viewport[1] - 1.0;
        v[2] = 0.0;
        v[3] = 1.0;
        mat4.rotate(cam, cam, degToRad(camangle[0]), [1, 0, 0]);
        mat4.rotate(cam, cam, degToRad(camangle[1]), [0, 1, 0]);
        //build and invert viewproj matrix
        mat4.multiply(m, proj, cam);
        if (!mat4.invert(m, m)) {
            return null;
        }


        vec4.transformMat4(tv, v, m);
        dest[0] = tv[0];
        dest[1] = tv[1];
        dest[2] = tv[2];
        return dest;
    },
    //Möller-Trumbore
    //http://en.m.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
    glRayPick: function (o, d, p0, p1, p2, retp) {
        var EPSILON = 0.0000001;
        var glRayPick_obj = {p12: [0, 0, 0], p13: [0, 0, 0], P: [0, 0, 0], T: [0, 0, 0], Q: [0, 0, 0]};
        var p12 = glRayPick_obj.p12;
        var p13 = glRayPick_obj.p13;
        vec3.sub(p12, p1, p0);
        vec3.sub(p13, p2, p0);
        var P = glRayPick_obj.P;
        vec3.cross(P, d, p13);
        var det = vec3.dot(p12, P);
        if (det > -EPSILON && det < EPSILON)
            return -1;
        var idet = 1 / det;
        var T = glRayPick_obj.T;
        vec3.sub(T, o, p0);
        var u = vec3.dot(T, P) * idet;
        if (u < 0 || u > 1)
            return -2;
        var Q = glRayPick_obj.Q;
        vec3.cross(Q, T, p12);
        var v = vec3.dot(d, Q) * idet;
        if (v < 0 || u + v > 1)
            return -3;
        var t = vec3.dot(p13, Q) * idet;
        if (t > EPSILON) {
            retp[0] = o[0] + (d[0] * t);
            retp[1] = o[1] + (d[1] * t);
            retp[2] = o[2] + (d[2] * t);
            return 1;
        }
        return -4;
    },
    //szélek nem lesznek ellenőrizve, végtelen széles 3szög
    glRayPick_onlyvec: function (o, d, p0, p1, p2, retp) {
        var EPSILON = 0.0000001;
        var glRayPick_obj = {p12: [0, 0, 0], p13: [0, 0, 0], P: [0, 0, 0], T: [0, 0, 0], Q: [0, 0, 0]};
        var p12 = glRayPick_obj.p12;
        var p13 = glRayPick_obj.p13;
        vec3.sub(p12, p1, p0);
        vec3.sub(p13, p2, p0);
        var P = glRayPick_obj.P;
        vec3.cross(P, d, p13);
        var det = vec3.dot(p12, P);
        if (det > -EPSILON && det < EPSILON)
            return -1;
        var idet = 1 / det;
        var T = glRayPick_obj.T;
        vec3.sub(T, o, p0);
        var Q = glRayPick_obj.Q;
        vec3.cross(Q, T, p12);
        var t = vec3.dot(p13, Q) * idet;
        if (t > EPSILON) {
            retp[0] = o[0] + (d[0] * t);
            retp[1] = o[1] + (d[1] * t);
            retp[2] = o[2] + (d[2] * t);
            return 1;
        }
        return -4;
    },
    glRayPick_uv: function (o, d, p0, p1, p2) {
        var EPSILON = 0.0000001;
        var glRayPick_obj = {p12: [0, 0, 0], p13: [0, 0, 0], P: [0, 0, 0], T: [0, 0, 0], Q: [0, 0, 0]};
        var p12 = glRayPick_obj.p12;
        var p13 = glRayPick_obj.p13;
        vec3.sub(p12, p1, p0);
        vec3.sub(p13, p2, p0);
        var P = glRayPick_obj.P;
        vec3.cross(P, d, p13);
        var det = vec3.dot(p12, P);
        if (det > -EPSILON && det < EPSILON)
            return -1;
        var idet = 1 / det;
        var T = glRayPick_obj.T;
        vec3.sub(T, o, p0);
        var u = vec3.dot(T, P) * idet;
        if (u < 0 || u > 1)
            return -2;
        var Q = glRayPick_obj.Q;
        vec3.cross(Q, T, p12);
        var v = vec3.dot(d, Q) * idet;
        if (v < 0 || u + v > 1)
            return -3;
        return [u, v];
    },
    rayPickGeometry: function (camposp, camangelp, onlyvec) {
        this.vertex_elementi = -1;
        var campos = [camposp[0], camposp[1], camposp[2]];
        var camangel = [camangelp[0], camangelp[1], camangelp[2]];
        var glRayPick = (onlyvec) ? PokolVoxelEngine.prototype.math.glRayPick_onlyvec : PokolVoxelEngine.prototype.math.glRayPick;
        this.setCampos = function (camposp) {
            campos = [camposp[0], camposp[1], camposp[2]];
        };
        this.setCamangle = function (camangelp) {
            camangel = [camangelp[0], camangelp[1], camangelp[2]];
        };
        this.plain = function (p1, p2, p3, p4) {
            var boxpos = [0, 0, 0];
            return glRayPick(campos, camangel, p1, p2, p3, boxpos) === 1 || glRayPick(campos, camangel, p2, p3, p4, boxpos) === 1;
        };
        this.tri = function (p1, p2, p3) {
            var boxpos = [0, 0, 0];
            if (glRayPick(campos, camangel, p1, p2, p3, boxpos) === 1)
                return boxpos;
            return undefined;
        };
        this.vertex = function (vertex, element, pos) {
            var ltav = Number.MAX_VALUE;
            var retpos = undefined;
            this.vertex_elementi = -1;
            for (var i = 0; i < element.length; i += 3) {
                var p1i = element[i] * 3;
                var p2i = element[i + 1] * 3;
                var p3i = element[i + 2] * 3;
                var p1 = [vertex[p1i + 0] + pos[0], vertex[p1i + 1] + pos[1], vertex[p1i + 2] + pos[2]];
                var p2 = [vertex[p2i + 0] + pos[0], vertex[p2i + 1] + pos[1], vertex[p2i + 2] + pos[2]];
                var p3 = [vertex[p3i + 0] + pos[0], vertex[p3i + 1] + pos[1], vertex[p3i + 2] + pos[2]];
                var ret = this.tri(p1, p2, p3);
                if (ret) {
                    var tav = vec3.distance(campos, ret);
                    if (ltav > tav) {
                        ltav = tav;
                        retpos = ret;
                        this.vertex_elementi = i;
                    }
                }
            }
            return retpos;
        };
        ///////////////nem csak egyenes plain////////////
        this.plain_getpos = function (p1, p2, p3, p4) {
            var boxpos1 = [0, 0, 0];
            var boxpos2 = [0, 0, 0];
            var pick1 = glRayPick(campos, camangel, p1, p2, p3, boxpos1) === 1;
            var pick2 = glRayPick(campos, camangel, p2, p3, p4, boxpos2) === 1;
            if (!pick1 && !pick2)
                return undefined;
            if (pick1 && pick2) {
                var tav1 = vec3.distance(campos, boxpos1);
                var tav2 = vec3.distance(campos, boxpos2);
                return (tav1 < tav2) ? boxpos1 : boxpos2;
            }
            return (pick1) ? boxpos1 : boxpos2;
        };
        this.plain_getposline = function (p1, p2, p3, p4) {
            var boxpos1 = [0, 0, 0];
            var boxpos2 = [0, 0, 0];
            var pick1 = glRayPick(campos, camangel, p1, p2, p3, boxpos1) === 1;
            var pick2 = glRayPick(campos, camangel, p2, p3, p4, boxpos2) === 1;
            return (pick1) ? boxpos1 : (pick2) ? boxpos2 : undefined;
        };
        this.box = function (pos, x, y, z) {
            var p_x_y_z = [pos[0], pos[1], pos[2]];
            var p_px_y_z = [pos[0] + x, pos[1], pos[2]];
            var p_x_y_pz = [pos[0], pos[1], pos[2] + z];
            var p_px_y_pz = [pos[0] + x, pos[1], pos[2] + z];
            var p_x_py_z = [pos[0], pos[1] + y, pos[2]];
            var p_px_py_z = [pos[0] + x, pos[1] + y, pos[2]];
            var p_x_py_pz = [pos[0], pos[1] + y, pos[2] + z];
            var p_px_py_pz = [pos[0] + x, pos[1] + y, pos[2] + z];
            if (this.plain(p_x_y_z, p_px_y_z, p_x_y_pz, p_px_y_pz))
                return true;
            if (this.plain(p_x_py_z, p_px_py_z, p_x_py_pz, p_px_py_pz))
                return true;
            if (this.plain(p_x_y_z, p_px_y_z, p_x_py_z, p_px_py_z))
                return true;
            if (this.plain(p_x_y_pz, p_px_y_pz, p_x_py_pz, p_px_py_pz))
                return true;
            if (this.plain(p_x_y_z, p_x_y_pz, p_x_py_z, p_x_py_pz))
                return true;
            if (this.plain(p_px_y_z, p_px_y_pz, p_px_py_z, p_px_py_pz))
                return true;
            return false;
        };
        this.box_getoldal = function (pos, x, y, z) {
            var p_x_y_z = [pos[0], pos[1], pos[2]];
            var p_px_y_z = [pos[0] + x, pos[1], pos[2]];
            var p_x_y_pz = [pos[0], pos[1], pos[2] + z];
            var p_px_y_pz = [pos[0] + x, pos[1], pos[2] + z];
            var p_x_py_z = [pos[0], pos[1] + y, pos[2]];
            var p_px_py_z = [pos[0] + x, pos[1] + y, pos[2]];
            var p_x_py_pz = [pos[0], pos[1] + y, pos[2] + z];
            var p_px_py_pz = [pos[0] + x, pos[1] + y, pos[2] + z];
            var pos1 = this.plain_getposline(p_x_y_z, p_px_y_z, p_x_y_pz, p_px_y_pz);
            var pos2 = this.plain_getposline(p_x_py_z, p_px_py_z, p_x_py_pz, p_px_py_pz);
            var pos3 = this.plain_getposline(p_x_y_z, p_px_y_z, p_x_py_z, p_px_py_z);
            var pos4 = this.plain_getposline(p_x_y_pz, p_px_y_pz, p_x_py_pz, p_px_py_pz);
            var pos5 = this.plain_getposline(p_x_y_z, p_x_y_pz, p_x_py_z, p_x_py_pz);
            var pos6 = this.plain_getposline(p_px_y_z, p_px_y_pz, p_px_py_z, p_px_py_pz);
            var tav = Number.POSITIVE_INFINITY;
            var ret = 0;
            var invertcampos = [campos[0], campos[1], campos[2]];
            if (pos1) {
                var atav = vec3.distance(pos1, invertcampos);
                if (atav < tav) {
                    ret = 1;
                    tav = atav;
                }
            }

            if (pos2) {
                var atav = vec3.distance(pos2, invertcampos);
                if (atav < tav) {
                    ret = 2;
                    tav = atav;
                }
            }

            if (pos3) {
                var atav = vec3.distance(pos3, invertcampos);
                if (atav < tav) {
                    ret = 3;
                    tav = atav;
                }
            }
            if (pos4) {
                var atav = vec3.distance(pos4, invertcampos);
                if (atav < tav) {
                    ret = 4;
                    tav = atav;
                }
            }
            if (pos5) {
                var atav = vec3.distance(pos5, invertcampos);
                if (atav < tav) {
                    ret = 5;
                    tav = atav;
                }
            }
            if (pos6) {
                var atav = vec3.distance(pos6, invertcampos);
                if (atav < tav) {
                    ret = 6;
                    tav = atav;
                }
            }

            return ret;
        };
    },
    cylinderToCylinder: function (datalist1, datalist2) {
        var retindexlist = [];
        var pair_list = [];
        var l1i = 0;
        var l2i = 1;
        var firstpair = -1;
        var abs1 = datalist1[0];
        var abs2 = datalist2[0];
        var len1 = datalist1.length - 1;
        var len2 = datalist2.length - 1;

        for (var i = 0; i < datalist1.length; i++) {
            datalist1[i] -= abs1;
            if (datalist1[i] < 0) {
                datalist1[i] += 2 * Math.PI;
            }
        }
        for (var i = 0; i < datalist2.length; i++) {
            datalist2[i] -= abs2;
            if (datalist2[i] < 0) {
                datalist2[i] += 2 * Math.PI;
            }
        }
        if (datalist1.length < datalist2.length) {
            l1i = 1;
            l2i = 0;
            var temp = datalist1;
            datalist1 = datalist2;
            datalist2 = temp;
        }
        for (var i = 0; i < len2; i++) {

            var nexti;
            var nextp;
            if (i < len2 - 1) {
                nexti = i + 1;
                nextp = datalist2[i + 1];
            } else {
                nexti = i + 1;
                nextp = datalist2[0] + (Math.PI * 2);
            }
            var p = (datalist2[i] + nextp) / 2;
            var nearp;
            var nlen = Number.POSITIVE_INFINITY;
            for (var j = 0; j < len1; j++) {
                var pp = datalist1[j];
                var len = Math.abs(pp - p);
                if (len < nlen) {
                    nearp = j;
                    nlen = len;
                }
            }
            if (firstpair === -1)
                firstpair = nearp;
            if (pair_list[nearp] === undefined)
                pair_list[nearp] = i;
            retindexlist.push([l2i, i], [l2i, nexti], [l1i, nearp]);
        }

        for (var i = 0; i < len1; i++) {
            var nexti;
            var validi = i;
            if (i === 0) {
                retindexlist.push([l1i, len1], [l1i, len1 - 1], [l2i, len2]);
                continue;
            } else {
                nexti = i - 1;
            }
            var pair = firstpair;
            for (var j = validi; j < pair_list.length; j++) {
                if (pair_list[j] !== undefined) {
                    pair = j;
                    break;
                }
            }
            var ppair = pair_list[pair];
            retindexlist.push([l1i, validi], [l1i, nexti], [l2i, ppair]);
        }
        return retindexlist;
    },
    cylinderToCylinderCalc: function (retStarEnd, vd1, vd2, c0_p, c1_p, c0_r, c1_r) {
        var cylindervertex = [];
        var cylindertexcoord = [];
        var cylinderindices = [];
        var ang_list1 = [];
        var ang_list2 = [];
        var veci1 = this.vectorFromPointAngle(c0_p, c0_r, [0, -1, 0]);
        var veci2 = this.vectorFromPointAngle(c1_p, c1_r, [0, -1, 0]);
        for (var vi = retStarEnd[0]; vi < retStarEnd[1]; vi += 3) {
            var sp_p = [vd1[vi], vd1[vi + 1], vd1[vi + 2]];
            ang_list1.push(this.vectorvectorAngle(c0_p, veci1, sp_p));
            cylindervertex.push(sp_p[0], sp_p[1], sp_p[2]);
            cylindertexcoord.push((vi - retStarEnd[0]) / (retStarEnd[1] - retStarEnd[0] - 3), 0);
        }


        for (var vi = retStarEnd[2]; vi < retStarEnd[3]; vi += 3) {
            var sp_p = [vd2[vi], vd2[vi + 1], vd2[vi + 2]];

            ang_list2.push(this.vectorvectorAngle(c1_p, veci2, sp_p));
            cylindervertex.push(sp_p[0], sp_p[1], sp_p[2]);
            cylindertexcoord.push((vi - retStarEnd[2]) / (retStarEnd[3] - retStarEnd[2] - 3), 1);
        }

        var calcindices = this.cylinderToCylinder(ang_list1, ang_list2);

        var halfindex = ((vd1.length / 2) / 3) | 0;
        for (var i = 0; i < calcindices.length; i += 3) {
            var c0 = calcindices[i];
            var c1 = calcindices[i + 1];
            var c2 = calcindices[i + 2];
            var i0 = c0[1];
            if (c0[0] === 1)
                i0 += halfindex;
            var i1 = c1[1];
            if (c1[0] === 1)
                i1 += halfindex;
            var i2 = c2[1];
            if (c2[0] === 1)
                i2 += halfindex;

            cylinderindices.push(Math.abs(i1), Math.abs(i0), Math.abs(i2));
        }
        return [cylindervertex, [], cylinderindices, cylindertexcoord];
    },
    circle_circle_intersection: function (c0, c1) {
        var dx = c1.x - c0.x;
        var dy = c1.y - c0.y;
        var d = Math.sqrt((dy * dy) + (dx * dx));
        if (d > (c0.r + c1.r)) {
            return undefined;
        }
        if (d < Math.abs(c0.r - c1.r)) {
            return undefined;
        }
        var a = ((c0.r * c0.r) - (c1.r * c1.r) + (d * d)) / (2 * d);
        var x2 = c0.x + (dx * a / d);
        var y2 = c0.y + (dy * a / d);
        var h = Math.sqrt((c0.r * c0.r) - (a * a));
        var rx = -dy * (h / d);
        var ry = dx * (h / d);
        return {x1: x2 + rx, x2: x2 - rx, y1: y2 + ry, y2: y2 - ry};
    },
    getKarfok: function (r1, r2, ossz, minusz) {
        var ret = this.circle_circle_intersection({x: 0, y: 0, r: r1}, {x: 0, y: ossz, r: r2});
        if (ret !== undefined) {
            var vpos = [ret.x1, ret.y1];
            if (minusz) {
                vpos = [ret.x2, ret.y2];
            }
            return  this.getAngle([0, 0], vpos);
        }
        return 0;
    },
    linePointIntersection: function (l1, l2, p) {
        var v = vec3.sub([0, 0, 0], l2, l1);
        var w = vec3.sub([0, 0, 0], p, l1);
        var c1 = vec3.dot(w, v);
        var c2 = vec3.dot(v, v);
        var b = 0;
        if (c2 !== 0)
            b = c1 / c2;
        var ret = vec3.add(v, l1, vec3.scale(v, v, b));
        ret[3] = b;
        return ret;

    },
    getRelPosFromMat: function (mat, pos) {
        mat = mat4.clone(mat);
        var vo = vec3.transformMat4([0, 0, 0], [0, 0, 0], mat);
        var vx = vec3.transformMat4([0, 0, 0], [1, 0, 0], mat);
        var vy = vec3.transformMat4([0, 0, 0], [0, 1, 0], mat);
        var vz = vec3.transformMat4([0, 0, 0], [0, 0, 1], mat);
        var retx = this.linePointIntersection(vo, vx, pos);
        var rety = this.linePointIntersection(vo, vy, pos);
        var retz = this.linePointIntersection(vo, vz, pos);
        return [retx[3], rety[3], retz[3]];
    },
    calcCsukloPos: function (v0, v1, angle, comb_hossz, vadli_hossz) {
        var mm = mat4.create();
        var ret = [0, 0, 0];
        var hossz1 = vec3.distance(v0, v1);
        var vfok = this.getKarfok(comb_hossz, vadli_hossz, hossz1, true);
        var csuklo_2d = this.rotateToPoint(v0, comb_hossz, vfok);
        csuklo_2d[2] = 0;
        var ujboka = vec3.sub([0, 0, 0], v0, [0, hossz1, 0]);
        var pp = this.linePointIntersection(v0, ujboka, csuklo_2d);
        var linedis = vec3.distance(pp, csuklo_2d);
        var linelength = vec3.distance(pp, v0);
        mat4.translate(mm, mm, v0);
        var retang = this.getAngle3DX(v0, v1);
        mat4.rotateX(mm, mm, retang[1]);
        mat4.rotateZ(mm, mm, retang[0]);
        mat4.rotateY(mm, mm, angle);
        vec3.transformMat4(ret, [0, -linelength, linedis], mm);
        ret[3] = pp[3];
        return ret;
    },
    plaintopointDot: function (v0, v1, p) {
        v0 = vec3.normalize([0, 0, 0], v0);
        v1 = vec3.normalize([0, 0, 0], v1);
        p = vec3.normalize([0, 0, 0], p);
        return [vec3.dot(v0, p), vec3.dot(v1, p)];
    },
    vectorvectorAngle: function (v0, v1, vp) {
        var retang = this.getAngle3DX(v0, v1);
        var mm = mat4.create();
        mat4.rotateX(mm, mm, retang[1]);
        mat4.rotateZ(mm, mm, retang[0]);
        var ret1 = vec3.transformMat4([0, 0, 0], [0, -0, 1], mm);//0 fok
        var ret2 = vec3.transformMat4([0, 0, 0], [1, -0, 0], mm);//90 fok
        var lineret = this.linePointIntersection(v0, v1, vp);
        var nvp = vec3.sub([0, 0, 0], vp, lineret);
        var p2dret = this.plaintopointDot(ret2, ret1, nvp);
        return Math.atan2(p2dret[0], p2dret[1]);
    },
    vectorFromPointAngle: function (v, r, irany) {
        function degToRad(a) {
            return a * Math.PI / 180;
        }
        var mm = mat4.create();
        mat4.translate(mm, mm, v);
        mat4.rotateY(mm, mm, degToRad(r[1]));
        mat4.rotateX(mm, mm, degToRad(r[0]));

        return vec3.transformMat4([0, 0, 0], irany, mm);
    },
    calcVertexNormal: function (vd, vi) {
        var ret = [];
        for (var i = 0; i < vd.length / 3; i++) {
            var crosslist = [];
            for (var j = 0; j < vi.length; j++) {
                if (vi[j] === i) {
                    var ri = (j - (j % 3));
                    var ia = vi[ri] * 3;
                    var ib = vi[ri + 1] * 3;
                    var ic = vi[ri + 2] * 3;
                    var a = [vd[ia++], vd[ia++], vd[ia++]];
                    var c = [vd[ib++], vd[ib++], vd[ib++]];
                    var b = [vd[ic++], vd[ic++], vd[ic++]];

                    var v = vec3.cross([0, 0, 0], vec3.sub(a, a, b), vec3.sub(c, c, b));
                    for (var k = 0; k < crosslist.length; k++) {
                        if (crosslist[k][0] === v[0] && crosslist[k][1] === v[1] && crosslist[k][2] === v[2])
                            continue;
                    }
                    crosslist.push(v);

                }
            }
            var vnorm = [0, 0, 0];
            for (var j = 0; j < crosslist.length; j++) {
                vec3.add(vnorm, vnorm, crosslist[j]);
            }
            vec3.normalize(vnorm, vnorm);
            ret.push(vnorm[0], vnorm[1], vnorm[2]);
        }
        return ret;
    },
    geosSphereData: function (r, vertnum) {
        var latitudeBands = vertnum * 2;
        var longitudeBands = vertnum * 4;
        var vertexPositionData = [];
        var normalData = [];
        var textureCoordData = [];
        for (var latNumber = 0; latNumber <= latitudeBands; latNumber++) {
            var theta = latNumber * Math.PI / latitudeBands;
            var sinTheta = Math.sin(theta);
            var cosTheta = Math.cos(theta);

            for (var longNumber = 0; longNumber <= longitudeBands; longNumber++) {
                var phi = longNumber * 2 * Math.PI / longitudeBands;
                var sinPhi = Math.sin(phi);
                var cosPhi = Math.cos(phi);

                var x = cosPhi * sinTheta;
                var y = cosTheta;
                var z = sinPhi * sinTheta;
                var u = 1 - (longNumber / longitudeBands);
                var v = 1 - (latNumber / latitudeBands);

                normalData.push(x);
                normalData.push(y);
                normalData.push(z);
                textureCoordData.push(u);
                textureCoordData.push(v);
                vertexPositionData.push(r[0] * x);
                vertexPositionData.push(r[1] * y);
                vertexPositionData.push(r[2] * z);
            }
        }
        var indexData = [];
        for (var latNumber = 0; latNumber < latitudeBands; latNumber++) {
            for (var longNumber = 0; longNumber < longitudeBands; longNumber++) {
                var first = (latNumber * (longitudeBands + 1)) + longNumber;
                var second = first + longitudeBands + 1;
                indexData.push(first);
                indexData.push(first + 1);
                indexData.push(second);

                indexData.push(second);
                indexData.push(first + 1);
                indexData.push(second + 1);
            }
        }
        return [vertexPositionData, normalData, indexData, textureCoordData];
    },
    geosBoneData: function (vertnum, bone_list) {
        var vertexPositionData = [];
        var textureCoordData = [];
        var indexData = [];
        var rnum = (Math.PI * 2) / vertnum;
        vertnum++;
        var listnum = bone_list.length;
        for (var il = 0; il < listnum; il++) {
            var litem = bone_list[il];
            var mm = mat4.create();
            mat4.translate(mm, mm, litem.pos);
            litem.rotobj.calcMatix(mm);
            for (var i = 0; i < vertnum; i++) {
                var degress = rnum * i;
                var v = [Math.sin(degress) * litem.radus[0], 0, Math.cos(degress) * litem.radus[1]];
                vec3.transformMat4(v, v, mm);
                var n = [v[0], 0, v[2]];
                vec3.normalize(n, n);
                vertexPositionData.push(v[0], v[1], v[2]);
                textureCoordData.push(i / (vertnum - 1), il / (listnum - 1));
            }
        }
        for (var il = 0; il < listnum - 1; il++) {
            for (var i = 0; i < vertnum; i++) {
                var index = (il * vertnum) + i;
                if (i < vertnum - 1) {
                    indexData.push(index, index + 1, index + vertnum);
                    indexData.push(index + 1, index + vertnum + 1, index + vertnum);
                } else {
                    indexData.push(index, (il * vertnum), index + vertnum);
                    indexData.push((il * vertnum), (il * vertnum) + vertnum, index + vertnum);
                }
            }
        }
        return [vertexPositionData, this.calcVertexNormal(vertexPositionData, indexData), indexData, textureCoordData];
    },
    geosCylinderData: function (start, end, r1, r2, vertnum) {
        var vertexPositionData = [];
        var normalData = [];
        var textureCoordData = [];
        var indexData = [];
        var rnum = (Math.PI * 2) / vertnum;
        vertnum++;

        for (var i = 0; i < vertnum; i++) {
            var degress = rnum * i;
            var v = [Math.sin(degress) * r1[0], start, Math.cos(degress) * r1[1]];

            var n = [v[0], 0, v[2]];
            vec3.normalize(n, n);
            vertexPositionData.push(v[0], v[1], v[2]);

            normalData.push(n[0], n[1], n[2]);

            textureCoordData.push(i / (vertnum - 1), 0);

        }

        for (var i = 0; i < vertnum; i++) {
            var degress = rnum * i;
            var v = [Math.sin(degress) * r2[0], end, Math.cos(degress) * r2[1]];
            var n = [v[0], 0, v[2]];
            vec3.normalize(n, n);
            vertexPositionData.push(v[0], v[1], v[2]);

            normalData.push(n[0], n[1], n[2]);

            textureCoordData.push(i / (vertnum - 1), 1);

        }

        for (var i = 0; i < vertnum - 1; i++) {
            indexData.push(i, i + vertnum, i + 1);
            indexData.push(i + 1, i + vertnum, i + vertnum + 1);
        }
        var i = vertnum - 1;
        indexData.push(i, i + vertnum, 0);
        indexData.push(0, i + vertnum, 0 + vertnum);

        return [vertexPositionData, normalData, indexData, textureCoordData];
    },
    geosBoxData: function (meret) {
        var vertexPositionData = [];
        var normalData = [];
        var textureCoordData = [];
        vertexPositionData.push(0, 0, 0);
        vertexPositionData.push(meret[0], 0, 0);
        vertexPositionData.push(0, 0, meret[2]);
        vertexPositionData.push(meret[0], 0, meret[2]);
        vertexPositionData.push(0, meret[1], 0);
        vertexPositionData.push(meret[0], meret[1], 0);
        vertexPositionData.push(0, meret[1], meret[2]);
        vertexPositionData.push(meret[0], meret[1], meret[2]);
        normalData.push(0, -1, 0);
        normalData.push(0, -1, 0);
        normalData.push(0, -1, 0);
        normalData.push(0, -1, 0);
        normalData.push(0, 1, 0);
        normalData.push(0, 1, 0);
        normalData.push(0, 1, 0);
        normalData.push(0, 1, 0);
        textureCoordData.push(0, 0);
        textureCoordData.push(0, 1);
        textureCoordData.push(1, 0);
        textureCoordData.push(1, 1);
        textureCoordData.push(1, 1);
        textureCoordData.push(0, 1);
        textureCoordData.push(1, 0);
        textureCoordData.push(0, 0);
        var indexData = [];
        indexData.push(0, 1, 2);
        indexData.push(2, 1, 3);
        indexData.push(0, 4, 1);
        indexData.push(1, 4, 5);
        indexData.push(0, 2, 4);
        indexData.push(4, 2, 6);
        indexData.push(4, 6, 5);
        indexData.push(5, 6, 7);
        indexData.push(2, 3, 6);
        indexData.push(6, 3, 7);
        indexData.push(1, 5, 3);
        indexData.push(3, 5, 7);
        return [vertexPositionData, normalData, indexData, textureCoordData];
    },
    geosFenyData: function (meret) {
        var vertexPositionData = [];
        var normalData = [];
        var textureCoordData = [];
        var y_f1 = meret[1] / 3;
        var y_f2 = y_f1 * 2;
        var x_f = meret[0] / 2;
        var z_f = meret[2] / 2;
        vertexPositionData.push(0, y_f1, 0);
        vertexPositionData.push(meret[0], y_f1, 0);
        vertexPositionData.push(0, y_f1, meret[2]);
        vertexPositionData.push(meret[0], y_f1, meret[2]);
        vertexPositionData.push(0, y_f2, 0);
        vertexPositionData.push(meret[0], y_f2, 0);
        vertexPositionData.push(0, y_f2, meret[2]);
        vertexPositionData.push(meret[0], y_f2, meret[2]);

        vertexPositionData.push(x_f, 0, z_f);//8
        vertexPositionData.push(x_f, meret[1], z_f);//9

        normalData.push(-1, -0.5, -1);
        normalData.push(1, -0.5, -1);
        normalData.push(-1, -0.5, 1);
        normalData.push(1, -0.5, 1);
        normalData.push(-1, 0.5, -1);
        normalData.push(1, 0.5, -1);
        normalData.push(-1, 0.5, 1);
        normalData.push(1, 0.5, 1);
        normalData.push(0, -1, 0);
        normalData.push(0, 1, 0);
        textureCoordData.push(0, 0);
        textureCoordData.push(0, 1);
        textureCoordData.push(1, 0);
        textureCoordData.push(1, 1);
        textureCoordData.push(1, 1);
        textureCoordData.push(0, 1);
        textureCoordData.push(1, 0);
        textureCoordData.push(0, 0);
        textureCoordData.push(1, 1);
        textureCoordData.push(0, 0);

        var indexData = [];
        indexData.push(8, 1, 0);
        indexData.push(8, 2, 0);
        indexData.push(8, 1, 3);
        indexData.push(8, 2, 3);

        indexData.push(0, 4, 1);
        indexData.push(1, 4, 5);
        indexData.push(0, 2, 4);
        indexData.push(4, 2, 6);

        indexData.push(2, 3, 6);
        indexData.push(6, 3, 7);
        indexData.push(1, 5, 3);
        indexData.push(3, 5, 7);

        indexData.push(9, 5, 7);
        indexData.push(9, 6, 7);
        indexData.push(9, 5, 4);
        indexData.push(9, 6, 4);

        return [vertexPositionData, normalData, indexData, textureCoordData];
    },
    interpolMatrix: function (mStart, mEnd, interpol, outm) {
        var q = quat.create();
        var qStart = quat.create();
        var qEnd = quat.create();
        var p = vec3.create();
        var s = vec3.create();

        vec3.lerp(p, [mStart[3], mStart[7], mStart[11]], [mEnd[3], mEnd[7], mEnd[11]], interpol);
        quat.slerp(q, quat.fromMat4(qStart, mStart), quat.fromMat4(qEnd, mEnd), interpol);

        vec3.lerp(s, [
            vec3.length([mStart[0], mStart[4], mStart[8]]),
            vec3.length([mStart[1], mStart[5], mStart[9]]),
            vec3.length([mStart[2], mStart[6], mStart[10]])
        ], [
            vec3.length([mEnd[0], mEnd[4], mEnd[8]]),
            vec3.length([mEnd[1], mEnd[5], mEnd[9]]),
            vec3.length([mEnd[2], mEnd[6], mEnd[10]])
        ], interpol);


        var m = outm;

        mat4.fromQuat(m, q);
        m[0] *= s[0];
        m[4] *= s[0];
        m[8] *= s[0];
        m[1] *= s[1];
        m[5] *= s[1];
        m[9] *= s[1];
        m[2] *= s[2];
        m[6] *= s[2];
        m[10] *= s[2];

        m[3] = p[0];
        m[7] = p[1];
        m[11] = p[2];
    }
};


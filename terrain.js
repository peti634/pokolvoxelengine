/* global mat4 */

PokolVoxelEngine.prototype.terrainFormer = function (BlockSize, boxsize) {
    return new PokolVoxelEngine_terrainFormer(this, BlockSize, boxsize);
};

function PokolVoxelEngine_terrainFormer(core, BlockSize, boxsize) {
    var gl = core.ogl;
    var shaderObj;
    this.Korpontok;

    this.VertexBuf = gl.createBuffer();
    this.HeightBuf = gl.createBuffer();
    this.IndexBuf = gl.createBuffer();

    this.SzerkTipus;
    this.Range;
    this.blocksize = BlockSize;

    this.Height = [];
    this.RealPos = [];
    this.boxsize = boxsize;

    this.oldx = 0;
    this.oldy = 0;
    this.oldmin = 0;
    this.oldmax = 0;
    this.oldaRange = 0;


    this.GenerateKijelolPoint = function (range, szerktipus) {//midpoint circle alg
        var ret = [];
        if (szerktipus === 1) {
            for (var i = -range; i <= range; i++)
                ret.push({x: i, yk: -range, yv: range});
        } else {
            var szelpont = [];
            var f = 1 - range;
            var tolx = 0;
            var toly = -2 * range;
            var x = 0;
            var y = range;
            while (x < y) {
                if (f >= 0) {
                    y--;
                    toly += 2;
                    f += toly;
                }
                x++;
                tolx += 2;
                f += tolx + 1;
                szelpont.push({x: x, y: y});
                szelpont.push({x: y, y: x});
            }
            szelpont.push({x: 0, y: range});
            for (var i = 0; i < szelpont.length; i++) {
                var aktpoint = szelpont[i];
                for (var j = i + 1; j < szelpont.length; j++) {
                    var vizsgpoint = szelpont[j];
                    if (aktpoint.y <= vizsgpoint.y && aktpoint.x === vizsgpoint.x) {
                        szelpont.splice(j, 1);
                        j--;
                    }
                }
            }


            for (var i = 0; i < szelpont.length; i++) {
                var aktpoint = szelpont[i];
                ret.push({x: aktpoint.x, yk: -aktpoint.y, yv: aktpoint.y});
                if (aktpoint.x !== 0)
                    ret.push({x: -aktpoint.x, yk: -aktpoint.y, yv: aktpoint.y});
            }
        }

        return ret;
    };

    this.RealPos.Keres = function (x, y) {//visszaaja x-y alapján hogy elérhető e a pont
        if (!this[x])
            return false;
        if (!this[x][y])
            return false;
        return !this[x][y].nan;

    };
    
    this.setPos = function (x, y) {
        this.oldx = x;
        this.oldy = y;
    };
    this.Draw = function (kj, blockkozep) {
        core.shader.shaderUse(shaderObj, "szerk");
        gl.mvPush();
        core.shader.shaderAtrribute("aTerrainHeight", this.HeightBuf);
        core.shader.shaderAtrribute("aVertexPosition", this.VertexBuf);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexBuf);
        if (blockkozep)
            mat4.translate(gl.mvMatrix, gl.mvMatrix, [this.blocksize / 2, 0, this.blocksize / 2]);

        mat4.translate(gl.mvMatrix, gl.mvMatrix, [kj.x * this.blocksize, 0, kj.y * this.blocksize]);
        gl.upload_mvMatrix();
        gl.drawElements(gl.TRIANGLES, this.IndexBufSize, gl.UNSIGNED_SHORT, 0);
        gl.mvPop();
    };
    this.UpdateHeight = function (vx, vy, Terrain, Frissit, HaszNaN, Erzekeny, blockkozep) {
        var min = Number.POSITIVE_INFINITY;
        var max = Number.NEGATIVE_INFINITY;
        var fomagas = Terrain.GetHeight(vx, vy);
        for (var i = 0; i <= this.Range << 1; i++) {
            for (var j = 0; j <= this.RealPos[i].yl; j++) {
                var aktpos = this.RealPos[i][j];


                var magas = Terrain.GetHeight(aktpos.x + vx, aktpos.y + vy);
                if (Frissit)
                    aktpos.nan = Math.abs(fomagas - magas) > Erzekeny;//ha nem nyomjuk a gombot, akkor folyamatos frissítés
                if (!HaszNaN)
                    aktpos.nan = false;//ha nem használjuk a közeli pontokat, akkor mindig hamis a NaN
                if (aktpos.nan)
                    magas = NaN;
                if (aktpos.Index !== undefined) {
                    if (blockkozep) {
                        var vmagas = magas;
                        vmagas = Math.max(vmagas, Terrain.GetHeight(aktpos.x + vx + 1, aktpos.y + vy));
                        vmagas = Math.max(vmagas, Terrain.GetHeight(aktpos.x + vx, aktpos.y + vy + 1));
                        vmagas = Math.max(vmagas, Terrain.GetHeight(aktpos.x + vx + 1, aktpos.y + vy + 1));


                        for (var k = 0; k < 4; k++) {

                            this.Height[(aktpos.Index * 8) + k] = magas;
                        }
                        this.Height[(aktpos.Index * 8) + 4] = vmagas;
                        this.Height[(aktpos.Index * 8) + 5] = vmagas;
                        this.Height[(aktpos.Index * 8) + 6] = vmagas;
                        this.Height[(aktpos.Index * 8) + 7] = vmagas;
                        if (vmagas > max)
                            max = vmagas;
                    } else {

                        for (var k = 0; k < 8; k++) {

                            this.Height[(aktpos.Index * 8) + k] = magas;
                        }
                    }

                    if (min > magas)
                        min = magas;
                    if (magas > max)
                        max = magas;
                }
            }
        }


        gl.bindBuffer(gl.ARRAY_BUFFER, this.HeightBuf);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.Height), gl.STATIC_DRAW);

    };

    this.UpdateData = function (Range, szerktipus) {
        if (Range === this.Range && szerktipus === this.SzerkTipus)
            return;
        var VertexData = [];
        var cubeVertexIndices = [];
        this.Range = Range;
        this.SzerkTipus = szerktipus;
        this.RealPos = [];
        this.Height = [];
        this.Korpontok = this.GenerateKijelolPoint(Range, szerktipus);
        var aktindex = 0;
        var IndexSzamol = 0;
        /*
         RealPos-ba X, ebben Y tömbben helyezkednek el az elemek, mindig 0-n-ig (nincs assoc tömb)
         Az x 0 -> Range*2-ig megy
         Az y 0 -> y vége - y kezd, így a végéig el megyünk
         yl az y vége
         Az x-y megadásával azonnal lekérhető az adott pont, esetleg az eltolási értéket kell csak kivonni
         */

        this.Korpontok.sort(function (a, b) {
            return a.x - b.x;
        });
        for (var i = 0; i < this.Korpontok.length; i++) {
            var aktpont = this.Korpontok[i];
            var vi = aktpont.x + Range;
            this.RealPos[vi] = [];
            this.RealPos[vi].yk = aktpont.yk;//ezzel az eltolással fogjuk vizsgálni a tömböt
            this.RealPos[vi].yl = aktpont.yv - aktpont.yk;

            for (var j = 0; j <= aktpont.yv - aktpont.yk; j++) {
                var x = aktpont.x;
                var y = j + aktpont.yk;

                this.RealPos[vi][j] = {x: x, y: y, nan: false};

                //túl sok index kikerülése
                if (this.Korpontok.length > 120 && i !== this.Korpontok.length - 1 && i !== 0) {//ha nagyobb mint 120 a kör, és nem a szélén van
                    if (szerktipus === 1 && j > 0 && j < aktpont.yv - aktpont.yk)
                        continue;
                    if (szerktipus === 0) {//kör szerkesztés
                        if (y < 0) {
                            if (y > this.Korpontok[i + ((aktpont.x > 0) ? +1 : -1)].yk)
                                continue;
                        } else {
                            if (y < this.Korpontok[i + ((aktpont.x > 0) ? +1 : -1)].yv)
                                continue;
                        }
                    }
                } else if (this.Korpontok.length > 80 && this.Korpontok.length <= 120)//kevesebbet állítunk be
                    if ((x & 1) === 1 || (y & 1) === 1)
                        continue;


                //ez a változó fogja megmutatni, hogy hova mutat a Height tömbe
                //csak akkor kell létrehozni, hogy ha biztosan hozzáadjuk, ha túl sok a korpont, 
                //akkor ez valamikor kimarad, így nem eggyezik a méret, ezért kell felírni az indexet
                this.RealPos[vi][j].Index = IndexSzamol;
                IndexSzamol++;

                x *= BlockSize;
                y *= BlockSize;

                VertexData.push(x - boxsize, -boxsize, y - boxsize);
                VertexData.push(x + boxsize, -boxsize, y - boxsize);
                VertexData.push(x - boxsize, -boxsize, y + boxsize);
                VertexData.push(x + boxsize, -boxsize, y + boxsize);
                VertexData.push(x - boxsize, boxsize, y - boxsize);//4
                VertexData.push(x - boxsize, boxsize, y + boxsize);
                VertexData.push(x + boxsize, boxsize, y - boxsize);//6
                VertexData.push(x + boxsize, boxsize, y + boxsize);

                this.Height.push(0, 0, 0, 0, 0, 0, 0, 0);

                cubeVertexIndices.push(aktindex + 0, aktindex + 1, aktindex + 2);
                cubeVertexIndices.push(aktindex + 3, aktindex + 1, aktindex + 2);

                cubeVertexIndices.push(aktindex + 1, aktindex + 3, aktindex + 6);
                cubeVertexIndices.push(aktindex + 3, aktindex + 6, aktindex + 7);

                cubeVertexIndices.push(aktindex + 0, aktindex + 1, aktindex + 4);
                cubeVertexIndices.push(aktindex + 6, aktindex + 1, aktindex + 4);

                cubeVertexIndices.push(aktindex + 2, aktindex + 5, aktindex + 3);
                cubeVertexIndices.push(aktindex + 7, aktindex + 5, aktindex + 3);

                cubeVertexIndices.push(aktindex + 0, aktindex + 4, aktindex + 2);
                cubeVertexIndices.push(aktindex + 5, aktindex + 4, aktindex + 2);

                cubeVertexIndices.push(aktindex + 5, aktindex + 4, aktindex + 6);
                cubeVertexIndices.push(aktindex + 5, aktindex + 6, aktindex + 7);
                aktindex += 8;

            }
        }


        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexBuf);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(VertexData), gl.STATIC_DRAW);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexBuf);
        this.IndexBufSize = cubeVertexIndices.length;
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.DYNAMIC_DRAW);

    };

    this.FormingTerrain = function (value, szerkpos, viszonypY, tipus, terrainobj) {
        value *= 0.01;
        var korp = this.RealPos;
        var range = this.Range;
        for (var i = 0; i <= range << 1; i++) {
            for (var j = 0; j <= korp[i].yl; j++) {
                var aktpont = korp[i][j];


                if (aktpont.nan)
                    continue;
                var vx = aktpont.x + szerkpos.x;
                var vy = aktpont.y + szerkpos.y;

                var tavolsag = 0;//távolság, nem lehet több mint a Range
                if (this.SzerkTipus === 0)
                    tavolsag = Math.min(Math.sqrt((aktpont.y * aktpont.y) + (aktpont.x * aktpont.x)), range);
                else
                    tavolsag = Math.min(Math.max(Math.abs(aktpont.x), Math.abs(aktpont.y)), range);
                switch (tipus) {
                    case 1:
                        terrainobj.SetHeight(vx, vy, terrainobj.GetHeight(vx, vy) - value);
                        break;
                    case 2:
                        if (range < 1)
                            break;
                        var vvalue = Math.cos(core.math.degToRad(tavolsag / (range) * 90)) * value;
                        terrainobj.SetHeight(vx, vy, terrainobj.GetHeight(vx, vy) - vvalue);
                        break;
                    case 3:
                        var vvalue = Math.abs(value);
                        if (terrainobj.GetHeight(vx, vy) > viszonypY + vvalue)
                            terrainobj.SetHeight(vx, vy, terrainobj.GetHeight(vx, vy) - vvalue);
                        else if (terrainobj.GetHeight(vx, vy) < viszonypY - vvalue)
                            terrainobj.SetHeight(vx, vy, terrainobj.GetHeight(vx, vy) + vvalue);
                        else
                            terrainobj.SetHeight(vx, vy, viszonypY);
                        break;
                    case 4:
                        if (range === 0)
                            break;
                        var vvalue = Math.abs(value) * (range - tavolsag) / range;
                        vvalue *= ((((Math.random() * 2) >> 0) === 1) ? (1) : (-1));
                        terrainobj.SetHeight(vx, vy, terrainobj.GetHeight(vx, vy) + vvalue);
                        break;
                    case 6:
                    case 5:
                        value = Math.min(Math.abs(value), 1);
                        var vvalue = 0, db = 0;
                        var vizsg = [[-1, -1], [-1, 0], [-1, +1], [+1, -1], [+1, 0], [+1, +1], [0, -1], [0, +1], [0, 0]];
                        for (var k = 0; k < vizsg.length; k++) {
                            var elem = vizsg[k];
                            if (isNaN(terrainobj.GetHeight(vx + elem[0], vy + elem[1])))
                                continue;
                            //ha határon belüli, de a pont nem elérhető, továbbmegyünk
                            //a pontból kivonjuk az eltolást (hogy 0 és n között legyen), x-nél Range feljebbtolás, 
                            //y-nál a kezdő értéket kivonás, mivel az minusz érték, hozzáadódik
                            if (tipus === 6 && !korp.Keres(aktpont.x + elem[0] + range, aktpont.y + elem[1] - korp[i].yk))
                                continue;
                            db++;
                            vvalue += terrainobj.GetHeight(vx + elem[0], vy + elem[1]);
                        }
                        vvalue = vvalue / Math.max(db, 1) - terrainobj.GetHeight(vx, vy);
                        terrainobj.SetHeight(vx, vy, terrainobj.GetHeight(vx, vy) + vvalue * value);//maxmimum 10 lehet a value
                        break;
                }
            }
        }
    };
    this.Paint = function (szerkpos, tipus, terrainobj) {
        if (tipus === -1)
            return;
        var korp = this.RealPos;
        var range = this.Range;
        for (var i = 0; i <= range << 1; i++) {
            for (var j = 0; j <= korp[i].yl; j++) {
                var aktpont = korp[i][j];


                if (aktpont.nan)
                    continue;
                var vx = aktpont.x + szerkpos.x;
                var vy = aktpont.y + szerkpos.y;

                terrainobj.SetTipus(vx, vy, tipus);

            }
        }
    };
    var loadShader = function () {
        var fragment =
                "void main(void) { \n" +
                "   gl_FragColor = vec4(1.0); \n" +
                "}";

        var vertex =
                "attribute vec3 aVertexPosition; \n" +
                "attribute float aTerrainHeight; \n" +
                "uniform mat4 uMVMatrix; \n" +
                "uniform mat4 uCMatrix; \n" +
                "uniform mat4 uPMatrix; \n" +
                "void main(void) { \n" +
                "gl_Position = uPMatrix * (uCMatrix * uMVMatrix) * vec4(aVertexPosition+vec3(0.0,aTerrainHeight,0.0), 1.0); \n" +
                "}";

        shaderObj = core.shader.loadShader(fragment, vertex, "szerk", []);
    };
    loadShader();
}

var TERRAIN_RAYPICK_TREENUM = 4;

function PokolVoxelEngine_Terrain(core, blocksize, blockdarab) {
    var gl = core.ogl;

    this.treeMaxHeight = new Float32Array(TERRAIN_RAYPICK_TREENUM * TERRAIN_RAYPICK_TREENUM);
    this.treeMinHeight = new Float32Array(TERRAIN_RAYPICK_TREENUM * TERRAIN_RAYPICK_TREENUM);
    this.Normal = new Float32Array((blockdarab + 1) * (blockdarab + 1));

    this.VertexBuf = gl.createBuffer();
    this.IndexBuf = gl.createBuffer();
    this.HeightBuff = gl.createBuffer();
    this.NormalBuff = gl.createBuffer();

    this.needheightupd;
    this.heightmax;
    this.heightmin;
    this.voxelupd;

    this.Tipus = new Uint16Array((blockdarab + 1) * (blockdarab + 1));
    this.TipusBuff = gl.createBuffer();
    this.needtipusupd;


    var CalcNormal = function (vec1, vec2, vec3) {
        var u = {x: vec2[0] - vec1[0], y: vec2[1] - vec1[1], z: vec2[2] - vec1[2]};
        var v = {x: vec3[0] - vec1[0], y: vec3[1] - vec1[1], z: vec3[2] - vec1[2]};
        var x = u.y * v.z - u.z * v.y;
        var y = u.z * v.x - u.x * v.z;
        var z = u.x * v.y - u.y * v.x;
        return Math.abs(y / Math.sqrt(x * x + y * y + z * z));
    };

    var CalcFeny = function (h, h1, h2, h3, h4) {
        var norm = 0, oszt = 0;
        if (!isNaN(h1) && !isNaN(h2)) {
            norm += CalcNormal([1, h1, 0], [0, h, 0], [0, h2, +1]);
            oszt++;
        }
        if (!isNaN(h1) && !isNaN(h4)) {
            norm += CalcNormal([1, h1, 0], [0, h, 0], [0, h4, -1]);
            oszt++;
        }
        if (!isNaN(h3) && !isNaN(h2)) {
            norm += CalcNormal([-1, h3, 0], [0, h, 0], [0, h2, +1]);
            oszt++;
        }
        if (!isNaN(h3) && !isNaN(h4)) {
            norm += CalcNormal([-1, h3, 0], [0, h, 0], [0, h4, -1]);
            oszt++;
        }
        norm = 1 - (norm / oszt);
        return Math.max(0.6, 1 - norm * 2);
    };

    this.updateTreeHeight = function () {
        var arany = ((this.blockdarab + 1) / TERRAIN_RAYPICK_TREENUM);
        this.tesztHeight = function (index, xpos, ypos) {
            this.treeMaxHeight[index] = Number.NEGATIVE_INFINITY;
            this.treeMinHeight[index] = Number.POSITIVE_INFINITY;
            var tullogas = TERRAIN_RAYPICK_TREENUM - 1;
            for (var i = 0; i <= arany + tullogas; i++) {
                for (var j = 0; j <= arany + tullogas; j++) {
                    var bx = (xpos * arany + i) | 0;
                    var by = (ypos * arany + j) | 0;
                    var e = this.GetHeight(bx, by);
                    if (this.treeMaxHeight[index] < e)
                        this.treeMaxHeight[index] = e;
                    if (this.treeMinHeight[index] > e)
                        this.treeMinHeight[index] = e;
                }
            }

        };
        for (var i = 0; i < this.treeMaxHeight.length; i++) {
            this.tesztHeight(i, i % TERRAIN_RAYPICK_TREENUM, (i / TERRAIN_RAYPICK_TREENUM) | 0);
            if (i === 0) {
                this.heightmax = this.treeMaxHeight[0];
                this.heightmin = this.treeMinHeight[0];
            } else {
                if (this.heightmax < this.treeMaxHeight[i])
                    this.heightmax = this.treeMaxHeight[i];
                if (this.heightmin > this.treeMinHeight[i])
                    this.heightmin = this.treeMinHeight[i];
            }
        }

    };
    this.DrawRaypickTree = function () {



        if (this.DrawTreeHeight_buffer === undefined)
            this.DrawTreeHeight_buffer = gl.createBuffer();
        var arany = ((this.blockdarab + 1) / TERRAIN_RAYPICK_TREENUM);
        var x = arany * this.blocksize;
        var z = arany * this.blocksize;
        var VertexData1 = [];
        VertexData1.push(0, 0, 0);
        VertexData1.push(x, 0, 0);
        VertexData1.push(0, 0, z);
        VertexData1.push(x, 0, 0);
        VertexData1.push(0, 0, z);
        VertexData1.push(x, 0, z);

        VertexData1.push(x, 1, 0);
        VertexData1.push(0, 1, z);
        VertexData1.push(x, 1, z);
        VertexData1.push(x, 1, 0);
        VertexData1.push(0, 1, z);
        VertexData1.push(0, 1, 0);


        gl.bindBuffer(gl.ARRAY_BUFFER, this.DrawTreeHeight_buffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(VertexData1), gl.STATIC_DRAW);

        core.shader.shaderAtrribute("aVertexPosition", this.DrawTreeHeight_buffer);
        for (var i = 0; i < this.treeMaxHeight.length; i++) {
            var ossz = Math.abs(this.treeMaxHeight[i] - this.treeMinHeight[i]) + 0.02;//picit több és kevesebb (nem olvadjon bele)
            core.shader.shaderUniform("uHeight", [ossz]);
            gl.mvPush();
            var xpos = (i % TERRAIN_RAYPICK_TREENUM) * arany * this.blocksize;
            var zpos = ((i / TERRAIN_RAYPICK_TREENUM) | 0) * arany * this.blocksize;
            mat4.translate(gl.mvMatrix, gl.mvMatrix, [xpos, this.treeMinHeight[i] - 0.01, zpos]);
            gl.upload_mvMatrix();

            gl.drawArrays(gl.TRIANGLES, 0, VertexData1.length / 3);


            gl.mvPop();
        }
    };

    this.NormalIndexFrissit = function (x, y) {
        if (x < 0 || y < 0 || x > blockdarab || y > blockdarab)
            return;
        this.Normal[x + (y * (this.blockdarab + 1))] = 0;
    };
    this.GetHeight = function (x, y) {
        if (x < 0 || y < 0 || x > blockdarab || y > blockdarab)
            return undefined;
        return  this.VertexHeight[x + (y * (this.blockdarab + 1))];
    };
    this.GetNormal = function (x, y) {
        if (x < 0 || y < 0 || x > blockdarab || y > blockdarab)
            return undefined;
        return  this.Normal[x + (y * (this.blockdarab + 1))];
    };
    this.GetTipus = function (x, y) {
        if (x < 0 || y < 0 || x > blockdarab || y > blockdarab)
            return undefined;
        return  this.Tipus[x + (y * (this.blockdarab + 1))];
    };
    this.SetHeight = function (x, y, e) {
        if (x < 0 || y < 0 || x > blockdarab || y > blockdarab)
            return;
        this.voxelupd = true;
        var index = x + (y * (this.blockdarab + 1));
        this.VertexHeight[index] = e;
        this.Normal[index] = 0;
        this.NormalIndexFrissit(x + 1, y);
        this.NormalIndexFrissit(x, y + 1);
        this.NormalIndexFrissit(x - 1, y);
        this.NormalIndexFrissit(x, y - 1);
        this.needheightupd = true;
    };
    this.SetTipus = function (x, y, e) {
        if (x < 0 || y < 0 || x > blockdarab || y > blockdarab)
            return;
        this.voxelupd = true;
        var index = x + (y * (this.blockdarab + 1));
        this.Tipus[index] = e;

        this.needtipusupd = true;
    };
    this.Draw = function () {
        core.shader.shaderAtrribute("aVertexPosition", this.VertexBuf);
        core.shader.shaderAtrribute("aNormal", this.NormalBuff);
        core.shader.shaderAtrribute("aTerrainHeight", this.HeightBuff);
        core.shader.shaderAtrribute("aTipus", this.TipusBuff);


        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexBuf);
        gl.upload_mvMatrix();
        gl.drawElements(gl.TRIANGLES, this.IndexBufSize, gl.UNSIGNED_SHORT, 0);



    };
    this.DrawVonal = function () {
        core.shader.shaderAtrribute("aVertexPosition", this.VertexBuf);
        core.shader.shaderAtrribute("aTerrainHeight", this.HeightBuff);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexBuf);
        mat4.translate(gl.mvMatrix, gl.mvMatrix, [0, 0.005, 0]);
        gl.upload_mvMatrix();
        gl.drawElements(gl.LINES, this.IndexBufSize, gl.UNSIGNED_SHORT, 0);

    };
    this.LoadFromImage = function (img) {
        var canv = document.createElement("CANVAS");
        canv.width = img.width;
        canv.height = img.height;
        var cont = canv.getContext("2d");
        var vheight = (this.blockdarab > img.height) ? img.height : this.blockdarab;
        var vwidth = (this.blockdarab > img.width) ? img.width : this.blockdarab;

        cont.drawImage(img, 0, 0);
        var pixelArr = cont.getImageData(0, 0, vwidth, vheight).data;
        var pixelarr_index = 0;
        for (var i = 0; i < vwidth; i++) {
            for (var j = 0; j < vheight; j++) {

                var r = pixelArr[pixelarr_index];
                var g = pixelArr[pixelarr_index + 1];
                var b = pixelArr[pixelarr_index + 2];
                var h = ((r - 127) * 255) + g + (b / 255);
                this.SetHeight(j, i, h);
                pixelarr_index += 4;
            }
        }
        this.UpdateHeight();
    };

    this.UpdateNormal = function () {
        for (var i = 0; i < this.blockdarab + 1; i++) {

            for (var j = 0; j < this.blockdarab + 1; j++) {
                var index = j + (i * (this.blockdarab + 1));
                if (this.Normal[index] !== 0)
                    continue;
                var h1 = this.GetHeight(j + 1, i);
                var h2 = this.GetHeight(j, i + 1);
                var h3 = this.GetHeight(j - 1, i);
                var h4 = this.GetHeight(j, i - 1);
                this.Normal[index] = CalcFeny(this.VertexHeight[index], h1, h2, h3, h4);
            }
        }
    };
    this.UpdateTipus = function () {
        if (!this.needtipusupd)
            return false;

        var TipusData = new Float32Array(this.blockdarab * this.blockdarab * 4);
        

        var index = 0;
        for (var i = 0; i < this.blockdarab; i++) {
            for (var j = 0; j < this.blockdarab; j++) {

                TipusData[index++] = this.GetTipus(i, j);
                TipusData[index++] = this.GetTipus(i, j);

                TipusData[index++] = this.GetTipus(i, j);
                TipusData[index++] = this.GetTipus(i, j);
            }

        }

        gl.bindBuffer(gl.ARRAY_BUFFER, this.TipusBuff);
        gl.bufferData(gl.ARRAY_BUFFER, TipusData, gl.STATIC_DRAW);
        this.needtipusupd = false;
        return true;

    };
    this.UpdateHeight = function () {
        if (!this.needheightupd)
            return false;


        var VertexHe = new Float32Array(this.blockdarab * this.blockdarab * 4);

        var index = 0;
        for (var i = 0; i < this.blockdarab; i++) {
            for (var j = 0; j < this.blockdarab; j++) {

                VertexHe[index++] = this.GetHeight(i, j);
                VertexHe[index++] = this.GetHeight(i + 1, j);

                VertexHe[index++] = this.GetHeight(i, j + 1);
                VertexHe[index++] = this.GetHeight(i + 1, j + 1);
            }

        }

        gl.bindBuffer(gl.ARRAY_BUFFER, this.HeightBuff);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(VertexHe), gl.STATIC_DRAW);
        this.updateTreeHeight();

        this.UpdateNormal();

        var VertexNormal = new Float32Array(this.blockdarab * this.blockdarab * 4);

        var index = 0;
        for (var i = 0; i < this.blockdarab; i++) {
            for (var j = 0; j < this.blockdarab; j++) {

                VertexNormal[index++] = this.GetNormal(i, j);
                VertexNormal[index++] = this.GetNormal(i + 1, j);

                VertexNormal[index++] = this.GetNormal(i, j + 1);
                VertexNormal[index++] = this.GetNormal(i + 1, j + 1);
            }

        }
        gl.bindBuffer(gl.ARRAY_BUFFER, this.NormalBuff);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(VertexNormal), gl.STATIC_DRAW);


        this.needheightupd = false;
        return true;
    };
    this.Update = function () {
        this.needheightupd = true;
        this.heightmax = 0;
        this.heightmin = 0;
        this.voxelupd = true;
        this.needtipusupd = true;
        //index, x-z
        this.needheightupd = true;
        this.Normal = new Float32Array((this.blockdarab + 1) * (this.blockdarab + 1));
        this.VertexData = new Float32Array(this.blockdarab * this.blockdarab * 4 * 2);

        var index = 0;
        for (var i = 0; i < this.blockdarab; i++) {
            for (var j = 0; j < this.blockdarab; j++) {

                this.VertexData[index++] = i * this.blocksize;
                this.VertexData[index++] = j * this.blocksize;

                this.VertexData[index++] = (i + 1) * this.blocksize;
                this.VertexData[index++] = j * this.blocksize;

                this.VertexData[index++] = i * this.blocksize;
                this.VertexData[index++] = (j + 1) * this.blocksize;

                this.VertexData[index++] = (i + 1) * this.blocksize;
                this.VertexData[index++] = (j + 1) * this.blocksize;
            }

        }

        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexBuf);
        gl.bufferData(gl.ARRAY_BUFFER, this.VertexData, gl.STATIC_DRAW);

        var index = 0;
        var iindex = 0;
        var VertexIndex = new Uint16Array(this.blockdarab * this.blockdarab * 6);
        for (var i = 0; i < this.blockdarab; i++)
            for (var j = 0; j < this.blockdarab; j++) {
                VertexIndex[index++] = iindex + 0;
                VertexIndex[index++] = iindex + 1;
                VertexIndex[index++] = iindex + 2;
                VertexIndex[index++] = iindex + 3;
                VertexIndex[index++] = iindex + 1;
                VertexIndex[index++] = iindex + 2;
                iindex += 4;
            }

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexBuf);
        this.IndexBufSize = VertexIndex.length;
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(VertexIndex), gl.DYNAMIC_DRAW);

        this.UpdateHeight();
        this.UpdateTipus();

    };
    this.blocksize = blocksize;
    this.blockdarab = blockdarab;

    this.VertexHeight = new Float32Array((blockdarab + 1) * (blockdarab + 1));

    this.Update();
    this.getRayPick = function (campos, camAngle, blockkozep) {

        var raypick = new core.math.rayPickGeometry(campos, camAngle);
        var treeblock = new Array(this.treeMaxHeight.length);

        var arany = ((this.blockdarab + 1) / TERRAIN_RAYPICK_TREENUM);
        var treemeret = arany * blocksize;
        for (var i = 0; i < this.treeMaxHeight.length; i++) {
            var elem = {};

            var x = (i % TERRAIN_RAYPICK_TREENUM);
            var z = (i / TERRAIN_RAYPICK_TREENUM) | 0;
            var blockmx = arany;
            var blockmz = arany;

            elem.merx = blockmx * blocksize;
            elem.mery = this.treeMaxHeight[i] - this.treeMinHeight[i];
            elem.merz = blockmz * blocksize;

            elem.posx = x * treemeret;
            elem.posy = this.treeMinHeight[i];
            elem.posz = z * treemeret;


            elem.id_x = x * arany;
            elem.id_xm = blockmx;
            elem.id_z = z * arany;
            elem.id_zm = blockmz;

            var kozepx = elem.posx + (elem.merx) / 2;
            var kozepz = elem.posz + (elem.merz) / 2;

            kozepx -= campos[0];
            kozepz -= campos[2];

            kozepx = kozepx * kozepx;
            kozepz = kozepz * kozepz;

            elem.tav = Math.sqrt(kozepx + kozepz);

            treeblock[i] = elem;

        }

        treeblock.sort(function (a, b) {
            return a.tav - b.tav;
        });
        var legpos = undefined;

        for (var k = 0; k < treeblock.length; k++) {
            var elem = treeblock[k];
            if (raypick.box([elem.posx, elem.posy, elem.posz], elem.merx, elem.mery, elem.merz)) {

                var legtav = Number.MAX_VALUE;
                legpos = undefined;

                for (var i = (elem.id_z | 0); i < elem.id_z + elem.id_zm && i < (this.blockdarab); i++) {
                    for (var j = (elem.id_x | 0); j < elem.id_x + elem.id_xm && j < (this.blockdarab); j++) {



                        var index1 = j + (i * (this.blockdarab + 1));
                        var index2 = j + 1 + (i * (this.blockdarab + 1));
                        var index3 = j + ((i + 1) * (this.blockdarab + 1));
                        var index4 = j + 1 + ((i + 1) * (this.blockdarab + 1));

                        var e1 = this.VertexHeight[index1];
                        var e2 = this.VertexHeight[index2];
                        var e3 = this.VertexHeight[index3];
                        var e4 = this.VertexHeight[index4];

                        var p1 = [j * this.blocksize, e1, i * this.blocksize];
                        var p2 = [(j + 1) * this.blocksize, e2, i * this.blocksize];
                        var p3 = [j * this.blocksize, e3, (i + 1) * this.blocksize];
                        var p4 = [(j + 1) * this.blocksize, e4, (i + 1) * this.blocksize];

                        var boxpos = raypick.plain_getpos(p1, p2, p3, p4);
                        if (boxpos !== undefined) {
                            var tav = vec3.distance(campos, boxpos);
                            if (tav < legtav) {
                                legtav = tav;
                                legpos = [boxpos[0], boxpos[1], boxpos[2]];
                            }

                        }
                    }
                }




                if (legpos !== undefined)
                    break;


            }
        }

        if (legpos === undefined)
            return undefined;
        //van ütközés, index pont kiszámítása
        if (blockkozep) {
            return [Math.floor(legpos[0] / this.blocksize), Math.floor(legpos[2] / this.blocksize)];

        }
        return [Math.round(legpos[0] / this.blocksize), Math.round(legpos[2] / this.blocksize)];

    };

}
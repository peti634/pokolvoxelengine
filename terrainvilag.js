/*global PokolVoxelEngine_jspath*/
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/terrain.js'></script>");

function PokolVoxelEngine_TerrainVilag(core, settings) {
    var gl = core.ogl;
    var terrainAtlas = core.getTerrainAtlas();
    var shaderObj;
    var shaderObjVonal;
    var relativPos = [0, 0, 0];
    this.tlist = [];//terrain list
    this.tlistrendez = [];
    this.campos = [0, 0, 0];


    this.blockdarab = settings.voxel_terrainLengthXZ;
    this.blocksize = settings.voxel_blokSsize;
    this.indices = [];

    this.setRelativePos = function (rpos) {
        relativPos = rpos;
    };
    this.getRelativePos = function () {
        return relativPos;
    };
    this.AddTerrain = function (x, y) {
        if (this.getTerrain(x, y))
            return;
        //index tömb kell majd
        var elem = new PokolVoxelEngine_Terrain(core, this.blocksize, this.blockdarab);
        elem.x = x;
        elem.y = y;
        elem.UpdateHeight();
        var index = this.tlist.push(elem) - 1;
        this.indices[x + "_" + y] = index;
        this.tlistrendez.push(elem);
        //force
        this.campos = [undefined, undefined, undefined];
        this.setCampos(gl.campos);
    };
    this.Clean = function () {
        this.tlist = [];
        this.tlistrendez = [];
        this.indices = [];

    };
    this.getTerrain = function (x, y) {
        var index = this.indices[x + "_" + y];
        if (index !== undefined) {
            return this.tlist[index];
        }
        return undefined;

    };
    this.getTerrainList = function () {
        var ret = [];
        for (var i = 0; i < this.tlist.length; i++) {
            var elem = this.tlist[i];
            ret.push({x: elem.x, z: elem.y});

        }
        return ret;
    };
    this.setCampos = function (campos) {
        var relativCampos = [campos[0] + relativPos[0], campos[1] + relativPos[1], campos[2] + relativPos[2]];

        if (this.campos[0] === relativCampos[0] && this.campos[1] === relativCampos[1] && this.campos[2] === relativCampos[2])
            return;
        this.campos = [relativCampos[0], relativCampos[1], relativCampos[2]];
        var terrainm = this.blockdarab * this.blocksize;
        for (var i = 0; i < this.tlistrendez.length; i++) {
            var elem = this.tlistrendez[i];
            var posx = (elem.x * terrainm) + (terrainm / 2);
            var posy = (elem.y * terrainm) + (terrainm / 2);
            elem.tav = vec2.distance([campos[0], campos[2]], [posx, posy]);
            var tavx = Math.abs(posx - campos[0]) - (terrainm / 2);
            var tavy = Math.abs(posy - campos[2]) - (terrainm / 2);
            elem.needdraw = (tavx < settings.render_maxrange) && (tavy < settings.render_maxrange);
        }
        this.tlistrendez.sort(function (a, b) {
            return a.tav - b.tav;
        });
    };
    this.getTerrain_Pos = function (x, y, szele) {
        var bx = x / this.blockdarab | 0;
        var by = y / this.blockdarab | 0;
        var px = x % this.blockdarab;
        var py = y % this.blockdarab;

        if (szele) {
            if (px === 0 && bx > 0) {
                bx--;
                px = this.blockdarab;
            }
            if (py === 0 && by > 0) {
                by--;
                py = this.blockdarab;
            }
        }
        var ter = this.getTerrain(bx, by);
        if (ter === undefined)
            return undefined;
        return [ter, px, py];
    };
    this.DrawRaypickTree = function (x, y) {
        if (this.shaderObjRayTree === undefined) {
            var fragment = "void main(void) { \n" +
                    "   gl_FragColor = vec4(1.0); \n" +
                    "}";
            var vertex = "attribute vec3 aVertexPosition; \n" +
                    "uniform mat4 uMVMatrix; \n" +
                    "uniform mat4 uCMatrix; \n" +
                    "uniform mat4 uPMatrix; \n" +
                    "uniform float uHeight; \n" +
                    "void main(void) { \n" +
                    "   gl_Position = uPMatrix * (uCMatrix * uMVMatrix) * vec4(aVertexPosition.x,aVertexPosition.y * uHeight,aVertexPosition.z, 1.0); \n" +
                    "}";

            this.shaderObjRayTree = core.shader.loadShader(fragment, vertex, "terrainvonal", []);
        }
        core.shader.shaderUse(this.shaderObjRayTree, "terrain_raypicktreee");
        var terrainm = this.blockdarab * this.blocksize;
        for (var i = 0; i < this.tlistrendez.length; i++) {
            var elem = this.tlistrendez[i];
            if (elem.x === x && elem.y === y) {
                gl.mvPush();



                mat4.translate(gl.mvMatrix, gl.mvMatrix, [elem.x * terrainm, 0, elem.y * terrainm]);
                gl.upload_mvMatrix();
                elem.DrawRaypickTree();
                gl.mvPop();
                break;
            }
        }


    };
    this.Draw = function (vonaldraw) {

        if (vonaldraw) {
            core.shader.shaderUse(shaderObjVonal, "terrainvonal");
        } else {
            core.shader.shaderUse(shaderObj, "terrain");
            core.texman.update();
            terrainAtlas.activeShader();

            core.shader.shaderUniform("uBitlen", [terrainAtlas.bitlen]);
        }

        this.setCampos(gl.campos);
        var terrainm = this.blockdarab * this.blocksize;
        gl.mvPush();
        mat4.translate(gl.mvMatrix, gl.mvMatrix, relativPos);
        var drawlist = [];

        
        core.shader.shaderUniform("uRange", [settings.render_maxrange]);
        for (var i = 0; i < this.tlistrendez.length; i++) {
            var elem = this.tlistrendez[i];
            if (elem === undefined)
                continue;
            if (elem.needdraw === false) {
                continue;
            }
            drawlist.push({elem: elem, x: elem.x * terrainm, z: elem.y * terrainm});

        }
        

        for (var i = 0; i < drawlist.length; i++) {
            var elem = drawlist[i];
            gl.mvPush();



            mat4.translate(gl.mvMatrix, gl.mvMatrix, [elem.x, 0, elem.z]);
            gl.upload_mvMatrix();

            if (vonaldraw) {
                elem.elem.DrawVonal();
            } else {
                elem.elem.Draw();
            }

            gl.mvPop();
        }
        gl.mvPop();
        if (vonaldraw !== true) {
            terrainAtlas.unActiveShader();
        }
    };
    this.SetHeight = function (x, y, e) {
        var ret = this.getTerrain_Pos(x, y, true);

        if (ret) {
            var bx = ret[0].x;
            var by = ret[0].y;
            ret[0].SetHeight(ret[1], ret[2], e);
            if (ret[1] === this.blockdarab) {
                var ter = this.getTerrain(bx + 1, by);
                if (ter) {
                    ter.SetHeight(0, ret[2], e);
                }
            }
            if (ret[2] === this.blockdarab) {
                var ter = this.getTerrain(bx, by + 1);
                if (ter) {
                    ter.SetHeight(ret[1], 0, e);
                }
            }
            if (ret[1] === this.blockdarab && ret[2] === this.blockdarab) {
                var ter = this.getTerrain(bx + 1, by + 1);
                if (ter) {
                    ter.SetHeight(0, 0, e);
                }
            }
        }

    };
    this.SetTipus = function (x, y, e) {
        var ret = this.getTerrain_Pos(x, y, false);

        if (ret) {
            ret[0].SetTipus(ret[1], ret[2], e);
        }
    };
    this.GetTipus = function (x, y) {
        var ret = this.getTerrain_Pos(x, y, false);
        if (ret) {
            return ret[0].GetTipus(ret[1], ret[2]);
        }
        return undefined;
    };
    this.GetHeight = function (x, y) {
        var ret = this.getTerrain_Pos(x, y, true);
        if (ret) {
            return ret[0].GetHeight(ret[1], ret[2]);
        }
        return undefined;
    };
    this.AddHeight = function (x, y, e) {
        var ret = this.GetHeight(x, y);
        if (ret !== undefined && ret !== NaN) {
            this.SetHeight(x, y, ret + e);
        }
    };
    this.Update = function () {
        var terrainm = this.blockdarab * this.blocksize;
        for (var i = 0; i < this.tlist.length; i++) {
            var elem = this.tlist[i];
            if (elem === undefined)
                continue;
            if (elem.UpdateHeight() || elem.UpdateTipus()) {
                var cullfacepos = [elem.x * terrainm, elem.heightmin, elem.y * terrainm];
                var boxsize = [terrainm + this.blocksize, elem.heightmax - elem.heightmin, terrainm + this.blocksize];
                //ffbuffer.buffer.renderFrameBuffersRange(false, cullfacepos, boxsize);
            }
        }
    };
    this.RayPick = function (campos, blockkozep) {
        this.setCampos(gl.campos);
        var campos = [gl.campos[0] - relativPos[0], gl.campos[1] - relativPos[1], gl.campos[2] - relativPos[2]];


        var camszog = gl.unprojAngel;
        var raypick = new core.math.rayPickGeometry(campos, camszog);

        var terrainm = this.blockdarab * this.blocksize;


        var ret = [-1, 0];
        for (var i = 0; i < this.tlistrendez.length; i++) {
            var elem = this.tlistrendez[i];
            if (elem === undefined)
                continue;
            var cpos = [(elem.x * terrainm), elem.heightmin, (elem.y * terrainm)];

            if (!raypick.box(cpos, terrainm + this.blocksize, elem.heightmax - elem.heightmin, terrainm + this.blocksize))
                continue;
            cpos[0] = campos[0] - cpos[0];
            cpos[1] = campos[1];
            cpos[2] = campos[2] - cpos[2];

            var boxpos = elem.getRayPick(cpos, camszog, blockkozep);

            if (boxpos !== undefined) {
                ret.x = boxpos[0] + (elem.x * this.blockdarab);
                ret.y = boxpos[1] + (elem.y * this.blockdarab);
                return ret;
            }
        }
        return ret;

    };
    this.collectTerrainForVoxelTerrain = function (x, z, voxel_mxz) {
        var retlist = [];
        var kezdx = (((x - 1) * voxel_mxz) / this.blockdarab) | 0;
        var kezdz = (((z - 1) * voxel_mxz) / this.blockdarab) | 0;

        var vegx = (((x + 1) * voxel_mxz) / this.blockdarab) | 0;
        var vegz = (((z + 1) * voxel_mxz) / this.blockdarab) | 0;

        for (var ix = kezdx; ix <= vegx; ix++) {
            for (var iz = kezdz; iz <= vegz; iz++) {
                var ter = this.getTerrain(ix, iz);
                if (ter === undefined || ter.voxelupd === false)
                    continue;
                //ter.voxelupd = false;
                retlist.push({x: ix, z: iz, data: ter.VertexHeight, tipdata: ter.Tipus});
            }
        }
        return retlist;
    };
    this.updateFromTerrain = function (clean) {
        var vlist = core.voxelVilag.getTerrainList();
        for (var i = 0; i < vlist.length; i++) {
            var x = vlist[i].x;
            var z = vlist[i].z;
            var v = core.voxelVilag.getTerrain(x, z);

            if (clean) {
                this.worker_TtoV.postMessage({type: "TtoV_CLEAN", x: x, z: z});
            } else {
                var termxz = this.blockdarab;
                var voxmxz = v.mxz;


                var sendmsg = {type: "TtoV_UPD", tmxz: termxz, bs: this.blocksize, mxz: voxmxz};

                sendmsg.ypos = v.ypos;
                sendmsg.my = v.my;
                sendmsg.blockdata = v.blockdata.buffer;
                sendmsg.terlist = [];

                sendmsg.tpx = x;
                sendmsg.tpz = z;
                sendmsg.terlist = this.collectTerrainForVoxelTerrain(x, z, voxmxz);

                this.worker_TtoV.postMessage(sendmsg);
            }
        }

    };

    this.onmessage = function (data) {
        var voxelt = core.voxelVilag.getTerrain(data.px, data.pz);
        if (voxelt) {
            if (data.type === "TtoV_UPD") {
                if (voxelt.blockdata_terr !== undefined) {
                    throw "Voxel blockdata_terr NO undefined!!";

                }
                voxelt.blockdata_terr = new PokolVoxelEngine_dinamic_3d_array(voxelt.mxz, Uint8Array);

                var upddate = data.upddate;
                for (var i = 0; i < upddate.length; i += 4) {
                    var x = upddate[i + 0];
                    var y = upddate[i + 1];
                    var z = upddate[i + 2];

                    var t = upddate[i + 3];
                    if (t > 0) {

                        voxelt.addElem(x, y, z, t, undefined);
                        voxelt.blockdata_terr.setElem(x, y, z, 1);
                    } else {
                        voxelt.delElem(x, y, z);
                        voxelt.blockdata_terr.setElem(x, y, z, 0);
                    }

                }
                voxelt.update();
            } else if (data.type === "TtoV_CLEAN") {
                if (voxelt.blockdata_terr === undefined) {
                    throw "Voxel blockdata_terr undefined!!";
                }
                for (var x = 0; x < voxelt.mxz; x++) {
                    for (var y = 0; y < voxelt.my; y++) {
                        for (var z = 0; z < voxelt.mxz; z++) {
                            var vy = y + voxelt.ypos;
                            if (voxelt.blockdata_terr.getElem(x, vy, z) === 1) {
                                voxelt.delElem(x, vy, z);
                            }
                        }
                    }
                }
                voxelt.blockdata_terr = undefined;
                voxelt.update();
            }

        } else {
            throw "No voxel terrain in terrainvilag onmessage";
        }

    };

    function genrandstr()
    {
        return "";
        var ret = "";
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
            ret += chars.charAt(Math.floor(Math.random() * chars.length));

        return ret;
    }

    this.worker_TtoV = new Worker(PokolVoxelEngine_jspath + "/worker_terrtovox.js?rnd=" + genrandstr());
    this.worker_TtoV.onmessage = function (e) {
        this.parent.onmessage(e.data);
    };
    this.worker_TtoV.parent = this;


    var loadShader = function () {

        var fragment =
                "varying highp vec2 vTextureCoord; \n" +
                "varying highp vec2 vVertexPos; \n" +
                "varying highp float vFeny; \n" +
                "uniform highp float uRange; \n" +
                "uniform highp float uId; \n" +
                "uniform highp float uBitlen; \n" +
                "varying highp float vTipus; \n" +
                
                core.texman.TexAtlas.getFragmentShader("getTex") + " \n" +
                "void main(void) { \n" +
                "   if (any(greaterThan(abs(vVertexPos), vec2(uRange)))) discard; \n" +
                "   highp float x = mod(float(vTipus),uBitlen); \n" +
                "   highp float y = floor(float(vTipus) / uBitlen); \n" +
                "   highp vec3 texszin = getTex(x, y, vTextureCoord).rgb; \n" +
                "   gl_FragColor = vec4(texszin * vFeny ,1.0); \n" +
                "}";


        var vertex =
                "attribute vec2 aVertexPosition; \n" +
                "attribute float aTerrainHeight; \n" +
                "attribute float aNormal; \n" +
                "attribute float aTipus; \n" +
                "uniform mat4 uMVMatrix; \n" +
                "uniform mat4 uCMatrix; \n" +
                "uniform mat4 uPMatrix; \n" +
                "uniform vec3 uCamPos; \n" +
                "varying highp vec2 vTextureCoord; \n" +
                "varying highp float vFeny; \n" +
                "varying highp vec2 vVertexPos; \n" +
                "varying highp float vTipus; \n" +

                "void main(void) { \n" +
                "   gl_Position = uPMatrix * (uCMatrix * uMVMatrix) * vec4(aVertexPosition.x,aTerrainHeight,aVertexPosition.y, 1.0); \n" +
                "   vTextureCoord = aVertexPosition; \n" +
                "   vVertexPos = aVertexPosition + vec2(uMVMatrix[3][0] - uCamPos.x,uMVMatrix[3][2] - uCamPos.z); \n" +
                "   vFeny = aNormal; \n" +
                "   vTipus = aTipus; \n" +
                "}";

        shaderObj = core.shader.loadShader(fragment, vertex, "terrain", ["HIGHFENY"]);

        var fragment =
                "varying highp float vLen; \n" +
                "void main(void) { \n" +
                "gl_FragColor = vec4(0.0,0.0,0.0,vLen); \n" +
                "}";
        var vertex =
                "attribute vec2 aVertexPosition; \n" +
                "attribute float aTerrainHeight; \n" +
                "uniform vec3 uCamPos; \n" +
                "uniform mat4 uMVMatrix; \n" +
                "uniform mat4 uCMatrix; \n" +
                "uniform mat4 uPMatrix; \n" +
                "varying highp float vLen; \n" +
                "void main(void) { \n" +
                "gl_Position = uPMatrix * (uCMatrix * uMVMatrix) * vec4(aVertexPosition.x,aTerrainHeight,aVertexPosition.y, 1.0); \n" +
                "vLen = 2.0-(distance(vec3(aVertexPosition.x + uMVMatrix[3][0],aTerrainHeight,aVertexPosition.y + uMVMatrix[3][2]),uCamPos)/8.0); \n" +
                "}";
        shaderObjVonal = core.shader.loadShader(fragment, vertex, "terrainvonal", []);
    };
    loadShader();
}

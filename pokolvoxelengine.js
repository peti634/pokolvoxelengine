/*
 * PokolVoxelEngine v0.7 - 2016.04.10
 * PokolStudio - peti634
 */
/*global mat4*/
/*global vec3*/
/*global PokolVoxelEngine_jspath*/
function PokolVoxelEngine_DefaultSettings() {
    this.canvas = undefined;
    this.cam_persFar = 1000;
    this.cam_persNear = 0.1;
    this.render_maxrange = 100;
    this.voxel_pontfenyMaxRange = 10;
    this.voxel_napfenyRange = 0;
    this.voxel_terrainLengthXZ = 10;
    this.voxel_blokSsize = 1;
    this.voxel_blokkOldalFeny = [0.78, 0.7, 0.4, 1.0, 0.6, 0.85];
    this.blokkTypeList = [];
    this.blokkCsoportList = [];
    this.url_prefix = "";
    this.texatlas_fullsize = 4096;
    this.texatlas_texsize = 256;
    this.texatlas_border = 128;
    this.texatlas_lod_size = 128;
    this.callback_draw = undefined;
}

var PokolVoxelEngine_mainpath = document.currentScript.src;
var PokolVoxelEngine_jspath = PokolVoxelEngine_mainpath.substring(0, PokolVoxelEngine_mainpath.lastIndexOf("/"));

PokolVoxelEngine = function () {

};
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/lib/gl-matrix.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/lib/jszip.min.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/math.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/mapfile.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/texman.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/terrainvilag.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/geometry.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/simpgeos.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/control.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/shader.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/voxelvilag.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/animator.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/animatorMesh.js'></script>");
document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/fizika.js'></script>");

document.writeln("<script type='text/javascript' src='" + PokolVoxelEngine_jspath + "/load_collada.js'></script>");

PokolVoxelEngine.prototype.load = {};

PokolVoxelEngine.prototype.initGL = function (settings) {
    var _this = this;
    var ogl;
    try {
        ogl = settings.canvas.getContext("experimental-webgl", {antialias: false});
    } catch (e) {
    }
    if (!ogl) {
        alert("Unable to initialize WebGL. Your browser may not support it.");
    }
    if (ogl.getExtension("OES_element_index_uint") === null) {
        alert("OES_element_index_uint ERROR");
        return;
    }
    if (ogl.getExtension("OES_texture_float") === null) {
        alert("OES_texture_float ERROR");
        return;
    }

    var terrainAtlas;
    var karakterAtlas;
    var currentControl;
    this.napfeny = 1.0;

    this.ogl = ogl;

    ogl.createTextureOriginal = ogl.createTexture;
    ogl.textureNum = 0;
    ogl.createTexture = function () {
        var ret = ogl.createTextureOriginal();
        Object.defineProperty(ret, "textureID", {
            value: ogl.textureNum++,
            writable: false,
            configurable: false
        });
        return ret;
    };
    ogl.createBufferOriginal = ogl.createBuffer;
    ogl.createBuffer = function () {
        var ret = ogl.createBufferOriginal();
        ret.refnum = 1;

        return ret;
    };
    ogl.bufferIncRef = function (buffer) {
        if (buffer) {
            buffer.refnum++;
        }
    };
    ogl.bufferDecRef = function (buffer) {
        if (buffer) {
            buffer.refnum--;
            //delete buffer?
        }
    };

    ogl.campos = [0, 0, 0];
    ogl.Max_Texture_Size = ogl.getParameter(ogl.MAX_TEXTURE_SIZE);
    ogl.Max_Texture_Units = ogl.getParameter(ogl.MAX_TEXTURE_IMAGE_UNITS);
    this.canvas = settings.canvas;
    ogl.canvas = this.canvas;
    ogl.canvas_x = ogl.canvas.width;
    ogl.canvas_y = ogl.canvas.height;
    ogl.mvMatrixStack = new Array();
    ogl.mvPop = function () {
        this.mvMatrix = this.mvMatrixStack.pop();
    };
    ogl.mvPush = function () {
        var copy = mat4.create();
        mat4.copy(copy, this.mvMatrix);
        this.mvMatrixStack.push(copy);
    };
    //ret.enable(ret.CULL_FACE);
    ogl.enableAttrib = 0;
    ogl.pMatrix = mat4.perspective(mat4.create(), 45, ogl.canvas_x / ogl.canvas_y, settings.cam_persNear, settings.cam_persFar);
    ogl.pMatrixOrtho = mat4.ortho(mat4.create(), 0, ogl.canvas_x, ogl.canvas_y, 0, settings.cam_persNear, settings.cam_persFar);
    ogl.mvMatrix = mat4.create();
    ogl.camMatrix = mat4.create();
    ogl.Clear = function (color) {
        if (color)
            ogl.clearColor(color[0], color[1], color[2], color[3]);
        ogl.clear(ogl.COLOR_BUFFER_BIT | ogl.DEPTH_BUFFER_BIT);
        mat4.identity(ogl.mvMatrix);
        mat4.identity(ogl.camMatrix);
    };
    ogl.clearDepth(1.0);
    ogl.enable(ogl.DEPTH_TEST);
    ogl.depthFunc(ogl.LEQUAL);
    ogl.upload_mvMatrix = function (sprog) {

        _this.shader.shaderUniform("uCamPos", currentControl.pos);
        ogl.uniformMatrix4fv(ogl.aktShader.Uniform["uCMatrix"].loc, false, ogl.camMatrix);
        ogl.uniformMatrix4fv(ogl.aktShader.Uniform["uMVMatrix"].loc, false, ogl.mvMatrix);
        ogl.uniformMatrix4fv(ogl.aktShader.Uniform["uPMatrix"].loc, false, ogl.pMatrix);
    };
    ogl.aktShader = undefined;
    ogl.drawTriangles = function (buff, size) {
        ogl.bindBuffer(ogl.ELEMENT_ARRAY_BUFFER, buff);
        ogl.upload_mvMatrix();
        ogl.drawElements(ogl.TRIANGLES, size, ogl.UNSIGNED_SHORT, 0);
    };
    ogl.CubeInFrustum = function (pos, size) {
        var m = mat4.create();
        mat4.multiply(m, ogl.camMatrix, ogl.mvMatrix);
        mat4.multiply(m, ogl.pMatrix, m);
        this.plaincull = function (x, y, z, sx, sy, sz, ve) {
            if (ve[0] * (x) + ve[1] * (y) + ve[2] * (z) + ve[3] > 0)
                return true;
            if (ve[0] * (x + sx) + ve[1] * (y) + ve[2] * (z) + ve[3] > 0)
                return true;
            if (ve[0] * (x) + ve[1] * (y + sy) + ve[2] * (z) + ve[3] > 0)
                return true;
            if (ve[0] * (x + sx) + ve[1] * (y + sy) + ve[2] * (z) + ve[3] > 0)
                return true;
            if (ve[0] * (x) + ve[1] * (y) + ve[2] * (z + sz) + ve[3] > 0)
                return true;
            if (ve[0] * (x + sx) + ve[1] * (y) + ve[2] * (z + sz) + ve[3] > 0)
                return true;
            if (ve[0] * (x) + ve[1] * (y + sy) + ve[2] * (z + sz) + ve[3] > 0)
                return true;
            if (ve[0] * (x + sx) + ve[1] * (y + sy) + ve[2] * (z + sz) + ve[3] > 0)
                return true;
            return false;
        };
        var v = [m[3] - m[0], m[7] - m[4], m[11] - m[8], m[15] - m[12]];
        if (!this.plaincull(pos[0], pos[1], pos[2], size[0], size[1], size[2], v))
            return false;
        var v = [m[3] + m[0], m[7] + m[4], m[11] + m[8], m[15] + m[12]];
        if (!this.plaincull(pos[0], pos[1], pos[2], size[0], size[1], size[2], v))
            return false;
        var v = [m[3] - m[1], m[7] - m[5], m[11] - m[9], m[15] - m[13]];
        if (!this.plaincull(pos[0], pos[1], pos[2], size[0], size[1], size[2], v))
            return false;
        var v = [m[3] + m[1], m[7] + m[5], m[11] + m[9], m[15] + m[13]];
        if (!this.plaincull(pos[0], pos[1], pos[2], size[0], size[1], size[2], v))
            return false;
        var v = [m[3] + m[2], m[7] + m[6], m[11] + m[10], m[15] + m[14]];
        if (!this.plaincull(pos[0], pos[1], pos[2], size[0], size[1], size[2], v))
            return false;
        var v = [m[3] + m[2], m[7] + m[6], m[11] + m[10], m[15] + m[14]];
        if (!this.plaincull(pos[0], pos[1], pos[2], size[0], size[1], size[2], v))
            return false;
        return true;
    };
    ogl.camControl = function (pos, nezfok) {
        ogl.campos = [pos[0], pos[1], pos[2]];
        mat4.rotate(ogl.camMatrix, ogl.camMatrix, _this.math.degToRad(nezfok[0]), [1, 0, 0]);
        mat4.rotate(ogl.camMatrix, ogl.camMatrix, _this.math.degToRad(nezfok[1]), [0, 1, 0]);
        mat4.translate(ogl.camMatrix, ogl.camMatrix, [-pos[0], -pos[1], -pos[2]]);
    };
    this.updateCamera = function (control, mousePosInScreen) {
        currentControl = control;
        ogl.camControl(control.pos, control.nezszog);
        ogl.unprojAngel = _this.math.glUnproject(_this.math.degToRad, mousePosInScreen, control.nezszog, ogl.pMatrix, [ogl.canvas_x, ogl.canvas_y]);
    };
    ogl.resetMatrix = function () {
        mat4.identity(ogl.camMatrix);
        mat4.identity(ogl.mvMatrix);
    };
    ogl.glRayPick = this.glRayPick;
    ogl.glUnproject = this.math.glUnproject;
    var requestAnimFrame = (function () {
        return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                function (/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
                    window.setTimeout(callback, 1000 / 60);
                };
    })();
    this.getBlockTypeList = function () {
        return settings.blokkTypeList;
    };
    var innerDraw = function () {
        //sorrend fontos!
        if (settings.callback_draw !== undefined)
            settings.callback_draw();

        pve.geos.draw();

        _this.texman.checkTexture();
        requestAnimFrame(innerDraw);
    };
    this.loadMap_cb = function (data) {
        _this.loadMap_cb_proto(_this, data);
    };

    this.start = function () {
        innerDraw();
    };

    this.getTerrainAtlas = function () {
        return terrainAtlas;
    };
    this.getKarakterAtlas = function () {
        return karakterAtlas;
    };
    this.getProcessorCore = function () {
        return navigator.hardwareConcurrency;
    };
    var textureLoadComplete_Callback = function () {

    };
    this.newTerrainVilag = function () {
        return new PokolVoxelEngine_TerrainVilag(this, settings);
    };
    this.newVoxelVilag = function () {
        return new PokolVoxelEngine_VoxelVilag(this, settings);
    };
    this.getBlockSize = function () {
        return settings.voxel_blokSsize;
    };
    this.Load_Collada = function (data) {
        return this.load.collada(this, settings, data);
    };
    this.shader = this.glShader(this);



    this.texman = new PokolVoxelEngine_texman(this, textureLoadComplete_Callback);
    this.fizika = new PokolVoxelEngine_fizika(this);
    terrainAtlas = new this.texman.TexAtlas(settings.texatlas_fullsize, settings.texatlas_texsize, settings.texatlas_border, settings.texatlas_lod_size);
    function LoadTerrainAtlas(csop, url_prefix) {

        for (var i = 0; i < csop.elemek.length; i++) {
            var cselemid = csop.elemek[i];
            var cselem = settings.blokkTypeList[cselemid];
            if (cselem.url !== "") {
                var src = url_prefix + cselem.url;
                var tindex = terrainAtlas.loadimg(src);
                cselem.taxatlas_index = tindex;
            }
        }
    }

    for (var i in settings.blokkCsoportList) {
        var elem = settings.blokkCsoportList[i];
        LoadTerrainAtlas(elem, settings.url_prefix);
    }
    terrainAtlas.createTexture(9);
    terrainAtlas.updateTexture();


    this.geos = new PokolVoxelEngine_geos(this);
    this.terrainVilag = this.newTerrainVilag();
    this.voxelVilag = this.newVoxelVilag();
    this.animator = new PokolVoxelEngine_animator(this, settings);
    this.geometry = new this.geometryClass(this);
    this.animatorMesh = new this.animatorMeshClass(this);
    return ogl;
};
PokolVoxelEngine.prototype.clear = function (color) {
    if (color)
        this.ogl.clearColor(color[0], color[1], color[2], color[3]);
    this.ogl.clear(this.ogl.COLOR_BUFFER_BIT | this.ogl.DEPTH_BUFFER_BIT);
    mat4.identity(this.ogl.mvMatrix);
    mat4.identity(this.ogl.camMatrix);
};

PokolVoxelEngine.prototype.dinamicArray = function (inc_step, arrtype) {
    this.buffer = new arrtype(inc_step);
    this.pos = 0;
    this.need = function (size) {
        if (this.buffer.length < this.pos + size) {
            var ujlen = this.buffer.length + inc_step + size;
            var uj = new arrtype(ujlen);
            uj.set(this.buffer);
            this.buffer = uj;
        }
    };
    this.clean = function () {
        this.pos = 0;
    };
    this.add_1Item = function (i1) {
        this.need(1);
        this.buffer[this.pos++] = i1;
    };
    this.add_2Item = function (i1, i2) {
        this.need(2);
        this.buffer[this.pos++] = i1;
        this.buffer[this.pos++] = i2;

    };
    this.add_3Item = function (i1, i2, i3) {
        this.need(3);
        this.buffer[this.pos++] = i1;
        this.buffer[this.pos++] = i2;
        this.buffer[this.pos++] = i3;
    };
    this.add_4Item = function (i1, i2, i3, i4) {
        this.need(4);
        this.buffer[this.pos++] = i1;
        this.buffer[this.pos++] = i2;
        this.buffer[this.pos++] = i3;
        this.buffer[this.pos++] = i4;
    };
    this.add_data = function (data) {
        if (data.BYTES_PER_ELEMENT !== arrtype.BYTES_PER_ELEMENT) {
            throw "Size not equal";
        }
        this.buffer.set(data, this.pos);
        this.pos += data.length;
    };
}

function PokolVoxelEngine_dinamic_3d_array(mxz, type) {
    this.mxz = mxz;
    this.my = 0;
    this.ypos = 0;

    this.buffer = new type(0);
    this.getType = function () {
        return type;
    };
    this.copyFrom = function (source) {
        this.mxz = source.mxz;
        this.my = source.my;
        this.ypos = source.ypos;
        this.buffer = new type(source.buffer.length);
        this.buffer.set(source.buffer);
    };
    this.y_teszt = function (y, fill) {
        if (y >= (this.my + this.ypos)) {
            for (; y >= (this.my + this.ypos); this.my += 8)
                ;
            var ujdata = new type(this.mxz * this.mxz * this.my);
            ujdata.set(this.buffer);
            if (fill !== 0 && fill !== undefined) {
                for (var i = this.buffer.length; i < ujdata.length; i++) {
                    ujdata[i] = fill;
                }
            }
            this.buffer = ujdata;
        }
        if (y < this.ypos) {
            var regiy = this.ypos;
            for (; y < this.ypos; this.ypos -= 8, this.my += 8)
                ;
            var ujdata = new type(this.mxz * this.mxz * this.my);
            var masolpos = (regiy - this.ypos) * this.mxz * this.mxz;
            ujdata.set(this.buffer, masolpos);
            this.buffer = ujdata;
        }
    };
    this.setElem = function (x, y, z, t) {
        if (x < 0 || y < this.ypos || z < 0 || x >= this.mxz || z >= this.mxz || y >= (this.my + this.ypos))
            return;
        this.buffer[ x + (z * this.mxz) + ((y - this.ypos) * this.mxz * this.mxz) ] = t;
    };
    this.addElem = function (x, y, z, t) {
        if (x < 0 || y < this.ypos || z < 0 || x >= this.mxz || z >= this.mxz || y >= (this.my + this.ypos))
            return;
        this.buffer[ x + (z * this.mxz) + ((y - this.ypos) * this.mxz * this.mxz) ] += t;
    };
    this.getElem = function (x, y, z) {
        if (x < 0 || y < this.ypos || z < 0 || x >= this.mxz || z >= this.mxz || y >= (this.my + this.ypos))
            return 0;
        return this.buffer[ x + (z * this.mxz) + ((y - this.ypos) * this.mxz * this.mxz) ];
    };
    this.getElem_undef = function (x, y, z) {
        if (x < 0 || y < this.ypos || z < 0 || x >= this.mxz || z >= this.mxz || y >= (this.my + this.ypos))
            return undefined;
        return this.buffer[ x + (z * this.mxz) + ((y - this.ypos) * this.mxz * this.mxz) ];
    };
    this.getElem_noy = function (x, y, z) {
        if (x < 0 || y < 0 || z < 0 || x >= this.mxz || z >= this.mxz || y >= this.my)
            return 0;
        return this.buffer[ x + (z * this.mxz) + (y * this.mxz * this.mxz) ];
    };
    this.cleanFreeLevel = function (cleanvalue) {
        if (cleanvalue === undefined)
            cleanvalue = 0;
        var freeLevel = function (buffer, ypos, mxz) {
            ypos *= mxz * mxz;
            for (var i = 0; i < (mxz * mxz); i++) {
                if (buffer[i + ypos] !== cleanvalue)
                    return false;
            }
            return true;
        };
        var index = 0;
        while (freeLevel(this.buffer, index, this.mxz)) {
            index++;
        }
        if (index === this.my) {//all data cleaned
            this.buffer = new type(0);
            this.my = 0;
            this.ypos = 0;
        } else {
            var index2 = this.my;
            while (freeLevel(this.buffer, index2 - 1, this.mxz)) {
                index2--;
            }
            index2 -= index;
            this.my = index2;
            this.ypos += index;
            var ujbuff = new Uint16Array(this.my * this.mxz * this.mxz);


            ujbuff.set(this.buffer.subarray(index * this.mxz * this.mxz, ujbuff.length + (index * this.mxz * this.mxz)), 0);
            this.buffer = ujbuff;
        }
    };
}
PokolVoxelEngine.prototype.load.opendialog = function (cb_ok, cb_err) {

    var input = document.createElement("INPUT");
    document.body.appendChild(input);
    input.type = "file";
    input.style.display = "none";
    input.onchange = function (event) {
        var file = event.target.files[0];
        if (!file) {
            if (cb_err)
                cb_err(event);
        }
        var reader = new FileReader();
        reader.onload = function (e) {
            if (cb_ok)
                cb_ok(e.target.result);
        };
        reader.onerror = cb_err;
        reader.readAsBinaryString(file);
    };
    input.focus();
    input.click();
};

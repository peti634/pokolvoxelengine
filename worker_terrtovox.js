function PokolVoxelEngine_terrtovox(terr_mxz, terlist, tpx, tpz, voxmxz, blocksize) {
    var ARRAY_STEP = 10000;
    var upddata = new Int32Array(ARRAY_STEP);
    var upddataindex = 0;
    var vposx = tpx * voxmxz;
    var vposz = tpz * voxmxz;
    this.getHeight = function (x, z) {
        x += vposx;
        z += vposz;
        if (x < 0 || z < 0)
            return [undefined, 0];
        var terrdata = undefined;
        var tipdata;
        var tx = (x / terr_mxz) | 0;
        var tz = (z / terr_mxz) | 0;
        for (var i = 0; i < terlist.length; i++) {
            if (terlist[i].x === tx && terlist[i].z === tz) {
                terrdata = terlist[i].data;
                tipdata = terlist[i].tipdata;
            }
        }
        if (terrdata === undefined) {
            return [undefined, 0];
        }
        x %= terr_mxz;
        z %= terr_mxz;

        var tipus = tipdata[x + (z * (terr_mxz + 1))];
        tipus += 1;//1-el eltoljuk

        return [terrdata[x + (z * (terr_mxz + 1))], tipus];
    };
    this.addElem = function (x, y, z, t) {
        if (upddata.length <= upddataindex) {
            var ujupddata = new Int32Array(upddata.length + ARRAY_STEP);
            ujupddata.set(upddata);
            upddata = ujupddata;
        }

        upddata[upddataindex + 0] = x;
        upddata[upddataindex + 1] = y;
        upddata[upddataindex + 2] = z;
        upddata[upddataindex + 3] = t;

        upddataindex += 4;
    };

    for (var i = 0; i < voxmxz; i++) {
        for (var j = 0; j < voxmxz; j++) {
            var ret = this.getHeight(i, j);


            if (ret === undefined)
                continue;
            var t = ret[1];
            var e = ret[0];
            e = (e / blocksize) | 0;

            this.addElem(i,e,j,t);


            var mine = e;

            var e1 = (this.getHeight(i - 1, j)[0] / blocksize) | 0;
            var e2 = (this.getHeight(i + 1, j)[0] / blocksize) | 0;
            var e3 = (this.getHeight(i, j - 1)[0] / blocksize) | 0;
            var e4 = (this.getHeight(i, j + 1)[0] / blocksize) | 0;
            if (e1 !== undefined && e1 < mine) {
                mine = e1;
            }
            if (e2 !== undefined && e2 < mine) {
                mine = e2;
            }
            if (e3 !== undefined && e3 < mine) {
                mine = e3;
            }
            if (e4 !== undefined && e4 < mine) {
                mine = e4;
            }
            for (var k = e - 1; k >= mine; k--) {
                this.addElem(i,k,j,t);
            }

        }
    }
    return upddata.subarray(0, upddataindex);
    
}

if (self.document === undefined) {
    //this worker script

    self.onmessage = function (e) {
        var data = e.data;
        var tpx = data.tpx;
        var tpz = data.tpz;
        if (data.type === "TtoV_UPD") {
            var retdata = PokolVoxelEngine_terrtovox(data.tmxz, data.terlist, tpx, tpz, data.mxz, data.bs);
            self.postMessage({px: tpx, pz: tpz, type: "TtoV_UPD", upddate: retdata}, [retdata.buffer]);
        } else if (data.type === "TtoV_CLEAN") {
            self.postMessage({px: data.x, pz: data.z, type: "TtoV_CLEAN"});
        }
    };
} else {
    //this not worker script

}

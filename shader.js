PokolVoxelEngine.prototype.glShader = function () {
    return new PokolVoxelEngine_glShader(this);
};

function PokolVoxelEngine_glShader(core) {
    var gl = core.ogl;
    var _this = this;
    var sProg = [];
    function createShaderProg(fcode, vcode, name) {

        var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
        gl.shaderSource(fragmentShader, fcode);
        gl.compileShader(fragmentShader);

        if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
            alert("Fragment shader (" + name + ") error:\nFS An error occurred compiling the shaders:\n" + gl.getShaderInfoLog(fragmentShader));
            return undefined;
        }

        var vertexShader = gl.createShader(gl.VERTEX_SHADER);
        gl.shaderSource(vertexShader, vcode);
        gl.compileShader(vertexShader);

        if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
            alert("Vertex shader (" + name + ") error:\nVS An error occurred compiling the shaders:\n" + gl.getShaderInfoLog(vertexShader));
            return undefined;
        }

        var shaderProgram = gl.createProgram();
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);
        if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            alert("Unable to initialize the shader program.");
            return undefined;
        }

        return shaderProgram;

    }

    function InitShader(fcode, vcode, name) {
        var retprog = createShaderProg(fcode, vcode, name);

        if (retprog === undefined)
            return undefined;

        retprog.Attrib = [];
        retprog.Uniform = {};

        gl.useProgram(retprog);

        var attrb_len = gl.getProgramParameter(retprog, gl.ACTIVE_ATTRIBUTES);
        for (var j = 0; j < attrb_len; j++) {
            var elem = gl.getActiveAttrib(retprog, j);
            var loc = gl.getAttribLocation(retprog, elem.name);
            switch (elem.type) {
                case gl.FLOAT_MAT2:
                    retprog.Attrib[elem.name] = {loc: loc + 0, type: elem.type};
                    retprog.Attrib[elem.name + '[1]'] = {loc: loc + 1, type: elem.type};
                    break;
                case gl.FLOAT_MAT3:
                    retprog.Attrib[elem.name] = {loc: loc + 0, type: elem.type};
                    retprog.Attrib[elem.name + '[1]'] = {loc: loc + 1, type: elem.type};
                    retprog.Attrib[elem.name + '[2]'] = {loc: loc + 2, type: elem.type};
                    break;
                case gl.FLOAT_MAT4:
                    retprog.Attrib[elem.name] = {loc: loc + 0, type: elem.type};
                    retprog.Attrib[elem.name + '[1]'] = {loc: loc + 1, type: elem.type};
                    retprog.Attrib[elem.name + '[2]'] = {loc: loc + 2, type: elem.type};
                    retprog.Attrib[elem.name + '[3]'] = {loc: loc + 3, type: elem.type};
                    break;
                default:
                    retprog.Attrib[elem.name] = {loc: loc, type: elem.type};
                    break;
            }





        }

        var unif_len = gl.getProgramParameter(retprog, gl.ACTIVE_UNIFORMS);
        for (var j = 0; j < unif_len; j++) {
            var elem = gl.getActiveUniform(retprog, j);
            if (elem.size === 1) {
                retprog.Uniform[elem.name] = {loc: gl.getUniformLocation(retprog, elem.name), type: elem.type};

            } else {//array uniform
                var arrayname = elem.name.substr(0, elem.name.indexOf("["));
                for (var k = 0; k < elem.size; k++) {
                    var ename = arrayname + "[" + k + "]";
                    retprog.Uniform[ename] = {loc: gl.getUniformLocation(retprog, ename), type: elem.type};
                }

            }

        }
        if (retprog.Uniform["uPOrthoMatrix"])
            gl.uniformMatrix4fv(retprog.Uniform["uPOrthoMatrix"].loc, false, gl.pMatrixOrtho);
        else
        if (retprog.Uniform["uPMatrix"])
            gl.uniformMatrix4fv(retprog.Uniform["uPMatrix"].loc, false, gl.pMatrix);
        else
            alert("Can't find project matrix!");


        return retprog;
    }
    function optionsfakt(param) {
        if (param < 0)
            return [];
        var ret = [];
        var e = param--;
        var reqarr = [];
        if (param >= 0) {
            reqarr = optionsfakt(param);
            for (var i = 0; i < reqarr.length; i++) {
                var elem = reqarr[i];
                ret.push(elem.slice(0));
                elem.push(e);
                ret.push(elem.slice(0));
            }
        }
        ret.push([e]);
        return ret;
    }
    function OptionsShaders() {
        this.shaderlist = [];
        this.optionlist;
        this.AddShader = function (fcode, vcode, name, optionsbit) {
            var rets = InitShader(fcode, vcode, name);
            rets.optionsbit = optionsbit;
            this.shaderlist.push(rets);
        };
        this.Init = function (fcode, vcode, name, optionlist) {
            this.optionlist = optionlist;
            var faklist = optionsfakt(this.optionlist.length - 1);
            faklist.push([this.optionlist.length]);
            for (var i = 0; i < faklist.length; i++) {

                var code = "";
                var elem = faklist[i];
                var bits = 0;
                for (var k = 0; k < elem.length; k++) {

                    var optionstring = optionlist[elem[k]];
                    if (optionstring === undefined || optionstring.length === 0)
                        continue;
                    bits += 1 << elem[k];
                    code += "#define " + optionstring + "\r\n";
                }

                this.AddShader(code + fcode, code + vcode, name, bits);
            }
        };
        this.KeresShader = function (options) {
            var bits = 0;
            for (var i = 0; options && i < options.length; i++) {
                var elem = options[i];
                var j;
                for (j = 0; j < this.optionlist.length; j++) {
                    if (elem === this.optionlist[j])
                        break;
                }
                if (j === this.optionlist.length) {
                    return undefined;
                }
                bits += 1 << j;
            }
            for (var i = 0; i < this.shaderlist.length; i++) {
                if (this.shaderlist[i].optionsbit === bits) {
                    return this.shaderlist[i];
                }
            }
            return undefined;
        };
    }
    this.loadShader = function (fs, vs, name, options) {
        var sprogobj = new OptionsShaders();
        sprogobj.Init(fs, vs, name, options);
        return sprogobj;
    };
    this.shaderUniform = function (nev, data) {
        var loc = gl.aktShader.Uniform[nev];
        if (loc === undefined)
            return;
        switch (loc.type) {
            case gl.INT:
            case gl.SAMPLER_2D:
                gl.uniform1i(loc.loc, data[0]);
                break;
            case gl.FLOAT:
                gl.uniform1f(loc.loc, data[0]);
                break;
            case gl.FLOAT_VEC2:
                gl.uniform2f(loc.loc, data[0], data[1]);
                break;
            case gl.FLOAT_VEC3:
                gl.uniform3f(loc.loc, data[0], data[1], data[2]);
                break;
            case gl.FLOAT_VEC4:
                gl.uniform4f(loc.loc, data[0], data[1], data[2], data[3]);
                break;
        }

    };

    this.shaderAtrribute = function (nev, buff) {
        var loc = gl.aktShader.Attrib[nev];
        if (loc === undefined)
            return;
        if (buff === undefined) {
            var bit = 1 << loc.loc;
            if ((gl.enableAttrib & bit) !== 0) {
                gl.disableVertexAttribArray(loc.loc);
                gl.enableAttrib &= ~(bit);
            }
            return;
        }
        if ((gl.enableAttrib & bit) === 0) {
            gl.enableAttrib |= 1 << loc.loc;
            gl.enableVertexAttribArray(loc.loc);
        }
        gl.bindBuffer(gl.ARRAY_BUFFER, buff);
        var size = 0;
        switch (loc.type) {
            case gl.FLOAT:
                size = 1;
                break;
            case gl.FLOAT_VEC2:
                size = 2;
                break;
            case gl.FLOAT_VEC3:
                size = 3;
                break;
            case gl.FLOAT_VEC4:
                size = 4;
                break;
            case gl.FLOAT_MAT2:
                gl.vertexAttribPointer(loc.loc + 0, 2, gl.FLOAT, false, 4 * 4, 0 * 4);
                gl.vertexAttribPointer(loc.loc + 1, 2, gl.FLOAT, false, 4 * 4, 2 * 4);
                return;
                break;
            case gl.FLOAT_MAT3:
                gl.vertexAttribPointer(loc.loc + 0, 3, gl.FLOAT, false, 9 * 4, 0 * 4);
                gl.vertexAttribPointer(loc.loc + 1, 3, gl.FLOAT, false, 9 * 4, 3 * 4);
                gl.vertexAttribPointer(loc.loc + 2, 3, gl.FLOAT, false, 9 * 4, 6 * 4);
                return;
                break;
            case gl.FLOAT_MAT4:
                gl.vertexAttribPointer(loc.loc + 0, 4, gl.FLOAT, false, 16 * 4, 0 * 4);
                gl.vertexAttribPointer(loc.loc + 1, 4, gl.FLOAT, false, 16 * 4, 4 * 4);
                gl.vertexAttribPointer(loc.loc + 2, 4, gl.FLOAT, false, 16 * 4, 8 * 4);
                gl.vertexAttribPointer(loc.loc + 3, 4, gl.FLOAT, false, 16 * 4, 12 * 4);
                return;
                break;
        }
        gl.vertexAttribPointer(loc.loc, size, gl.FLOAT, false, 0, 0);
    };
    this.shaderUse = function (optshader, name, options) {
        var prog = optshader.KeresShader(options);
        if (prog === undefined)
            throw "Shader options nem található: " + name + " -> " + options.toString();
        gl.useProgram(prog);
        for (var i = 0; gl.enableAttrib >> i; i++)
            if ((1 << i) & gl.enableAttrib)
                gl.disableVertexAttribArray(i);
        gl.enableAttrib = 0;
        for (var attrindex in prog.Attrib) {
            gl.enableVertexAttribArray(prog.Attrib[attrindex].loc);
            gl.enableAttrib |= 1 << prog.Attrib[attrindex].loc;
        }
        gl.aktShader = prog;
    };

    this.shaderUseList = function (name, options) {
        var optshader = sProg[name];
        if (optshader === undefined) {
            throw "No shader prog: " + name;
        }
        _this.shaderUse(optshader, name, options);
    };
    this.attributeObject = function (name, data) {
        var atrb = gl.createBuffer();
        var name = name;
        var use;
        if (data === undefined) {
            use = false;
        } else {
            use = true;
            gl.bindBuffer(gl.ARRAY_BUFFER, atrb);
            gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
        }
        this.select = function () {
            if (use) {
                core.shader.shaderAtrribute(name, atrb);
            } else {
                core.shader.shaderAtrribute(name, undefined);
            }
        };
        this.deselect = function () {
            core.shader.shaderAtrribute(name, undefined);
        };
    };
}

var VOXEL_RAYPICK_TREENUM = 4;
function PokolVoxelEngine_TerrainVoxel(core, px, pz, meret_xz, blocksize, szulovilag) {
    var gl = core.ogl;
    var blocktypelist = core.getBlockTypeList();
    this.treeMaxHeight = new Float32Array(VOXEL_RAYPICK_TREENUM * VOXEL_RAYPICK_TREENUM);
    this.treeMinHeight = new Float32Array(VOXEL_RAYPICK_TREENUM * VOXEL_RAYPICK_TREENUM);
    this.heightmax;
    this.heightmin;

    this.needUpd;

    this.px = px;
    this.pz = pz;
    this.mxz = meret_xz;
    this.my;
    this.ypos;
    this.szulovilag = szulovilag;
    this.needFBdraw = false;
    this.blockdata;
    this.blockdata_terr; //csak terrain adatok

    this.TextureDataId = core.texman.getfreetex(gl.NEAREST, gl.NEAREST);
    this.TextureFenyID = core.texman.getfreetex(gl.NEAREST, gl.NEAREST);
    this.csoportindexpos;
    this.textureSizeX;
    this.textureSizeY;

    this.blocksize = blocksize;

    this.ARRAY_STEP = 10000;
    this.upddata = new Int32Array(this.ARRAY_STEP);
    this.upddataindex;
    this.upddata_data = [];

    this.IndexBuf = gl.createBuffer();
    this.VertexBuf = gl.createBuffer();
    this.TexcoordBuf = gl.createBuffer();
    this.IndexBufSize;

    this.geos = new core.geos.GeosContainer();
    this.sendworker = function (msg) {
        this.szulovilag.sendworker(px, pz, msg);
    };
    this.updateTreeHeight = function () {
        var arany = (this.mxz / VOXEL_RAYPICK_TREENUM);
        this.tesztHeight = function (index, xpos, ypos) {
            this.treeMaxHeight[index] = Number.NEGATIVE_INFINITY;
            this.treeMinHeight[index] = Number.POSITIVE_INFINITY;
            for (var i = 0; i <= arany; i++) {
                for (var j = 0; j <= arany; j++) {
                    var bx = (xpos * arany + i) | 0;
                    var by = (ypos * arany + j) | 0;
                    for (var y = this.ypos; y < this.my; y++) {
                        var e = this.getElem(bx, y, by);
                        if (e === 0)
                            continue;
                        if (this.treeMaxHeight[index] < y + 1)
                            this.treeMaxHeight[index] = y + 1;
                        if (this.treeMinHeight[index] > y)
                            this.treeMinHeight[index] = y;
                    }


                }
            }

        };
        for (var i = 0; i < this.treeMaxHeight.length; i++) {
            this.tesztHeight(i, i % VOXEL_RAYPICK_TREENUM, (i / VOXEL_RAYPICK_TREENUM) | 0);
            if (i === 0) {
                this.heightmax = this.treeMaxHeight[0];
                this.heightmin = this.treeMinHeight[0];
            } else {
                if (this.heightmax < this.treeMaxHeight[i])
                    this.heightmax = this.treeMaxHeight[i];
                if (this.heightmin > this.treeMinHeight[i])
                    this.heightmin = this.treeMinHeight[i];
            }
        }

    };
    this.del = function () {
        this.sendworker({type: "DELDATA"});

    };
    this.init = function () {
        this.my = 0;
        this.ypos = 0;
        //
        this.blockdata = new PokolVoxelEngine_dinamic_3d_array(this.mxz, Uint16Array);
        this.blockdata_terr = undefined;

        this.needUpd = true;

        this.upddataindex = 0;
        this.upddata = new Int32Array(this.ARRAY_STEP);

    };

    this.addObj = function (x, y, z, tipus, typeelem, data) {
        if (Object.prototype.toString.call(data) !== "[object Uint32Array]" || data.length !== 2) {
            throw "Geo_Feny data error";
        }
        this.upddata_data["d" + x + "_" + y + "_" + z] = data;

        var color = [data[0] & 0xFF, (data[0] >> 8) & 0xFF, (data[0] >> 16) & 0xFF];
        var obj = new core.geos.Geo_Feny([this.blocksize, this.blocksize, this.blocksize], [x * this.blocksize, y * this.blocksize, z * this.blocksize], color);
        obj.typeid = tipus;
        obj.infodata32 = data;

        this.geos.addList(obj);

    };

    this.addElem = function (x, y, z, tipus, data) {
        this.blockdata.y_teszt(y);
        if (this.blockdata_terr !== undefined)
            this.blockdata_terr.y_teszt(y);
        this.my = this.blockdata.my;
        this.ypos = this.blockdata.ypos;


        var gett = this.blockdata.getElem(x, y, z);
        if ((gett === 0 && tipus === 0) || (gett !== 0 && tipus !== 0)) {
            return;
        }
        if (tipus === 0) {

            var typeelem = blocktypelist[gett];
            if (typeelem.tul.obj3d === 1) {
                this.geos.delElemPos(x * this.blocksize, y * this.blocksize, z * this.blocksize);
            }
        } else {

            var typeelem = blocktypelist[tipus];
            if (typeelem.tul.obj3d === 1) {
                this.addObj(x, y, z, tipus, typeelem, data);
            }
        }
        if (this.blockdata_terr !== undefined)
            this.blockdata_terr.setElem(x, y, z, 0);

        this.blockdata.setElem(x, y, z, tipus);

        this.needUpd = true;
        if (this.upddata.length <= this.upddataindex) {
            var ujupddata = new Int32Array(this.upddata.length + this.ARRAY_STEP);
            ujupddata.set(this.upddata);
            this.upddata = ujupddata;
        }

        this.upddata[this.upddataindex + 0] = x;
        this.upddata[this.upddataindex + 1] = y;
        this.upddata[this.upddataindex + 2] = z;
        this.upddata[this.upddataindex + 3] = tipus;

        this.upddataindex += 4;
    };
    this.delElem = function (x, y, z) {
        this.addElem(x, y, z, 0);

    };
    this.getElem = function (x, y, z) {
        return this.blockdata.getElem(x, y, z);
    };

    this.oszlopUpd = function (x, z, arr) {

        arr.sort(function (a, b) {
            return a.y - b.y;
        });
        var arrindex = 0;

        var legalso = this.ypos;
        if (arr.length > 0 && arr[0].y < legalso) {
            this.addElem(x, arr[0].y, z, arr[0].t);
        }

        for (var i = 0; i < this.my; i++) {
            var iypos = i + this.ypos;
            var arrelem = arr[arrindex];
            var elem = this.getElem(x, iypos, z);
            if (arrelem && arrelem.y === iypos) {
                if (elem !== 0 && elem !== arrelem.t) {//van ott valami, csak rossz típus
                    this.delElem(x, iypos, z);
                    this.addElem(x, iypos, z, arrelem.t);
                }
                if (elem === 0) {//nincs ott semmi
                    this.addElem(x, iypos, z, arrelem.t);
                }
                arrindex++;
            } else if (elem !== 0) {//van ott valami, de nem kellene
                this.delElem(x, iypos, z);
            }

        }
        for (var i = arrindex; i < arr.length; i++) {
            var arrelem = arr[i];
            this.addElem(x, arrelem.y, z, arrelem.t);
        }

    };
    this.SetTexutreData = function (x, y, data) {
        if (gl.Max_Texture_Size < x || gl.Max_Texture_Size < y) {
            throw "textureData > resize > Textura határ elérve";
        }
        this.textureSizeX = x;
        this.textureSizeY = y;
        gl.bindTexture(gl.TEXTURE_2D, this.TextureDataId);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, x, y, 0, gl.RGBA, gl.UNSIGNED_BYTE, data);
    };
    this.SetTexutreFeny = function (x, y, data) {
        if (gl.Max_Texture_Size < x || gl.Max_Texture_Size < y) {
            throw "textureFeny > resize > Textura határ elérve";
        }
        /*
         ellen�rz�s?
         this.textureSizeX = x;
         this.textureSizeY = y;
         */
        gl.bindTexture(gl.TEXTURE_2D, this.TextureFenyID);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, x, y, 0, gl.RGBA, gl.UNSIGNED_BYTE, data);
    };
    this.onmessage = function (data) {

        if (data.type === "DATA") {
            this.csoportindexpos = data.csoportindexpos;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexBuf);
            gl.bufferData(gl.ARRAY_BUFFER, data.vertdata, gl.STATIC_DRAW);

            gl.bindBuffer(gl.ARRAY_BUFFER, this.TexcoordBuf);
            gl.bufferData(gl.ARRAY_BUFFER, data.texcoord, gl.STATIC_DRAW);


            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexBuf);
            this.IndexBufSize = data.vertindex.length;
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, data.vertindex, gl.STATIC_DRAW);

            this.SetTexutreData(data.texdatax, data.texdatay, data.texdata);
            this.SetTexutreFeny(data.texdatax, data.texdatay, data.fenydata);
            this.needFBdraw = true;
            this.updateTreeHeight();
        } else if (data.type === "FENYDATA") {
            this.SetTexutreFeny(data.texfenyx, data.texfenyy, data.fenydata);
        }


    };
    this.update = function () {
        if (!this.needUpd)
            return;
        this.upddata = this.upddata.subarray(0, this.upddataindex);

        this.sendworker({type: "UPDDATA", upddata: this.upddata, upddata_data: this.upddata_data}, [this.upddata.buffer]);
        this.upddataindex = 0;
        this.upddata = new Int32Array(this.ARRAY_STEP);
        this.upddata_data = [];
        this.needUpd = false;
    };
    this.Draw = function (csop, alp) {
        if (this.VertexBuf === undefined)
            return;
        if (csop === -1) {
            this.geos.draw();
            return;
        }

        core.texman.lockTexture(this.TextureDataId, "uTexdata");
        core.texman.lockTexture(this.TextureFenyID, "uFeny");

        core.shader.shaderUniform("uSamplerMeret", [1 / this.textureSizeX, 1 / this.textureSizeY]);
        core.shader.shaderUniform("uAlpha", [alp]);

        core.shader.shaderAtrribute("aVertexPosition", this.VertexBuf);
        core.shader.shaderAtrribute("aTexcoord", this.TexcoordBuf);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexBuf);
        gl.upload_mvMatrix();
        if (csop === 1) {
            if (this.csoportindexpos !== undefined && this.csoportindexpos[1] !== undefined) {
                var index = this.csoportindexpos[1][0];
                var size = this.csoportindexpos[1][1];
                gl.drawElements(gl.TRIANGLES, size, gl.UNSIGNED_INT, index);
            }
        } else if (csop === 2) {
            if (this.csoportindexpos !== undefined && this.csoportindexpos[2] !== undefined) {
                var index = this.csoportindexpos[2][0];
                var size = this.csoportindexpos[2][1];
                gl.drawElements(gl.TRIANGLES, size, gl.UNSIGNED_INT, index * 4);
            }
            if (this.csoportindexpos !== undefined && this.csoportindexpos[3] !== undefined) {
                var index = this.csoportindexpos[3][0];
                var size = this.csoportindexpos[3][1];
                gl.drawElements(gl.TRIANGLES, size, gl.UNSIGNED_INT, index * 4);
            }

        }


        core.texman.unlockTexture(this.TextureDataId);
        core.texman.unlockTexture(this.TextureFenyID);

    };
    this.rayPick = function (campos, camAngle) {

        var raypick = new core.math.rayPickGeometry(campos, camAngle);

        var treeblock = new Array(this.treeMaxHeight.length);

        var arany = (this.mxz / VOXEL_RAYPICK_TREENUM);
        var treemeret = arany * this.blocksize;
        for (var i = 0; i < this.treeMaxHeight.length; i++) {
            var elem = {};

            var x = (i % VOXEL_RAYPICK_TREENUM);
            var z = (i / VOXEL_RAYPICK_TREENUM) | 0;
            var blockmx = arany;
            var blockmz = arany;

            elem.merx = blockmx * this.blocksize;
            elem.mery = this.treeMaxHeight[i] - this.treeMinHeight[i];
            elem.merz = blockmz * this.blocksize;

            elem.posx = x * treemeret;
            elem.posy = this.treeMinHeight[i];
            elem.posz = z * treemeret;


            elem.id_x = x * arany;
            elem.id_xm = blockmx;
            elem.id_z = z * arany;
            elem.id_zm = blockmz;

            var kozepx = elem.posx + (elem.merx) / 2;
            var kozepz = elem.posz + (elem.merz) / 2;

            kozepx -= campos[0];
            kozepz -= campos[2];

            kozepx = kozepx * kozepx;
            kozepz = kozepz * kozepz;

            elem.tav = Math.sqrt(kozepx + kozepz);

            treeblock[i] = elem;

        }

        treeblock.sort(function (a, b) {
            return a.tav - b.tav;
        });



        for (var i = 0; i < treeblock.length; i++) {
            var treeelem = treeblock[i];
            var legtav = Number.POSITIVE_INFINITY;
            var ret = [0, 0, 0, -1];
            if (raypick.box([treeelem.posx, treeelem.posy * this.blocksize, treeelem.posz], treeelem.merx, treeelem.mery * this.blocksize, treeelem.merz)) {
                for (var fx = (treeelem.id_x | 0); fx < treeelem.id_x + treeelem.id_xm && fx < this.mxz; fx++) {
                    for (var fz = (treeelem.id_z | 0); fz < treeelem.id_z + treeelem.id_zm && fz < this.mxz; fz++) {
                        for (var fy = treeelem.posy; fy < treeelem.posy + treeelem.mery; fy++) {

                            var elem = this.getElem(fx, fy, fz);
                            if (elem === 0)
                                continue;


                            var raypos = [fx * this.blocksize, fy * this.blocksize, fz * this.blocksize];
                            var oldal = raypick.box_getoldal(raypos, this.blocksize, this.blocksize, this.blocksize);
                            if (!oldal)
                                continue;
                            var tav = vec3.distance(campos, raypos);
                            if (tav < legtav) {
                                ret = [fx, fy, fz, oldal];
                                legtav = tav;
                            }
                        }
                    }

                }
            }
            if (legtav !== Number.POSITIVE_INFINITY) {
                return ret;
            }
        }


        return [0, 0, 0, -1];
    };
    this.addFeny = function (pos, color, range) {
        this.sendworker({type: "NEWFENY", pos: pos, color: color, range: range});
    };
    this.init();
}

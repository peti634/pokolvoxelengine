/* global mat4 */
/* global vec4 */
/* global vec3 */
/* global vec2 */
/* global quat */
/* global PokolVoxelEngine*/

function PokolVoxelEngine_geos(core)
{
    var allGeoIdCounter = 1;
    var gl = core.ogl;
    var shaderObj;
    this.globalGeos;
    this.GeosContainer = function () {
        this.geo_list = [];
        this.customShader;
        this.addList = function (geo) {
            return this.geo_list.push(geo);
        };
        this.delElemPos = function (x, y, z) {
            for (var i = 0; i < this.geo_list.length; i++) {
                var elem = this.geo_list[i];
                if (elem.pos[0] === x && elem.pos[1] === y && elem.pos[2] === z) {
                    this.geo_list.splice(i, 1);
                    elem.delBuffers();
                    break;
                }
            }
        };
        this.delElemRef = function (ref) {
            for (var i = 0; i < this.geo_list.length; i++) {
                var elem = this.geo_list[i];
                if (elem === ref) {
                    this.geo_list.splice(i, 1);
                    elem.delBuffers();
                    break;
                }
            }
        };
        this.draw = function () {
            if (this.customShader !== undefined) {
                core.shader.shaderUse(this.customShader, "geos-custom");
            } else {
                core.shader.shaderUse(shaderObj, "geos");
            }
            gl.enable(gl.CULL_FACE);
            for (var i = 0; i < this.geo_list.length; i++) {
                var elem = this.geo_list[i];
                gl.mvPush();
                elem.draw();
                gl.mvPop();
            }
            gl.disable(gl.CULL_FACE);


        };
    };
    this.globalGeos = new this.GeosContainer();
    this.addList = function (geo) {
        return this.globalGeos.addList(geo);
    };
    this.draw = function () {
        this.globalGeos.draw();
    };

    function SimpleSuper(geometry, pos, color) {
        var protected = {
            geoId: allGeoIdCounter,
            hidden: false,
            parent: undefined,
            parentMatrix: undefined,
            childrenList: [],
            pos: vec3.create(),
            rotate: quat.create(),
            color: color,
            TexcoordPos: [0, 0, 1, 1],
            geometryObject: geometry,
            ExtMatrix: undefined,
            Texture: undefined,
            DrawMode: gl.TRIANGLES,
            AlphaValue: 1.0,
            externalTextures: [],
            externalUniforms: [],
            externalAttributes: [],
            drawAfterCallback: undefined,
            userObj: undefined//need for callback
        };
        allGeoIdCounter++;
        if (protected.geometryObject === undefined) {
            protected.geometryObject = new core.geometry.geometryObject();
        }
        this.getGeoId = function () {
            return protected.geoId;
        };
        this.draw = function () {
            if (protected.hidden)
                return;

            //Sorrend: parent,ext, pos-rot
            //Fontos mert a parent és az EXT-nek egymás után kell lennie, 
            //mivel ha nem így van, nem másolható a parent mátrix nem másolható az extbe


            if (protected.parentMatrix !== undefined)
                mat4.mul(gl.mvMatrix, gl.mvMatrix, protected.parentMatrix);


            if (protected.ExtMatrix !== undefined)
                mat4.mul(gl.mvMatrix, gl.mvMatrix, protected.ExtMatrix);
            this.getTranslateMatrix(gl.mvMatrix);


            if (protected.drawAfterCallback !== undefined) {
                protected.drawAfterCallback(this, protected.userObj);
            }
            gl.upload_mvMatrix();

            for (var i = 0; i < protected.externalAttributes.length; i++) {
                protected.externalAttributes[i].select();
            }
            for (var i = 0; i < protected.externalUniforms.length; i++) {
                var uniform = protected.externalUniforms[i];
                if (uniform && uniform.data) {
                    core.shader.shaderUniform(uniform.name, uniform.data);
                }
            }
            for (var i = 0; i < protected.externalTextures.length; i++) {
                var tex = protected.externalTextures[i];
                if (tex) {
                    core.texman.lockTexture(tex.texture, tex.name);
                }
            }

            core.shader.shaderUniform("uSzin", protected.color);

            if (protected.Texture === undefined) {
                core.shader.shaderUniform("uTexUse", [0]);
            } else {
                core.shader.shaderUniform("uTexUse", [1]);
                core.texman.lockTexture(protected.Texture, "uTex");
            }



            core.shader.shaderUniform("uFenySzin[0]", [0.8, 0.8, 0.8]);
            core.shader.shaderUniform("uFenySzin[1]", [0.7, 0.7, 0.7]);
            core.shader.shaderUniform("uFenySzin[2]", [1, 1, 1]);
            core.shader.shaderUniform("uFenySzin[3]", [0.1, 0.1, 0.1]);
            core.shader.shaderUniform("uFenySzin[4]", [0.6, 0.6, 0.6]);
            core.shader.shaderUniform("uFenySzin[5]", [0.5, 0.5, 0.5]);
            core.shader.shaderUniform("uAlpha", [protected.AlphaValue]);

            if (protected.AlphaValue < 1.0) {
                gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
                gl.enable(gl.BLEND);
            }

            protected.geometryObject.select();
            gl.drawElements(protected.DrawMode, protected.geometryObject.getElementLength(), gl.UNSIGNED_SHORT, 0);
            if (protected.AlphaValue < 1.0) {
                gl.disable(gl.BLEND);
            }
            if (protected.Texture !== undefined) {
                core.texman.unlockTexture(protected.Texture);
            }
            for (var i = 0; i < protected.externalTextures.length; i++) {
                var tex = protected.externalTextures[i];
                if (tex) {
                    core.texman.unlockTexture(tex.texture);
                }
            }
            //optimalizálható, nem kell mindig be kikapcsolgatni, viszont van aki nem használja, ekkor bekapcsolva maradna
            for (var i = 0; i < protected.externalAttributes.length; i++) {
                protected.externalAttributes[i].deselect();
            }
        };
        this.getVertexData = function (no_rot, no_pos) {
            if (no_rot === true && no_pos === true) {
                //return [protected.VertexData.concat(), protected.ElementData.concat()];
            }
            var mm = mat4.create();
            if (no_pos !== true) {
                mat4.translate(mm, mm, protected.pos);
            }
            if (no_rot !== true) {
                var rotmat = mat4.create();
                mat4.fromQuat(rotmat, protected.rotate);
                mat4.mul(mm, mm, rotmat);
            }
            if (protected.ExtMatrix !== undefined)
                mat4.mul(mm, mm, protected.ExtMatrix);
            var VertexData_copy = protected.VertexData.concat();
            for (var i = 0; i < VertexData_copy.length; i += 3) {
                var p = [VertexData_copy[i], VertexData_copy[i + 1], VertexData_copy[i + 2]];
                vec3.transformMat4(p, p, mm);
                VertexData_copy[i] = p[0];
                VertexData_copy[i + 1] = p[1];
                VertexData_copy[i + 2] = p[2];
            }
            //return [VertexData_copy, protected.ElementData];
        };
        this.raypick = function (needDataObj) {
            var ret = this.getVertexData();
            var raypick = new core.math.rayPickGeometry(gl.campos, gl.unprojAngel);
            var pick = raypick.vertex(ret[0], ret[1], [0, 0, 0]);
            if (pick !== undefined && needDataObj !== undefined) {
                if (needDataObj.needIndex === 1)
                    needDataObj.needIndex = raypick.vertex_elementi;
                if (needDataObj.needElement === 1) {
                    var ei = raypick.vertex_elementi;
                    needDataObj.needElement = [ret[1][ei], ret[1][ei + 1], ret[1][ei + 2]];
                }
                if (needDataObj.needPickTexcoord === 1) {
                    var elementi = [ret[1][ei], ret[1][ei + 1], ret[1][ei + 2]];
                    var vd1_index = elementi[0] * 3;
                    var vd2_index = elementi[1] * 3;
                    var vd3_index = elementi[2] * 3;
                    var vd1 = [ret[0][vd1_index], ret[0][vd1_index + 1], ret[0][vd1_index + 2]];
                    var vd2 = [ret[0][vd2_index], ret[0][vd2_index + 1], ret[0][vd2_index + 2]];
                    var vd3 = [ret[0][vd3_index], ret[0][vd3_index + 1], ret[0][vd3_index + 2]];
                    var retuv = core.math.glRayPick_uv(gl.campos, gl.unprojAngel, vd1, vd2, vd3);
                    var texarr = protected.TextureData;
                    var vd1_index = elementi[0] * 2;
                    var vd2_index = elementi[1] * 2;
                    var vd3_index = elementi[2] * 2;
                    var vt1 = [texarr[vd1_index], texarr[vd1_index + 1]];
                    var vt2 = [texarr[vd2_index], texarr[vd2_index + 1]];
                    var vt3 = [texarr[vd3_index], texarr[vd3_index + 1]];
                    vec2.sub(vt2, vt2, vt1);
                    vec2.sub(vt3, vt3, vt1);
                    var uv = vt1;
                    vec3.add(uv, uv, vec3.scale([0, 0, 0], vt2, retuv[0]));
                    vec3.add(uv, uv, vec3.scale([0, 0, 0], vt3, retuv[1]));
                    needDataObj.needPickTexcoord = uv;
                }

            }
            return pick;
        };
        this.setHidden = function (hidden) {
            protected.hidden = hidden;
        };
        this.getHidden = function () {
            return protected.hidden;
        };
        this.delParent = function (saveMatrixToExt) {
            //nincs parent, nincs müvelet, FONTOS mert visszahívható!! (végtelen rekúrzió)
            if (protected.parent === undefined) {
            } else {
                var m;
                if (saveMatrixToExt) {
                    m = protected.parentMatrix;
                }
                var parent = protected.parent;
                protected.parent = undefined;
                parent.delChildren(this);
                if (saveMatrixToExt) {
                    if (protected.ExtMatrix)
                        mat4.mul(m, m, protected.ExtMatrix);
                    protected.ExtMatrix = m;
                }

            }
        };
        this.setParent = function (parentObject, subMatrixFromExt) {

            //nincs művelet, FONTOS!! ez a függvény vissza lehet hívva az addChildren függvényből
            //végtelen rekúrzió miatt ez szükséges
            if (protected.parent === parentObject) {
            } else if (protected.parent === undefined) {
                protected.parent = parentObject;
                parentObject.addChildren(this);

                var m = protected.parentMatrix;
                if (m) {
                    mat4.invert(m, m);
                    mat4.mul(m, m, protected.ExtMatrix);
                    protected.ExtMatrix = m;
                }
            } else { //protected.parent !== parentObject
                this.delParent(subMatrixFromExt);
                this.setParent(parentObject, subMatrixFromExt);
            }

        };
        this.addChildren = function (children, subMatrixFromExt) {
            var id = children.getGeoId();
            if (protected.childrenList[id] === undefined) {
                protected.childrenList[id] = children;
                children.setParent(this, subMatrixFromExt);
                this.sendParentMatrixToChilds();
            }
        };
        this.delChildren = function (children, subMatrixFromExt) {
            var id = children.getGeoId();
            if (protected.childrenList[id] !== undefined) {
                delete protected.childrenList[id];
                children.delParent(subMatrixFromExt);
                children.setParentMatrix(undefined);

            }
        };
        this.getParent = function () {
            return protected.parent;
        };
        this.getChildList = function () {
            var ret = [];
            for (var i in protected.childrenList)
                ret.push(protected.childrenList[i]);
            return ret;
        };
        this.sendParentMatrixToChilds = function () {
            for (var i in protected.childrenList) {
                var c = protected.childrenList[i];
                var m = mat4.create();
                if (protected.ExtMatrix !== undefined)
                    mat4.mul(m, m, protected.ExtMatrix);
                if (protected.parentMatrix !== undefined)
                    mat4.mul(m, m, protected.parentMatrix);
                mat4.translate(m, m, protected.pos);
                var rotmat = mat4.create();
                mat4.fromQuat(rotmat, protected.rotate);
                mat4.mul(m, m, rotmat);

                c.setParentMatrix(m);
            }
        };
        this.getTranslateMatrix = function (outMatrix) {
            mat4.translate(outMatrix, outMatrix, protected.pos);
            var rotmat = mat4.create();
            mat4.fromQuat(rotmat, protected.rotate);
            mat4.mul(outMatrix, outMatrix, rotmat);
            return outMatrix;
        };
        this.setParentMatrix = function (matrix) {
            protected.parentMatrix = matrix;
            this.sendParentMatrixToChilds();
        };
        this.getParentMatrix = function () {
            if (protected.parentMatrix)
                return mat4.clone(protected.parentMatrix);
            return undefined;
        };
        this.getGeometryObject = function () {
            return protected.geometryObject;
        };
        this.resetRotate = function () {
            quat.identity(protected.rotate);
            this.sendParentMatrixToChilds();
        };
        this.addRotateX = function (angle, isdeg) {
            if (isdeg)
                angle = PokolVoxelEngine.prototype.math.degToRad(angle);
            quat.rotateX(protected.rotate, protected.rotate, angle);
            this.sendParentMatrixToChilds();
        };
        this.addRotateY = function (angle, isdeg) {
            if (isdeg)
                angle = PokolVoxelEngine.prototype.math.degToRad(angle);
            quat.rotateY(protected.rotate, protected.rotate, angle);
            this.sendParentMatrixToChilds();
        };
        this.addRotateZ = function (angle, isdeg) {
            if (isdeg)
                angle = PokolVoxelEngine.prototype.math.degToRad(angle);
            quat.rotateZ(protected.rotate, protected.rotate, angle);
            this.sendParentMatrixToChilds();
        };
        this.getQuatRotate = function () {
            return quat.clone(protected.rotate);
        };

        this.getPos = function () {
            return vec3.clone(protected.pos);
        };
        this.setPos = function (pos) {
            vec3.set(protected.pos, pos[0], pos[1], pos[2]);
            this.sendParentMatrixToChilds();
        };
        this.setTexAtlas = function (tex) {
            protected.TextureAtlas = tex;
        };
        this.getTexAtlas = function () {
            return protected.TextureAtlas;
        };
        this.setTexAtlasIndex = function (i) {
            protected.TextureAtlasIndex = i;
        };
        this.setTexture = function (t) {
            protected.Texture = t;
        };
        this.getTexture = function () {
            return protected.Texture;
        };
        this.setTexcoord = function (texcoord) {
            protected.TexcoordPos = vec4.copy([0, 0, 0, 0], texcoord);
        };
        this.getTexcoord = function () {
            return vec4.copy([0, 0, 0, 0], protected.TexcoordPos);
        };
        this.getTexAtlasIndex = function () {
            return protected.TextureAtlasIndex;
        };
        this.setAlpha = function (alpha) {
            protected.AlphaValue = alpha;
        };
        this.getAlpha = function () {
            return protected.AlphaValue;
        };
        this.setExtMatrix = function (m) {
            if (m !== undefined) {
                m = mat4.clone(m);
            }
            protected.ExtMatrix = m;
            this.sendParentMatrixToChilds();
        };
        this.getExtMatrix = function () {
            if (protected.ExtMatrix === undefined) {
                return undefined;
            }
            return mat4.clone(protected.ExtMatrix);
        };
        this.delBuffers = function () {

            /*
             a geometry object attól függ jött, vagy itt hozódott létre
             gl.bufferDecRef(protected.VertexBuff);
             gl.bufferDecRef(protected.NormalBuff);
             gl.bufferDecRef(protected.ElementBuff);
             gl.bufferDecRef(protected.TextureBuff);
             for (var i = 0; i < protected.externalAttributes.length; i++) {
             var atrb = protected.externalAttributes[i];
             if (atrb) {
             gl.bufferDecRef(atrb);
             }
             }
             */
        };

        this.insertExternalAttribute = function (index, atrb) {
            protected.externalAttributes[index] = atrb;
        };
        this.getExternalAttribute = function (index) {
            return protected.externalAttributes[index];
        };
        this.insertExternalUniform = function (index, name) {
            protected.externalUniforms[index] = {name: name, data: undefined};
        };
        this.setExternalUniform = function (index, data) {
            protected.externalUniforms[index].data = data;
        };
        this.getExternalUniform = function (index) {
            return protected.externalUniforms[index];
        };
        this.insertExternalTextures = function (index, name, tex) {
            protected.externalTextures[index] = {name: name, texture: tex};
        };
        this.getExternalTextures = function (index) {
            return protected.externalTextures[index];
        };
        this.setDrawAfterCallback = function (callback, userobj) {
            protected.drawAfterCallback = callback;
            protected.userObj = userobj;
        };
        this.setColor = function(c){
            vec3.set(protected.color, c[0], c[1], c[2]);
        };
        this.setPos(pos);
        return protected;
    }

    this.Geo_Cylinder = function (r1, r2, start, end, vertnum, pos, color) {
        var protected = SimpleSuper.call(this, pos, color);
        var _this = this;
        this.hidden = false;
        var normalData;
        function init() {
            var ret = core.math.geosCylinderData(start, end, r1, r2, vertnum);
            protected.geometryObject.setVertexData(ret[0]);
            protected.geometryObject.setTexcoordData(ret[3]);
            protected.geometryObject.setNormalData(ret[1]);
            protected.geometryObject.setElementData(ret[2]);
        }

        this.getR1 = function () {
            return r1;
        };
        this.getR2 = function () {
            return r2;
        };
        this.getEnd = function () {
            return end;
        };
        this.getStart = function () {
            return start;
        };

        init();
    };

    this.Geo_Cylinder.prototype = Object.create(SimpleSuper.prototype);
    this.Geo_Cylinder.prototype.constructor = this.Geo_Cylinder;


    this.Geo_AxisArrow = function (arrowtype, size, vertnump, pos, color) {
        var protected = SimpleSuper.call(this, undefined, pos, color);
        var _this = this;
        this.hidden = false;
        if (arrowtype === core.geos.Geo_AxisArrow.XArrow) {
            //_this.setObjRotate(new core.objRotate(core.objRotate.YROT, [90, 90, 0], true));
        }
        if (arrowtype === core.geos.Geo_AxisArrow.ZArrow) {
            //_this.setObjRotate(new core.objRotate(core.objRotate.YROT, [90, 0, 0], true));
        }
        function init() {
            var bonelist = [
                new core.geos.BoneItemObj([0, 0, 0], [0, 0]),
                new core.geos.BoneItemObj([0, 0, 0], [1 * size, 1 * size]),
                new core.geos.BoneItemObj([0, 10 * size, 0], [1 * size, 1 * size]),
                new core.geos.BoneItemObj([0, 10 * size, 0], [1.5 * size, 1.5 * size]),
                new core.geos.BoneItemObj([0, 15 * size, 0], [0, 0])
            ];
            var ret = core.math.geosBoneData(vertnump, bonelist);
            protected.geometryObject.setVertexData(ret[0]);
            protected.geometryObject.setTexcoordData(ret[3]);
            protected.geometryObject.setNormalData(ret[1]);
            protected.geometryObject.setElementData(ret[2]);

        }

        this.setSize = function (sizep) {
            size = sizep;
            init();
        };
        this.startMouseMove = function () {
            this.startpos = undefined;
        };
        this.mouseMove = function () {
            var axisraypick = new core.math.rayPickGeometry(gl.campos, gl.unprojAngel, true);
            var p0 = this.getPos();
            var v0;
            var v1;

            if (arrowtype === core.geos.Geo_AxisArrow.XArrow) {
                v0 = [0, 1, 0];
                v1 = [1, 0, 0];
            } else if (arrowtype === core.geos.Geo_AxisArrow.YArrow) {
                v0 = [0, 1, 0];
                v1 = [1, 0, 0];
            } else {
                v0 = [0, 1, 0];
                v1 = [0, 0, 1];
            }

            var p1 = vec3.add([0, 0, 0], v0, p0);
            var p2 = vec3.add([0, 0, 0], v1, p0);
            var rp = axisraypick.tri(p0, p1, p2);
            if (rp) {
                if (this.startpos === undefined) {
                    this.startpos = vec3.sub([0, 0, 0], rp, p0);
                } else {
                    vec3.sub(rp, rp, this.startpos);
                    if (arrowtype === core.geos.Geo_AxisArrow.XArrow) {
                        p0[0] = rp[0];
                    } else if (arrowtype === core.geos.Geo_AxisArrow.YArrow) {
                        p0[1] = rp[1];
                    } else {
                        p0[2] = rp[2];
                    }
                }
                return p0;
            }
            return undefined;
        };
        init();

    };
    this.Geo_AxisArrow.prototype = Object.create(SimpleSuper.prototype);
    this.Geo_AxisArrow.prototype.constructor = this.Geo_AxisArrow;

    this.Geo_AxisArrow.XArrow = 0;
    this.Geo_AxisArrow.YArrow = 1;
    this.Geo_AxisArrow.ZArrow = 2;

    this.Geo_Triangle = function (v0, v1, pos, color) {
        var protected = SimpleSuper.call(this, undefined, pos, color);
        var _this = this;
        this.hidden = false;
        function init() {

            protected.geometryObject.setVertexData([0, 0, 0, v0[0], v0[1], v0[2], v1[0], v1[1], v1[2]]);
            protected.geometryObject.setTexcoordData([0, 0, 1, 0, 0, 1]);
            protected.geometryObject.setNormalData([0, 1, 0, 0, 1, 0, 0, 1, 0]);
            protected.geometryObject.setElementData([0, 1, 2]);
        }
        init();
        this.getVertices = function ()
        {
            var vv0 = vec3.add([0, 0, 0], v0, pos);
            var vv1 = vec3.add([0, 0, 0], v1, pos);
            return [[pos[0], pos[1], pos[2]], vv0, vv1];
        };
    };
    this.Geo_Triangle.prototype = Object.create(SimpleSuper.prototype);
    this.Geo_Triangle.prototype.constructor = this.Geo_Triangle;




    this.Geo_Free = function (drawMode, geometryObject, pos, color) {
        var protected = SimpleSuper.call(this, geometryObject, pos, color);
        var _this = this;
        this.hidden = false;
        protected.DrawMode = drawMode;
    };
    this.Geo_Free.prototype = Object.create(SimpleSuper.prototype);
    this.Geo_Free.prototype.constructor = this.Geo_Free;

    this.Geo_Free.Line = gl.LINES;
    this.Geo_Free.Triangle = gl.TRIANGLES;

    this.Geo_Sphere = function (meret, pos, color) {
        var protected = SimpleSuper.call(this, undefined, pos, color);
        var _this = this;
        this.adv = true;
        this.hidden = false;
        this.color;
        function init() {
            var ret = core.math.geosSphereData(meret, 10);
            protected.geometryObject.setVertexData(ret[0]);
            protected.geometryObject.setTexcoordData(ret[3]);
            protected.geometryObject.setNormalData(ret[1]);
            protected.geometryObject.setElementData(ret[2]);
        }
        init();
    };
    this.Geo_Sphere.prototype = Object.create(SimpleSuper.prototype);
    this.Geo_Sphere.prototype.constructor = this.Geo_Sphere;

    this.Geo_Line = function (meret, pos, color) {
        var protected = SimpleSuper.call(this, undefined, pos, color);
        var _this = this;
        this.hidden = false;
        protected.DrawMode = gl.LINES;
        this.color;

        function init() {
            var VertexData = [];
            var NormalData = [];
            var TexcoordData = [];
            var ElementData = [];
            VertexData.push(0, 0, 0);
            VertexData.push(meret[0], meret[1], meret[2]);
            NormalData.push(1, 1, 1);
            NormalData.push(1, 1, 1);
            TexcoordData.push(0, 0);
            TexcoordData.push(0, 0);
            ElementData.push(0, 1);
            protected.geometryObject.setVertexData(VertexData);
            protected.geometryObject.setTexcoordData(TexcoordData);
            protected.geometryObject.setNormalData(NormalData);
            protected.geometryObject.setElementData(ElementData);
        }
        this.setMeret = function (m) {
            meret = m;
            var VertexData = [];
            VertexData.push(0, 0, 0);
            VertexData.push(meret[0], meret[1], meret[2]);
            protected.geometryObject.setVertexData(VertexData);

        };
        this.getMeret = function () {
            return [meret[0], meret[1], meret[2]];
        };
        this.setPosTo = function (posto) {
            var p = protected.pos;
            this.setMeret([posto[0] - p[0], posto[1] - p[1], posto[2] - p[2]]);
        };
        this.getPosTo = function () {
            var p = protected.pos;
            return [meret[0] + p[0], meret[1] + p[1], meret[2] + p[2]];
        };
        init();
    };
    this.Geo_Line.prototype = Object.create(SimpleSuper.prototype);
    this.Geo_Line.prototype.constructor = this.Geo_Line;



    this.Geo_Box = function (meret, pos, color) {
        var protected = SimpleSuper.call(this, undefined, pos, color);
        var _this = this;
        this.hidden = false;
        this.color;
        function init() {
            var ret = core.math.geosBoxData(meret);
            protected.geometryObject.setVertexData(ret[0]);
            protected.geometryObject.setTexcoordData(ret[3]);
            protected.geometryObject.setNormalData(ret[1]);
            protected.geometryObject.setElementData(ret[2]);
        }
        init();
    };
    this.Geo_Box.prototype = Object.create(SimpleSuper.prototype);
    this.Geo_Box.prototype.constructor = this.Geo_Box;


    this.Geo_Feny = function (meret, pos, color) {
        var protected = SimpleSuper.call(this, undefined, pos, color);
        var _this = this;
        this.hidden = false;
        this.color;
        function init() {
            var ret = core.math.geosFenyData(meret);
            protected.geometryObject.setVertexData(ret[0]);
            protected.geometryObject.setTexcoordData(ret[3]);
            protected.geometryObject.setNormalData(ret[1]);
            protected.geometryObject.setElementData(ret[2]);
        }
        init();
    };
    this.Geo_Feny.prototype = Object.create(SimpleSuper.prototype);
    this.Geo_Feny.prototype.constructor = this.Geo_Feny;

    var loadShader = function () {
        var fragment =
                "uniform sampler2D uTex; \n" +
                "uniform highp vec3 uSzin; \n" +
                "uniform int uTexUse; \n" +
                "uniform highp float uAlpha; \n" +
                "varying highp vec3 vFeny; \n" +
                "varying highp vec2 vTexcoord; \n" +
                "void main(void) { \n" +
                "   highp vec4 texcolor = vec4(1.0); \n" +
                "   if(uTexUse == 1){\n" +
                "       texcolor = texture2D(uTex, vTexcoord); \n" +
                "   }\n" +
                "   gl_FragColor = vec4(texcolor.rgb * uSzin * vFeny,uAlpha); \n" +
                "}";
        var vertex =
                "attribute vec3 aVertexPosition; \n" +
                "attribute vec3 aNormal; \n" +
                "attribute vec2 aTexture; \n" +
                "uniform mat4 uMVMatrix; \n" +
                "uniform mat4 uCMatrix; \n" +
                "uniform mat4 uPMatrix; \n" +
                "uniform vec3 uFenySzin[6]; \n" +
                "varying highp vec3 vFeny; \n" +
                "varying highp vec2 vTexcoord; \n" +
                "highp float osszFeny; \n" +
                "vec3 transformedNormal; \n" +
                "void addFeny(highp vec3 normal, highp vec3 szin){ \n" +
                "   highp float ero = max(dot(transformedNormal, normal), 0.0); \n" +
                "   vFeny += szin * ero; \n" +
                "   osszFeny += ero; \n" +
                "} \n" +
                "void main(void) { \n" +
                "   gl_Position = uPMatrix * (uCMatrix * uMVMatrix) * vec4(aVertexPosition, 1.0); \n" +
                "   mat3 uMVMatrix3 = mat3(uMVMatrix); osszFeny = 0.0; vFeny = vec3(0.0);\n" +
                "   transformedNormal = uMVMatrix3 * aNormal; \n" +
                "   addFeny(vec3( 1.0,0.0,0.0),uFenySzin[0]); \n" +
                "   addFeny(vec3(-1.0,0.0,0.0),uFenySzin[1]); \n" +
                "   addFeny(vec3(0.0, 1.0,0.0),uFenySzin[2]); \n" +
                "   addFeny(vec3(0.0,-1.0,0.0),uFenySzin[3]); \n" +
                "   addFeny(vec3(0.0,0.0, 1.0),uFenySzin[4]); \n" +
                "   addFeny(vec3(0.0,0.0,-1.0),uFenySzin[5]); \n" +
                "   vFeny /= osszFeny; \n" +
                "   vTexcoord = aTexture; \n" +
                "}";
        shaderObj = core.shader.loadShader(fragment, vertex, "geos", []);
    };
    loadShader();
}
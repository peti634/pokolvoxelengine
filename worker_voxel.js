importScripts("worker_funcs.js");

var g_showPerf = false;
/*
 * Később:
 * - NapfenyCalc->AddAlforras:  > jó, de lassú, >= amikor a napfény kint van a keretből, akkor egyből elhal
 * - teljes kód kimérése, mi kerül sokba? ott lehetőség szerint optimalizáláni, és fények átlósan is terjedhetnek? teszt
 * - 2 terrain hozzáadásakor a korábbinál nem fognak eltünni a másik oldalában lévő blokkok az új terrain bekerülésekor a régi terrain szélét is le kellene frissíteni
 * - sarok fény javítás
 */

/* global self*/
/* global Float32Array*/
/* global Uint32Array*/
//GLOBALS
var g_mxz = 0;
var g_napfenyrange = 0;
var g_pontfeny_maxrange = 0;
var g_oldalfenyarr = [0, 0, 0, 0, 0, 0];
var g_blocktypelist;
var g_csoplist;
var g_atlas_bitlen = 8;
var g_blocksize;

var mer_osszpontf_upd = 0;


var INT32MIN = -0x80000000;

var wtlist = [];
var wtlist_index = [];

function delpixeldata(texdata, texpos_arr) {
    for (var i = 0; i < texpos_arr.length; i += 4)
        texdata.delTexPos(texpos_arr.subarray(i, i + 4));
}


function addpixeldata(texdata, oldalpos, oldali, vertex, blockdata, texpos)
{
    var mx = blockdata.mxz;
    var my = blockdata.my;
    texpos.arr = new Uint16Array(vertex.length * 4);
    for (var j = 0; j < vertex.length; j++) {

        var pos = vertex[j].pos;
        var pxk = pos[0];
        var pyk = pos[1];
        var pxv = pos[2] + 1;
        var pyv = pos[3] + 1;

        var vertexm_x = (pxv - pxk);
        var vertexm_y = (pyv - pyk);

        var pixelarr = new Uint32Array(vertexm_x * vertexm_y);

        this.getElem = function (x, y, z) {
            return blockdata.getElem_noy(x, y, z);
        };
        this.setPixel = function (x, y, er, eg, eb, ea) {
            var szin = (er & 0xFF) + ((eg & 0xFF) << 8) + ((eb & 0xFF) << 16) + ((ea & 0xFF) << 24);
            pixelarr[x + (y * vertexm_x)] = szin;
        };


        switch (oldali) {
            case 0:
            case 1:
                var index_x = oldalpos;
                var index_y = pyk;
                var index_z = pxk;

                for (var ix = 0; ix < vertexm_x; ix++) {
                    for (var iz = 0; iz < vertexm_y; iz++) {

                        var bpos = [index_x, index_y + iz, index_z + ix];

                        var elemid = this.getElem(bpos[0], bpos[1], bpos[2]);
                        var elem = g_blocktypelist[elemid];
                        var elemx = elem.taxatlas_index % g_atlas_bitlen;
                        var elemy = (elem.taxatlas_index / g_atlas_bitlen) | 0;
                        this.setPixel(ix, iz, elemx, elemy, 0, 0);

                    }
                }
                break;

            case 2:
            case 3:
                var index_x = pxk;
                var index_y = oldalpos;
                var index_z = pyk;

                for (var ix = 0; ix < vertexm_x; ix++) {
                    for (var iz = 0; iz < vertexm_y; iz++) {
                        var bpos = [index_x + ix, index_y, index_z + iz];

                        var elemid = this.getElem(bpos[0], bpos[1], bpos[2]);
                        var elem = g_blocktypelist[elemid];
                        var elemx = elem.taxatlas_index % g_atlas_bitlen;
                        var elemy = (elem.taxatlas_index / g_atlas_bitlen) | 0;
                        this.setPixel(ix, iz, elemx, elemy, 0, 0);

                    }
                }
                break;
            case 4:
            case 5:
                var index_x = pxk;
                var index_y = pyk;
                var index_z = oldalpos;

                for (var ix = 0; ix < vertexm_x; ix++) {
                    for (var iz = 0; iz < vertexm_y; iz++) {
                        var bpos = [index_x + ix, index_y + iz, index_z];

                        var elemid = this.getElem(bpos[0], bpos[1], bpos[2]);
                        var elem = g_blocktypelist[elemid];
                        var elemx = elem.taxatlas_index % g_atlas_bitlen;
                        var elemy = (elem.taxatlas_index / g_atlas_bitlen) | 0;
                        this.setPixel(ix, iz, elemx, elemy, 0, 0);

                    }
                }
                break;
        }

        var ret = texdata.getTexPos(vertexm_x, vertexm_y);
        texdata.setData(vertexm_x, vertexm_y, ret, pixelarr);
        texpos.arr[j * 4 + 0] = ret[0];
        texpos.arr[j * 4 + 1] = ret[1];
        texpos.arr[j * 4 + 2] = vertexm_x;
        texpos.arr[j * 4 + 3] = vertexm_y;

    }
}


function FullFenyUpd(wt) {
    if (g_showPerf)
        console.info("FullFenyUpd");
    var wtx = wt.px * g_mxz;
    var wtz = wt.pz * g_mxz;
    for (var x = 0; x < g_mxz; x++) {
        for (var z = 0; z < g_mxz; z++) {
            for (var y = wt.fullfenydata.ypos; y < (wt.fullfenydata.my + wt.fullfenydata.ypos); y++) {
                var bpos = [x + wtx, y, z + wtz];

                var afeny = [0, 0, 0];


                afeny[0] = Math.min(wt.pontfeny_r_data.getElem(x, y, z), 255);
                afeny[1] = Math.min(wt.pontfeny_g_data.getElem(x, y, z), 255);
                afeny[2] = Math.min(wt.pontfeny_b_data.getElem(x, y, z), 255);

                var napfeny = (wt.napfenydata.getElem(x, y, z) / g_napfenyrange) * 255;
                if (napfeny === 0)
                    napfeny = 1;

                var fullfeny = afeny[0] + (afeny[1] << 8) + (afeny[2] << 16) + ((napfeny & 0xFF) << 24);

                wt.fullfenydata.setElem(x, y, z, fullfeny);




            }
        }


    }

}

function SetFeny(gx, gy, gz, texdata, oldalpos, oldali, vertex, texpos) {


    for (var j = 0; j < vertex.length; j++) {

        var pos = vertex[j].pos;
        //a valós tömb körül még 1 érték keret lesz (pos-1, méret +1+1)
        var pxk = pos[0] - 1;
        var pyk = pos[1] - 1;

        var pixelposx = texpos[j * 4 + 0] - 1;
        var pixelposy = texpos[j * 4 + 1] - 1;

        var vertexm_x = texpos[j * 4 + 2] + 2;
        var vertexm_y = texpos[j * 4 + 3] + 2;

        var fenyarr = new Uint32Array(vertexm_x * vertexm_y);

        var oldalTol = getOldalPos([0, 0, 0], oldali);

        switch (oldali) {
            case 0:
            case 1:
                var index_x = oldalpos + oldalTol[0];
                var index_y = pyk + oldalTol[1];
                var index_z = pxk + oldalTol[2];
                //optimalizálható
                for (var ix = 0; ix < vertexm_x; ix++) {
                    for (var iz = 0; iz < vertexm_y; iz++) {
                        var bpos = [index_x + gx, index_y + iz + gy, index_z + ix + gz];

                        fenyarr[ix + (iz * vertexm_x)] = GetFullfenyGlobal(bpos[0], bpos[1], bpos[2]);
                    }
                }
                break;

            case 2:
            case 3:
                var index_x = pxk + oldalTol[0];
                var index_y = oldalpos + oldalTol[1];
                var index_z = pyk + oldalTol[2];
                for (var ix = 0; ix < vertexm_x; ix++) {
                    for (var iz = 0; iz < vertexm_y; iz++) {
                        var bpos = [index_x + ix + gx, index_y, index_z + iz + gz];

                        fenyarr[ix + (iz * vertexm_x)] = GetFullfenyGlobal(bpos[0], bpos[1], bpos[2]);
                    }
                }
                break;
            case 4:
            case 5:
                var index_x = pxk + oldalTol[0];
                var index_y = pyk + oldalTol[1];
                var index_z = oldalpos + oldalTol[2];
                for (var ix = 0; ix < vertexm_x; ix++) {
                    for (var iz = 0; iz < vertexm_y; iz++) {
                        var bpos = [index_x + ix + gx, index_y + iz + gy, index_z + gz];
                        fenyarr[ix + (iz * vertexm_x)] = GetFullfenyGlobal(bpos[0], bpos[1], bpos[2]);
                    }
                }
                break;
        }
        texdata.setFeny(vertexm_x, vertexm_y, [pixelposx, pixelposy], fenyarr);
    }

}

function VoxelWorker(px, pz) {
    this.px = px;
    this.pz = pz;
    this.blockdata;
    this.napfenydata;
    this.fullfenydata;
    this.pontfeny_r_data;
    this.pontfeny_g_data;
    this.pontfeny_b_data;
    this.my = 0;
    this.ypos = 0;
    this.texdata = new textureData(2);
    this.oszlopmax; //napfény max
    this.pontfenylist;
    this.oldalLapList = [[], [], [], [], [], []];
    this.needFenyUpd;

    this.oldalLap = function () {
        this.upd = false;
        this.vertex = undefined;
        this.texpos = [];
    };
    this.init = function () {
        this.oldalLapList = [[], [], [], [], [], []];
        this.pontfenylist = [];
        this.my = 0;
        this.ypos = 0;
        this.texdata.clean();

        this.pontfeny_r_data = new dinamic_3d_array(g_mxz, Uint32Array);
        this.pontfeny_g_data = new dinamic_3d_array(g_mxz, Uint32Array);
        this.pontfeny_b_data = new dinamic_3d_array(g_mxz, Uint32Array);


        this.fullfenydata = new dinamic_3d_array(g_mxz, Uint32Array);//pontfény, és napfény már kiszámolva
        this.blockdata = new dinamic_3d_array(g_mxz, Uint16Array);
        this.napfenydata = new dinamic_3d_array(g_mxz, Uint16Array);
        this.oszlopmax = new Int32Array(g_mxz * g_mxz);
        this.needFenyUpd = false;
        for (var i = 0; i < this.oszlopmax.length; i++)
            this.oszlopmax[i] = INT32MIN;
    };

    this.addPontFeny = function (f) {
        this.pontfenylist.push(f);
    };
    this.delPontFeny = function (pos) {
        for (var i = 0; i < this.pontfenylist.length; i++) {
            var f = this.pontfenylist[i];
            if (f.fenyp[0] === pos[0] && f.fenyp[1] === pos[1] && f.fenyp[2] === pos[2]) {
                this.pontfenylist.splice(i, 1);
                return f;
            }
        }
        return undefined;
    };

    this.getOldalLap = function (oldal, pos) {

        if (oldal === 2 || oldal === 3) {
            if (pos >= this.my)
                return;
        } else {
            if (pos >= g_mxz)
                return;
            if (pos < 0)
                return;
        }
        if (this.oldalLapList[oldal][pos] === undefined) {
            this.oldalLapList[oldal][pos] = new this.oldalLap();
        }
        return this.oldalLapList[oldal][pos];
    };


    this.updateLap = function (x, y, z) {

        this.lapupd = function (oldal, pos) {
            var lap = this.getOldalLap(oldal, pos);
            if (lap)
                lap.upd = true;
        };

        this.lapupd(0, x);
        this.lapupd(0, x + 1);
        this.lapupd(1, x);
        this.lapupd(1, x - 1);


        this.lapupd(2, y);
        this.lapupd(2, y + 1);
        this.lapupd(3, y);
        this.lapupd(3, y - 1);


        this.lapupd(4, z);
        this.lapupd(4, z + 1);
        this.lapupd(5, z);
        this.lapupd(5, z - 1);
    };

    this.addElem = function (x, y, z, tipus) {
        this.pontfeny_r_data.y_teszt(y);
        this.pontfeny_g_data.y_teszt(y);
        this.pontfeny_b_data.y_teszt(y);

        this.fullfenydata.y_teszt(y);
        this.blockdata.y_teszt(y);
        dinamic_3d_array_napfeny_yteszt(this.napfenydata, y, g_napfenyrange);
        this.my = this.blockdata.my;
        this.ypos = this.blockdata.ypos;
        this.updateLap(x, y, z);

        this.blockdata.setElem(x, y, z, tipus);

    };


    this.delElem = function (x, y, z) {
        this.updateLap(x, y, z);
        this.blockdata.setElem(x, y, z, 0);

    };
    this.getElem = function (x, y, z) {
        return this.blockdata.getElem(x, y, z);
    };


    this.update = function () {

        var upd_list = [];
        var updcount = 0;

        for (var i = 0; i < g_mxz; i++) {

            var o_m1 = this.oldalLapList[0][i];
            var o_p1 = this.oldalLapList[1][i];

            if ((o_m1 === undefined || !o_m1.upd) && (o_p1 === undefined || !o_p1.upd)) {
                continue;
            }
            var obj = {id: i, oldal: 0, m1: false, p1: false};
            if (o_m1 !== undefined && o_m1.upd) {
                obj.m1 = true;
                obj.m1_texpos = o_m1.texpos;
                o_m1.upd = false;
                updcount++;
            }
            if (o_p1 !== undefined && o_p1.upd) {
                obj.p1 = true;
                obj.p1_texpos = o_p1.texpos;
                o_p1.upd = false;
                updcount++;
            }
            upd_list.push(obj);
        }

        for (var i = this.ypos; i < this.my; i++) {

            var o_m1 = this.oldalLapList[2][i];
            var o_p1 = this.oldalLapList[3][i];

            if ((o_m1 === undefined || !o_m1.upd) && (o_p1 === undefined || !o_p1.upd)) {
                continue;
            }
            var obj = {id: i - this.ypos, oldal: 1, m1: false, p1: false};
            if (o_m1 !== undefined && o_m1.upd) {
                obj.m1 = true;
                obj.m1_texpos = o_m1.texpos;
                o_m1.upd = false;
                updcount++;
            }
            if (o_p1 !== undefined && o_p1.upd) {
                obj.p1 = true;
                obj.p1_texpos = o_p1.texpos;
                o_p1.upd = false;
                updcount++;
            }
            upd_list.push(obj);
        }

        for (var i = 0; i < g_mxz; i++) {

            var o_m1 = this.oldalLapList[4][i];
            var o_p1 = this.oldalLapList[5][i];

            if ((o_m1 === undefined || !o_m1.upd) && (o_p1 === undefined || !o_p1.upd)) {
                continue;
            }
            var obj = {id: i, oldal: 2, m1: false, p1: false};
            if (o_m1 !== undefined && o_m1.upd) {
                obj.m1 = true;
                obj.m1_texpos = o_m1.texpos;
                o_m1.upd = false;
                updcount++;
            }
            if (o_p1 !== undefined && o_p1.upd) {
                obj.p1 = true;
                obj.p1_texpos = o_p1.texpos;
                o_p1.upd = false;
                updcount++;
            }
            upd_list.push(obj);
        }
        return upd_list;

    };


    this.update_ok = function () {

        var INC_STEP = 1000000;
        var VertexIndexList = [];
        this.needVertexIndex = function (csop) {
            if (VertexIndexList[csop] === undefined) {
                VertexIndexList[csop] = new dinamicArray(INC_STEP, Uint32Array);
            }
        };
        var VertexData = new dinamicArray(INC_STEP, Float32Array);
        var VertexTexcoord = new dinamicArray(INC_STEP, Float32Array);

        var vertexindex = 0;

        for (var i = 0; i < this.oldalLapList.length; i++) {

            var oldalfeny = g_oldalfenyarr[i];


            var vertarr = this.oldalLapList[i];
            var oldal = (i / 2) | 0;
            var oldalindex = i % 2 === ((oldal === 2) ? 0 : 1);
            for (var k = this.ypos; k < vertarr.length; k++) {
                var vert = vertarr[k];
                if (vert === undefined || vert.upd === true || vert.vertex === undefined)
                    continue;
                var need_vertexdata = false;

                if (vert.VertexData === undefined) {
                    vert.VertexData = new Float32Array(vert.vertex.length * 4 * 4);
                    vert.VertexTexcoord = new Float32Array(vert.vertex.length * 4 * 4);
                    need_vertexdata = true;
                }

                var VertexDataIndex = 0;
                var VertexTexcoordIndex = 0;
                for (var j = 0; j < vert.vertex.length; j++) {

                    if (need_vertexdata) {
                        var py = k;
                        if (i % 2 === 1)
                            py++;
                        var pos = vert.vertex[j].pos;

                        var pxk = pos[0];
                        var pyk = pos[1];
                        var pxv = pos[2] + 1;
                        var pyv = pos[3] + 1;


                        var VertexErr = 0.0001;
                        if (oldal === 0) {
                            pyk += this.ypos;
                            pyv += this.ypos;
                            vert.VertexData[VertexDataIndex + 0] = py;
                            vert.VertexData[VertexDataIndex + 1] = pyk - VertexErr;
                            vert.VertexData[VertexDataIndex + 2] = pxk - VertexErr;
                            vert.VertexData[VertexDataIndex + 4] = py;
                            vert.VertexData[VertexDataIndex + 5] = pyk - VertexErr;
                            vert.VertexData[VertexDataIndex + 6] = pxv + VertexErr;
                            vert.VertexData[VertexDataIndex + 8] = py;
                            vert.VertexData[VertexDataIndex + 9] = pyv + VertexErr;
                            vert.VertexData[VertexDataIndex + 10] = pxk - VertexErr;
                            vert.VertexData[VertexDataIndex + 12] = py;
                            vert.VertexData[VertexDataIndex + 13] = pyv + VertexErr;
                            vert.VertexData[VertexDataIndex + 14] = pxv + VertexErr;
                        } else if (oldal === 1) {
                            vert.VertexData[VertexDataIndex + 0] = pxk - VertexErr;
                            vert.VertexData[VertexDataIndex + 1] = py;
                            vert.VertexData[VertexDataIndex + 2] = pyk - VertexErr;
                            vert.VertexData[VertexDataIndex + 4] = pxv + VertexErr;
                            vert.VertexData[VertexDataIndex + 5] = py;
                            vert.VertexData[VertexDataIndex + 6] = pyk - VertexErr;
                            vert.VertexData[VertexDataIndex + 8] = pxk - VertexErr;
                            vert.VertexData[VertexDataIndex + 9] = py;
                            vert.VertexData[VertexDataIndex + 10] = pyv + VertexErr;
                            vert.VertexData[VertexDataIndex + 12] = pxv + VertexErr;
                            vert.VertexData[VertexDataIndex + 13] = py;
                            vert.VertexData[VertexDataIndex + 14] = pyv + VertexErr;
                        } else if (oldal === 2) {
                            pyk += this.ypos;
                            pyv += this.ypos;

                            vert.VertexData[VertexDataIndex + 0] = pxk - VertexErr;
                            vert.VertexData[VertexDataIndex + 1] = pyk - VertexErr;
                            vert.VertexData[VertexDataIndex + 2] = py;
                            vert.VertexData[VertexDataIndex + 4] = pxv + VertexErr;
                            vert.VertexData[VertexDataIndex + 5] = pyk - VertexErr;
                            vert.VertexData[VertexDataIndex + 6] = py;
                            vert.VertexData[VertexDataIndex + 8] = pxk - VertexErr;
                            vert.VertexData[VertexDataIndex + 9] = pyv + VertexErr;
                            vert.VertexData[VertexDataIndex + 10] = py;
                            vert.VertexData[VertexDataIndex + 12] = pxv + VertexErr;
                            vert.VertexData[VertexDataIndex + 13] = pyv + VertexErr;
                            vert.VertexData[VertexDataIndex + 14] = py;

                        }

                        for (var im = 0; im < 16; im++) {
                            vert.VertexData[im + VertexDataIndex] *= g_blocksize;
                        }


                        vert.VertexData[VertexDataIndex + 3] = oldalfeny;
                        vert.VertexData[VertexDataIndex + 7] = oldalfeny;
                        vert.VertexData[VertexDataIndex + 11] = oldalfeny;
                        vert.VertexData[VertexDataIndex + 15] = oldalfeny;

                        VertexDataIndex += 16;


                        var texpos_x = vert.texpos[j * 4];
                        var texpos_y = vert.texpos[j * 4 + 1];
                        var texm_x = vert.texpos[j * 4 + 2];
                        var texm_y = vert.texpos[j * 4 + 3];

                        vert.VertexTexcoord[VertexTexcoordIndex + 0] = texpos_x;
                        vert.VertexTexcoord[VertexTexcoordIndex + 1] = texpos_y;
                        vert.VertexTexcoord[VertexTexcoordIndex + 2] = pxk;
                        vert.VertexTexcoord[VertexTexcoordIndex + 3] = pyk;

                        vert.VertexTexcoord[VertexTexcoordIndex + 4] = texpos_x + texm_x;
                        vert.VertexTexcoord[VertexTexcoordIndex + 5] = texpos_y;
                        vert.VertexTexcoord[VertexTexcoordIndex + 6] = pxk + texm_x;
                        vert.VertexTexcoord[VertexTexcoordIndex + 7] = pyk;

                        vert.VertexTexcoord[VertexTexcoordIndex + 8] = texpos_x;
                        vert.VertexTexcoord[VertexTexcoordIndex + 9] = texpos_y + texm_y;
                        vert.VertexTexcoord[VertexTexcoordIndex + 10] = pxk;
                        vert.VertexTexcoord[VertexTexcoordIndex + 11] = pyk + texm_y;

                        vert.VertexTexcoord[VertexTexcoordIndex + 12] = texpos_x + texm_x;
                        vert.VertexTexcoord[VertexTexcoordIndex + 13] = texpos_y + texm_y;
                        vert.VertexTexcoord[VertexTexcoordIndex + 14] = pxk + texm_x;
                        vert.VertexTexcoord[VertexTexcoordIndex + 15] = pyk + texm_y;
                        VertexTexcoordIndex += 16;
                    }



                    var csop = vert.vertex[j].pos[4];
                    this.needVertexIndex(csop);
                    var vertexindexelem = VertexIndexList[csop];
                    vertexindexelem.need(6);
                    if (oldalindex) {

                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 1;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 0;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 2;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 1;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 2;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 3;

                    } else {
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 0;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 1;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 2;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 1;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 3;
                        vertexindexelem.buffer[vertexindexelem.pos++] = vertexindex + 2;

                    }
                    vertexindex += 4;

                }

                VertexData.need(vert.VertexData.length);
                VertexData.buffer.set(vert.VertexData, VertexData.pos);
                VertexData.pos += vert.VertexData.length;

                VertexTexcoord.need(vert.VertexTexcoord.length);


                VertexTexcoord.buffer.set(vert.VertexTexcoord, VertexTexcoord.pos);
                VertexTexcoord.pos += vert.VertexTexcoord.length;

            }
        }

        for (var i = 0; i < VertexTexcoord.buffer.length; i += 4) {
            VertexTexcoord.buffer[i + 0] /= this.texdata.texsize_x;
            VertexTexcoord.buffer[i + 1] /= this.texdata.texsize_y;
        }


        var sendmsg = {type: "DATA"};
        sendmsg.vertdata = VertexData.buffer.subarray(0, VertexData.pos);
        sendmsg.fenydata = this.texdata.getFenyData();
        sendmsg.texcoord = VertexTexcoord.buffer.subarray(0, VertexTexcoord.pos);

        var fulllen = 0;
        for (var csi in VertexIndexList) {
            var vertexindexelem = VertexIndexList[csi];
            if (vertexindexelem) {
                fulllen += vertexindexelem.pos;
            }
        }
        var fullindex = new Uint32Array(fulllen);
        var indexsetpos = 0;
        var csoportindexpos = [];
        for (var csi in VertexIndexList) {
            var vertexindexelem = VertexIndexList[csi];
            if (vertexindexelem) {
                var pos = vertexindexelem.pos;
                csoportindexpos[csi] = [indexsetpos, pos];

                var vbuff = vertexindexelem.buffer.subarray(0, vertexindexelem.pos);
                fullindex.set(vbuff, indexsetpos);
                indexsetpos += pos;
            }
        }
        sendmsg.csoportindexpos = csoportindexpos;

        sendmsg.vertindex = fullindex.subarray(0, indexsetpos);

        sendmsg.texdata = this.texdata.getPixelData();
        sendmsg.texdatax = this.texdata.texsize_x;
        sendmsg.texdatay = this.texdata.texsize_y;
        sendmsg.px = this.px;
        sendmsg.pz = this.pz;
        self.postMessage(sendmsg);


        //this.SendFenyData();

    };
    this.UpdFenyData = function (nosend) {
        for (var i = 0; i < this.oldalLapList.length; i++) {
            var vertarr = this.oldalLapList[i];
            for (var k = this.ypos; k < vertarr.length; k++) {
                var laparr = vertarr[k];
                if (laparr === undefined || laparr.vertex === undefined)
                    continue;
                SetFeny(this.px * g_mxz, this.ypos, this.pz * g_mxz, this.texdata, k, i, laparr.vertex, laparr.texpos);

            }
        }
        if (nosend === true) {
            var sendmsg = {type: "FENYDATA"};
            sendmsg.fenydata = this.texdata.getFenyData();
            sendmsg.texfenyx = this.texdata.texsize_x;
            sendmsg.texfenyy = this.texdata.texsize_y;
            sendmsg.px = this.px;
            sendmsg.pz = this.pz;
            self.postMessage(sendmsg);
        }
    };
    this.getMaxOszlop = function (x, z) {
        if (x < 0 || z < 0 || x >= g_mxz || z >= g_mxz)
            return INT32MIN;
        return this.oszlopmax[x + (z * g_mxz)];
    };

    this.oszlopUpdate = function (oszloparr) {
        for (var i = 0; i < oszloparr.length; i++) {
            if (oszloparr[i] === 1) {
                var x = i % g_mxz;
                var z = (i / g_mxz) | 0;
                var maxy = INT32MIN;
                for (var y = (this.my + this.ypos); y >= this.ypos; y--) {
                    var elem = this.getElem(x, y, z);
                    if (elem !== 0) {
                        var type = GetBlockType(elem);
                        if (type.tul.szabadfeny === 1)
                            elem = 0;
                    }
                    if (elem !== 0) {
                        maxy = y + 1; //ahol blokk van, a felé lesz napfény
                        break;
                    }
                }


                this.oszlopmax[i] = maxy;
                if (maxy === INT32MIN) {
                    maxy = this.napfenydata.ypos;
                }
                for (var j = maxy; j < this.napfenydata.my + this.napfenydata.ypos; j++) {
                    this.napfenydata.setElem(x, j, z, g_napfenyrange);
                }
            }
        }
    };

    this.lapdata = function (oldal, id, vertex, texpos) {
        if (oldal === 2 || oldal === 3)
            id += this.ypos;
        var oldalelem = this.oldalLapList[oldal][id];
        if (oldalelem) {
            oldalelem.vertex = vertex;
            oldalelem.texpos = texpos;
            oldalelem.VertexData = undefined;
        }
    };
    this.init();
}

function NapFenyCalc(wt, oszloparr) {
    var NAPFENY_INC = 1000;

    var keret = g_napfenyrange + g_napfenyrange;//range + keret

    var updbitmap_x = (wt.px * g_mxz) - keret;
    var updbitmap_z = (wt.pz * g_mxz) - keret;
    var updbitmap_len = g_mxz + (keret * 2);

    var updbitmap = new Uint8Array(updbitmap_len * updbitmap_len);
    this.napfenylist = new Int32Array(0);
    this.napfenylist.count = 0;



    this.GetMaxOszlopGlobal = function (x, z) {

        var wtx = (x / g_mxz) | 0;
        var wtz = (z / g_mxz) | 0;
        var px = x % g_mxz;
        var pz = z % g_mxz;
        var wt = GetWt(wtx, wtz);
        if (wt === undefined)
            return INT32MIN;
        return wt.getMaxOszlop(px, pz);
    };
    this.AddNapfeny = function (x, y, z, e) {
        if (this.napfenylist.length < this.napfenylist.count + 4) {
            var uj = new Int32Array(this.napfenylist.length + NAPFENY_INC);
            uj.set(this.napfenylist);
            uj.count = this.napfenylist.count;
            this.napfenylist = uj;
        }
        this.napfenylist[this.napfenylist.count++] = x;
        this.napfenylist[this.napfenylist.count++] = y;
        this.napfenylist[this.napfenylist.count++] = z;
        this.napfenylist[this.napfenylist.count++] = e;
    };
    this.setBitmap = function (x, z, del) {
        var px = x - updbitmap_x;
        var pz = z - updbitmap_z;
        if (px < 0 || pz < 0 || px >= updbitmap_len || pz >= updbitmap_len) {
            throw "NAPFENY -> BITMAP BUG " + px + " _ " + pz;

        }
        var index = px + (pz * updbitmap_len);
        updbitmap[index] = Math.max(updbitmap[index], del);
    };
    this.addalforras = function (x, y, z, e) {
        e -= 1;
        //TODO Opt: > jó, de lassú, >= amikor a napfény kint van a keretből, akkor egyből elhal
        if (this.GetNapfenyGlobal(x, y, z) > e) {
            return;
        }

        if (GetElemGlobal_Feny(x, y, z) === 0) {
            this.AddNapfeny(x, y, z, e);

            this.SetNapfenyGlobal(x, y, z, e);
        }
    };

    this.OszlopVizsga = function (x, z, deloszlop) {

        var wtx = (x / g_mxz) | 0;
        var wtz = (z / g_mxz) | 0;
        var px = x % g_mxz;
        var pz = z % g_mxz;

        var lwt = GetWt(wtx, wtz);
        if (lwt === undefined)
            return;
        var maxy = lwt.getMaxOszlop(px, pz);
        if (maxy === undefined)
            return;

        if (maxy !== INT32MIN) {


            if (deloszlop === 2) {//ezt a területet törölni kell
                for (var i = lwt.napfenydata.ypos; i < maxy; i++) {
                    lwt.napfenydata.setElem(px, i, pz, 0);
                }
            }


            var max_szom = maxy;

            max_szom = Math.min(max_szom, this.GetMaxOszlopGlobal(x - 1, z));
            max_szom = Math.min(max_szom, this.GetMaxOszlopGlobal(x + 1, z));
            max_szom = Math.min(max_szom, this.GetMaxOszlopGlobal(x, z - 1));
            max_szom = Math.min(max_szom, this.GetMaxOszlopGlobal(x, z + 1));

            if (max_szom === INT32MIN) {//olyan szomszéd van, ahol nincs semmi
                max_szom = lwt.napfenydata.ypos;//feltöltjük a legaljáig
            }
            for (var k = max_szom; k < maxy - 1; k++) {
                this.addalforras(x, k, z, g_napfenyrange);

            }
        }
    };
    for (var i = 0; i < oszloparr.length; i++) {
        if (oszloparr[i] === 1) {
            var x = i % g_mxz;
            var z = (i / g_mxz) | 0;
            x += wt.px * g_mxz;
            z += wt.pz * g_mxz;

            for (var tx = -keret; tx < keret; tx++) {
                for (var tz = -keret; tz < keret; tz++) {

                    var torol = 1;
                    if (Math.abs(tx) <= g_napfenyrange && Math.abs(tz) <= g_napfenyrange)
                        torol = 2;

                    this.setBitmap(x + tx, z + tz, torol);

                }
            }

        }
    }

    for (var i = 0; i < updbitmap.length; i++) {
        if (updbitmap[i] === 0)
            continue;
        var x = i % updbitmap_len;
        var z = (i / updbitmap_len) | 0;
        x += updbitmap_x;
        z += updbitmap_z;
        this.OszlopVizsga(x, z, updbitmap[i]);
    }

    var debug = 50000;
    var pos = 0;
    while (debug--) {



        if (this.napfenylist.count < pos + 4)
            break;
        var x = this.napfenylist[pos++];
        var y = this.napfenylist[pos++];
        var z = this.napfenylist[pos++];
        var e = this.napfenylist[pos++];

        if (GetNapfenyGlobal(x, y, z) < e) {
            SetNapfenyGlobal(x, y, z, e);
        }

        this.addalforras(x - 1, y, z, e);
        this.addalforras(x + 1, y, z, e);
        this.addalforras(x, y - 1, z, e);
        this.addalforras(x, y + 1, z, e);
        this.addalforras(x, y, z - 1, e);
        this.addalforras(x, y, z + 1, e);


    }

    this.napfenyForrasok = [];
}


function Kigyujt_WtPontFeny(wtx, wtz) {
    var ret = [];
    var terraintav = Math.ceil(g_pontfeny_maxrange / g_mxz);
    var wtx1 = wtx - terraintav;
    var wtx2 = wtx + terraintav;
    var wtz1 = wtz - terraintav;
    var wtz2 = wtz + terraintav;

    var wt_tav = Math.ceil(g_mxz / 2);
    var wtpos_x = wtx * g_mxz + wt_tav;
    var wtpos_z = wtz * g_mxz + wt_tav;

    for (var x = wtx1; x <= wtx2; x++) {
        for (var z = wtz1; z <= wtz2; z++) {
            var awt = GetWt(x, z);
            if (awt === undefined)
                continue;
            for (var i = 0; i < awt.pontfenylist.length; i++) {
                var feny = awt.pontfenylist[i];
                //csak akkor kell kigyüjteni ha valóban eléri ezt a WT-t
                if (Math.abs(wtpos_x - feny.fenyp[0]) < feny.range + wt_tav && Math.abs(wtpos_z - feny.fenyp[2]) < feny.range + wt_tav) {
                    ret.push(feny);
                }
            }
        }
    }

    return ret;
}

function AddWt(x, z, wt) {
    var index = wtlist.push(wt) - 1;
    wtlist_index["" + x + "_" + z] = index;
}

function GetWt(x, z) {
    var index = wtlist_index["" + x + "_" + z];
    if (index === undefined)
        return undefined;
    var wt = wtlist[index];
    //ha wt === undefined error?
    return wt;
}

function DelWt(x, z) {
    var index = wtlist_index["" + x + "_" + z];
    if (index === undefined) {
        throw "delWT -> Not find index";
    }
    var lastindex = wtlist.length - 1;
    if (index !== lastindex) {//nem az ucsót töröljük
        wtlist[index] = wtlist[lastindex];
        var lpx = wtlist[lastindex].px;
        var lpz = wtlist[lastindex].pz;
        wtlist_index["" + lpx + "_" + lpz] = index;
    }
    wtlist_index.splice["" + x + "_" + z];
    wtlist.splice(lastindex);
}

function GetNapfenyGlobal(x, y, z) {
    var wtx = (x / g_mxz) | 0;
    var wtz = (z / g_mxz) | 0;
    var px = x % g_mxz;
    var pz = z % g_mxz;
    var wt = GetWt(wtx, wtz);
    if (wt === undefined)
        return g_napfenyrange;

    var ret = wt.napfenydata.getElem_undef(px, y, pz);
    if (ret === undefined)
        ret = g_napfenyrange;
    return ret;
}

function GetFullfenyGlobal(x, y, z) {
    var wtx = (x / g_mxz) | 0;
    var wtz = (z / g_mxz) | 0;
    var px = x % g_mxz;
    var pz = z % g_mxz;
    var wt = GetWt(wtx, wtz);
    if (wt === undefined)
        return 255 << 24;

    var ret = wt.fullfenydata.getElem_undef(px, y, pz);
    if (ret === undefined)
        ret = 255 << 24;
    return ret;
}

function AddFullfenyGlobal(x, y, z, er, eg, eb) {
    var wtx = (x / g_mxz) | 0;
    var wtz = (z / g_mxz) | 0;
    var px = x % g_mxz;
    var pz = z % g_mxz;
    var wt = GetWt(wtx, wtz);
    if (wt === undefined)
        return 255 << 24;
    wt.fullfenydata.y_teszt(y);
    wt.pontfeny_r_data.y_teszt(y);
    wt.pontfeny_g_data.y_teszt(y);
    wt.pontfeny_b_data.y_teszt(y);

    wt.pontfeny_r_data.addElem(px, y, pz, er);
    wt.pontfeny_g_data.addElem(px, y, pz, eg);
    wt.pontfeny_b_data.addElem(px, y, pz, eb);

}

function SetNapfenyGlobal(x, y, z, e) {
    var wtx = (x / g_mxz) | 0;
    var wtz = (z / g_mxz) | 0;
    var px = x % g_mxz;
    var pz = z % g_mxz;
    var wt = GetWt(wtx, wtz);
    if (wt === undefined)
        return 0;
    wt.napfenydata.setElem(px, y, pz, e);
}

function GetElemGlobal(x, y, z) {
    var wtx = (x / g_mxz) | 0;
    var wtz = (z / g_mxz) | 0;
    var px = x % g_mxz;
    var pz = z % g_mxz;
    var wt = GetWt(wtx, wtz);
    if (wt === undefined)
        return 0;
    return wt.getElem(px, y, pz);
}
//csak olyan elemehet ad vissza ahol a fény nem megy át (nem "átlátszó")
function GetElemGlobal_Feny(x, y, z) {
    var wtx = (x / g_mxz) | 0;
    var wtz = (z / g_mxz) | 0;
    var px = x % g_mxz;
    var pz = z % g_mxz;
    var wt = GetWt(wtx, wtz);
    if (wt === undefined)
        return 0;
    var elem = wt.getElem(px, y, pz);
    if (elem === 0)
        return 0;
    var type = GetBlockType(elem);
    if (type.tul.szabadfeny === 0) {
        return elem;

    }
    return 0;
}



function FenyUpd(wt, oszlopupd, kozelpontfeny_list) {
    wt.oszlopUpdate(oszlopupd);
    NapFenyCalc(wt, oszlopupd);

    var napfeny_range_gmx = Math.ceil(g_napfenyrange / g_mxz); //ennyi wt fogalal magába a napfény range távolság
    var upd_wt_x1 = wt.px - napfeny_range_gmx;
    var upd_wt_x2 = wt.px + napfeny_range_gmx;
    var upd_wt_z1 = wt.pz - napfeny_range_gmx;
    var upd_wt_z2 = wt.pz + napfeny_range_gmx;


    var now = performance.now();
    for (var fi = 0; fi < kozelpontfeny_list.length; fi++) {
        var feny = kozelpontfeny_list[fi];
        if (!feny.needupd)
            continue;
        feny.needupd = false;

        feny.calc();
        mer_osszpontf_upd++;
        feny.fenyDataApply(false);

        var updwt = Math.floor((feny.fenyp[0] - feny.range) / g_mxz);
        if (updwt < upd_wt_x1)
            upd_wt_x1 = updwt;
        var updwt = Math.ceil((feny.fenyp[0] + feny.range) / g_mxz);
        if (updwt > upd_wt_x2)
            upd_wt_x2 = updwt;
        var updwt = Math.floor((feny.fenyp[2] - feny.range) / g_mxz);
        if (updwt < upd_wt_z1)
            upd_wt_z1 = updwt;
        var updwt = Math.ceil((feny.fenyp[2] + feny.range) / g_mxz);
        if (updwt > upd_wt_z2)
            upd_wt_z2 = updwt;
    }
    if (g_showPerf)
        console.info("pontfenycalc: " + (performance.now() - now));

    for (var x = upd_wt_x1; x <= upd_wt_x2; x++) {
        for (var z = upd_wt_z1; z <= upd_wt_z2; z++) {
            var l_wt = GetWt(x, z);
            if (l_wt !== undefined) {
                l_wt.needFenyUpd = true;
            }
        }
    }
}

function Wt_needFenyUpd(wtx, wtz) {
    var now = performance.now();

    for (var i = 0; i < wtlist.length; i++) {
        var wt = wtlist[i];
        if (wt.needFenyUpd === false)
            continue;
        FullFenyUpd(wt);
    }
    if (g_showPerf)
        console.info("Updfenydata1: " + (performance.now() - now));
    var now = performance.now();
    for (var i = 0; i < wtlist.length; i++) {
        var wt = wtlist[i];
        if (wt.needFenyUpd === false)
            continue;
        var send = (wt.px === wtx && wt.pz === wtz);
        wt.UpdFenyData(!send);
        wt.needFenyUpd = false;
    }
    if (g_showPerf)
        console.info("Updfenydata2: " + (performance.now() - now));
}
function GetCsop(tip) {
    if (tip === 0)
        return 0;
    if (g_blocktypelist[tip] === undefined)
        throw tip;
    return g_blocktypelist[tip].csoport;
}
function GetBlockType(id) {
    var type = g_blocktypelist[id];
    if (type === undefined) {
        throw "#1 Ismeretlen blocktype:" + id;
    }
    return type;
}

function InitGlobal(data) {
    g_mxz = data.mxz;
    g_napfenyrange = data.napfenyrange;
    g_pontfeny_maxrange = data.pontfeny_maxrange;
    g_oldalfenyarr = data.oldalfenyarr;
    g_blocktypelist = data.blocktypelist;
    g_csoplist = data.csoplist;
    g_blocksize = data.blocksize;
}

function UpdKigyujtPontFeny(pontfenylist, x, y, z, torles) {
    for (var i = 0; i < pontfenylist.length; i++) {
        var feny = pontfenylist[i];
        feny.UpdateNeedRange(x, y, z, torles);
    }

}


self.onmessage = function (e) {
    var data = e.data;
    if (data.type === "INIT") {
        InitGlobal(data);
        return;
    }
    if (data.type === "NEWDATA") {
        var wt = new VoxelWorker(data.px, data.pz);
        AddWt(data.px, data.pz, wt);
        return;
    }

    if (data.type === "DELDATA") {
        DelWt(data.px, data.pz);
        self.postMessage({type: "DELOK", px: data.px, pz: data.pz});
        return;
    }

    var wt = GetWt(data.px, data.pz);
    if (wt === undefined) {
        throw "No texdata in worker";
    }
    if (data.type === "UPDDATA") {
        var ossz_per = performance.now();
        var upddate = data.upddata;
        var upddata_data = data.upddata_data;
        var oszlopupd = new Uint8Array(g_mxz * g_mxz);
        var addfeny_arr = [];
        var delfeny_arr = [];

        var kozelpontfeny_list = Kigyujt_WtPontFeny(wt.px, wt.pz);
        var wtr_x = wt.px * g_mxz;
        var wtr_z = wt.pz * g_mxz;

        for (var i = 0; i < upddate.length; i += 4) {
            var x = upddate[i + 0];
            var y = upddate[i + 1];
            var z = upddate[i + 2];
            var t = upddate[i + 3];



            if (t === 0) {

                var elem = wt.getElem(x, y, z);
                oszlopupd[x + (z * g_mxz)] = 1;
                //ha eleme fény akkor hozzáadás a törölt fény tömbhöz
                wt.delElem(x, y, z);

                var type = GetBlockType(elem);
                var tul = type.tul;
                if (tul.szabadfeny === 0)
                    UpdKigyujtPontFeny(kozelpontfeny_list, x + wtr_x, y, z + wtr_z, true);

                if (tul.pontfeny_rgb === 1) {
                    delfeny_arr.push([x + wtr_x, y, z + wtr_z]);
                }
                continue;
            }
            var type = GetBlockType(t);
            var tul = type.tul;
            if (tul.szabadfeny === 0)
                UpdKigyujtPontFeny(kozelpontfeny_list, x + wtr_x, y, z + wtr_z, false);

            oszlopupd[x + (z * g_mxz)] = 1;

            wt.addElem(x, y, z, t);


            if (tul.pontfeny_rgb === 1) {
                var data = upddata_data["d" + x + "_" + y + "_" + z];

                var fenyobj = {};
                fenyobj.pos = [x, y, z];
                fenyobj.pos[0] += wt.px * g_mxz;
                fenyobj.pos[2] += wt.pz * g_mxz;



                fenyobj.szin = [data[0] & 0xFF, (data[0] >> 8) & 0xFF, (data[0] >> 16) & 0xFF];
                fenyobj.range = data[1];
                if (fenyobj.range > g_pontfeny_maxrange) {
                    throw "ERROR: " + data.range + " (data.range) > " + g_pontfeny_maxrange + " (g_pontfeny_maxrange)";
                }
                addfeny_arr.push(fenyobj);

            }
        }

        for (var i = 0; i < delfeny_arr.length; i++) {
            var fenypos = delfeny_arr[i];



            var f = wt.delPontFeny(fenypos);
            if (f === undefined) {
                console.info(fenypos);
                throw "ERROR: Delfeny, nem talalhato";
            }
            for (var fi = 0; fi < kozelpontfeny_list.length; fi++) {
                if (kozelpontfeny_list[fi] === f) {
                    kozelpontfeny_list.splice(fi);
                    break;
                }
            }
            f.fenyDataApply(true);
        }
        for (var i = 0; i < addfeny_arr.length; i++) {
            var fenyobj = addfeny_arr[i];
            var f = new PontFeny(fenyobj.pos, fenyobj.szin, fenyobj.range, AddFullfenyGlobal, GetElemGlobal_Feny);

            wt.addPontFeny(f);
            kozelpontfeny_list.push(f);

        }



        var updlist = wt.update();


        var now = performance.now();
        mer_osszpontf_upd = 0;
        FenyUpd(wt, oszlopupd, kozelpontfeny_list);
        if (g_showPerf)
            console.info("upddata: " + (performance.now() - now) + " ossz upd pont feny: " + mer_osszpontf_upd);

        var mx = g_mxz;
        var my = wt.my;

        var wtx = wt.px * mx;
        var wtz = wt.pz * mx;

        var now = performance.now();

        this.getElemLoc = function (x, y, z) {
            /*if (x < 0 || x >= g_mxz || z < 0 || z >= g_mxz)
             return GetElemGlobal(x + wtx, y, z + wtz);*/
            return wt.blockdata.getElem_noy(x, y, z);
        };
        for (var i = 0; i < updlist.length; i++) {
            var oldalelem = updlist[i];

            var upd_p1 = oldalelem.p1;
            var upd_m1 = oldalelem.m1;



            if (oldalelem.oldal === 0) {

                var munkaarr_p1 = new Uint32Array(mx * my);
                var munkaarr_m1 = new Uint32Array(mx * my);
                var munkaarr_p1cs = new Uint32Array(mx * my);
                var munkaarr_m1cs = new Uint32Array(mx * my);
                for (var iy = 0; iy < my; iy++) {
                    for (var ix = 0; ix < mx; ix++) {

                        var elem = this.getElemLoc(oldalelem.id, iy, ix);
                        if (elem !== 0) {
                            var elemcs = GetCsop(elem);
                            var p1elem = this.getElemLoc(oldalelem.id + 1, iy, ix);
                            var m1elem = this.getElemLoc(oldalelem.id - 1, iy, ix);
                            if (GetCsop(m1elem) !== GetCsop(elem)) {
                                munkaarr_m1[ix + ((iy) * mx)] = elem;
                                munkaarr_m1cs[ix + ((iy) * mx)] = elemcs;
                            }
                            if (GetCsop(p1elem) !== GetCsop(elem)) {
                                munkaarr_p1[ix + ((iy) * mx)] = elem;
                                munkaarr_p1cs[ix + ((iy) * mx)] = elemcs;
                            }
                        }

                    }

                }
                if (upd_p1) {
                    delpixeldata(wt.texdata, oldalelem.p1_texpos);
                    var vertex = minimumVertex(munkaarr_p1, mx, munkaarr_p1cs);//ő tudja majd milyen csoportba vagyunk?!?!?!
                    var texpos = {};

                    addpixeldata(wt.texdata, oldalelem.id, 1, vertex, wt.blockdata, texpos);


                    wt.lapdata(1, oldalelem.id, vertex, texpos.arr);
                }
                if (upd_m1) {
                    delpixeldata(wt.texdata, oldalelem.m1_texpos);
                    var vertex = minimumVertex(munkaarr_m1, mx, munkaarr_m1cs);
                    var texpos = {};
                    addpixeldata(wt.texdata, oldalelem.id, 0, vertex, wt.blockdata, texpos);
                    wt.lapdata(0, oldalelem.id, vertex, texpos.arr);
                }

            } else if (oldalelem.oldal === 1) {

                var munkaarr_p1 = new Uint32Array(mx * mx);
                var munkaarr_m1 = new Uint32Array(mx * mx);
                var munkaarr_p1cs = new Uint32Array(mx * mx);
                var munkaarr_m1cs = new Uint32Array(mx * mx);
                for (var iy = 0; iy < mx; iy++) {
                    for (var ix = 0; ix < mx; ix++) {
                        var elem = this.getElemLoc(ix, oldalelem.id, iy);

                        if (elem !== 0) {
                            var elemcs = GetCsop(elem);
                            var p1elem = this.getElemLoc(ix, oldalelem.id + 1, iy);
                            var m1elem = this.getElemLoc(ix, oldalelem.id - 1, iy);

                            if (GetCsop(m1elem) !== elemcs) {
                                munkaarr_m1[ix + ((iy) * mx)] = elem;
                                munkaarr_m1cs[ix + ((iy) * mx)] = elemcs;
                            }
                            if (GetCsop(p1elem) !== elemcs) {
                                munkaarr_p1[ix + ((iy) * mx)] = elem;
                                munkaarr_p1cs[ix + ((iy) * mx)] = elemcs;
                            }
                        }

                    }
                }
                if (upd_p1) {
                    delpixeldata(wt.texdata, oldalelem.p1_texpos);
                    var vertex = minimumVertex(munkaarr_p1, mx, munkaarr_p1cs);
                    var texpos = {};
                    addpixeldata(wt.texdata, oldalelem.id, 3, vertex, wt.blockdata, texpos);


                    wt.lapdata(3, oldalelem.id, vertex, texpos.arr);
                }
                if (upd_m1) {
                    delpixeldata(wt.texdata, oldalelem.m1_texpos);
                    var vertex = minimumVertex(munkaarr_m1, mx, munkaarr_m1cs);
                    var texpos = {};
                    addpixeldata(wt.texdata, oldalelem.id, 2, vertex, wt.blockdata, texpos);

                    wt.lapdata(2, oldalelem.id, vertex, texpos.arr);
                }
            } else {
                var munkaarr_p1 = new Uint32Array(mx * my);
                var munkaarr_m1 = new Uint32Array(mx * my);
                var munkaarr_p1cs = new Uint32Array(mx * my);
                var munkaarr_m1cs = new Uint32Array(mx * my);
                for (var iy = 0; iy < my; iy++) {
                    for (var ix = 0; ix < mx; ix++) {

                        var elem = this.getElemLoc(ix, iy, oldalelem.id);

                        if (elem !== 0) {
                            var elemcs = GetCsop(elem);
                            var p1elem = this.getElemLoc(ix, iy, oldalelem.id + 1);
                            var m1elem = this.getElemLoc(ix, iy, oldalelem.id - 1);
                            if (GetCsop(m1elem) !== elemcs) {
                                munkaarr_m1[ix + ((iy) * mx)] = elem;
                                munkaarr_m1cs[ix + ((iy) * mx)] = elemcs;
                            }
                            if (GetCsop(p1elem) !== elemcs) {
                                munkaarr_p1[ix + ((iy) * mx)] = elem;
                                munkaarr_p1cs[ix + ((iy) * mx)] = elemcs;
                            }
                        }

                    }
                }
                if (upd_p1) {
                    delpixeldata(wt.texdata, oldalelem.p1_texpos);
                    var vertex = minimumVertex(munkaarr_p1, mx, munkaarr_p1cs);
                    var texpos = {};
                    addpixeldata(wt.texdata, oldalelem.id, 5, vertex, wt.blockdata, texpos);

                    wt.lapdata(5, oldalelem.id, vertex, texpos.arr);
                }
                if (upd_m1) {
                    delpixeldata(wt.texdata, oldalelem.m1_texpos);
                    var vertex = minimumVertex(munkaarr_m1, mx, munkaarr_m1cs);
                    var texpos = {};
                    addpixeldata(wt.texdata, oldalelem.id, 4, vertex, wt.blockdata, texpos);

                    wt.lapdata(4, oldalelem.id, vertex, texpos.arr);
                }
            }
        }
        if (g_showPerf)
            console.info("block update: " + (performance.now() - now));
        Wt_needFenyUpd(wt.px, wt.pz);

        var now = performance.now();
        wt.update_ok();
        if (g_showPerf)
            console.info("update_ok: " + (performance.now() - now));
        if (g_showPerf)
            console.info(performance.now() - ossz_per);

    }




}
;
